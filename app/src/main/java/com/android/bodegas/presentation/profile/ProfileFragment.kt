package com.android.bodegas.presentation.profile

import android.app.Activity.RESULT_OK
import android.app.Dialog
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.BitmapFactory
import android.os.Bundle
import android.provider.MediaStore
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.core.app.ActivityCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.swiperefreshlayout.widget.CircularProgressDrawable
import com.android.bodegas.R
import com.android.bodegas.data.PreferencesManager
import com.android.bodegas.domain.util.Status
import com.android.bodegas.presentation.login.LoginActivity
import com.android.bodegas.presentation.register_user.model.UserView
import com.android.bodegas.presentation.save_order.SaveOrderViewModel
import com.bumptech.glide.Glide
import com.google.android.material.snackbar.Snackbar
import com.google.android.material.textfield.TextInputEditText
import com.google.android.material.textfield.TextInputLayout
import kotlinx.android.synthetic.main.fragment_profile.*
import org.koin.androidx.viewmodel.ext.android.sharedViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel


class ProfileFragment : Fragment() {

    companion object {
        const val TAG = "ProfileFragment"
        const val TEMP_IMAGE_NAME = "tempImage"
        const val USERTYPE = "C"

        @JvmStatic
        fun newInstance() =
            ProfileFragment().apply {
                arguments = Bundle().apply {
                }
            }
    }
    private val MINIMUM_NEW_PASSWORD_LENGTH = 6
    private val MESSAGE_BAD_PASSWORD_LENGTH = "Se requiere al menos 6 caracteres."
    private val REQUEST_CODE_STORAGE = 200
    private val profileViewModel: ProfileViewModel by sharedViewModel()
    private val saveOrderViewModel: SaveOrderViewModel by viewModel()
    private var user = PreferencesManager.getInstance().getUser()
    private var listener: OnFragmentInteractionListener? = null
    private var filePathUser = String()

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnFragmentInteractionListener) {
            listener = context
        } else {
            throw RuntimeException("$context must implement OnFragmentInteractionListener")
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_profile, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        observeDataUser()
        observeUpdatePhoto()
        updatePassword()
        observeUpdatePassword()
    }

    fun refreshView(){
        profileViewModel.getDataUser()
    }

    private fun setView(user: UserView) {
        tvUserName.text = """${user.name} ${user.lastNamePaternal} ${user.lastNameMaternal}"""
        tvDirection.text = user.address
        tvEmail.text = user.email
        valueDepartment.text = user.departament
        valueProvince.text = user.province
        valueDistrict.text = user.district
        valueDni.text = user.dni
        valueUrbanization.text = user.urbanization

        btnUpdateData.setOnClickListener {
            goToUpdateDataEstablishment()
        }

        btnCloseSession.setOnClickListener {
            val dialogBuilder = AlertDialog.Builder(context!!)
            dialogBuilder
                .setMessage("Al cerrar se borrarán todos los datos que tenga guardados en su carrito")
                .setCancelable(false)
                .setPositiveButton("ACEPTAR", DialogInterface.OnClickListener { dialog, id ->
                    profileViewModel.deleteOrders()
                    val intent = Intent(context, LoginActivity::class.java)
                    startActivity(intent)
                    activity!!.finish()
                })
                .setNegativeButton("CANCELAR", DialogInterface.OnClickListener { dialog, id ->
                    dialog.cancel()
                })

            val alert = dialogBuilder.create()
            alert.setTitle("¿Desea cerrar sesión?")
            alert.show()

        }

        if (user.pathImage != "") {
            val circularProgressDrawable = CircularProgressDrawable(context!!)
            circularProgressDrawable.strokeWidth = 5f
            circularProgressDrawable.centerRadius = 30f
            circularProgressDrawable.start()

            Glide.with(context!!)  // this
                .load(user.pathImage)
                .placeholder(circularProgressDrawable)
                .error(context!!.getDrawable(R.drawable.ic_image_black_24dp))
                .into(ivUserPhoto)
            ivUserPhoto.setPadding(0, 0, 0, 0)
        }

        ivUserPhoto.setOnClickListener {
            if (validatePermissionsStorage()) {
                selectImage()
            } else {
                requestPermissions()
            }
        }

    }

    private fun observeDataUser() {
        profileViewModel.getUserView.observe(viewLifecycleOwner, Observer {
            when (it.status) {
                Status.SUCCESS -> {
                    user = it.data
                    setView(it.data)
                }
                else -> {
                }
            }
        })
        profileViewModel.getDataUser()
    }


    private fun validatePermissionsStorage(): Boolean {
        return ActivityCompat.checkSelfPermission(
            activity!!.applicationContext,
            android.Manifest.permission.READ_EXTERNAL_STORAGE
        ) == PackageManager.PERMISSION_GRANTED
    }

    private fun selectImage() {
        val options =
            arrayOf<CharSequence>("Elegir de galería (Imagen menor a 200kb)", "Cancelar")
        val builder = AlertDialog.Builder(context!!)
        builder.setTitle("AGREGA TU FOTO")
        builder.setItems(options, DialogInterface.OnClickListener { dialog, item ->
            /* if (options[item] == "Take Photo") {
                 val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
                 val f = File(
                     Environment.getExternalStorageDirectory(),
                     "temp.jpg"
                 )
                 intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(f))
                 startActivityForResult(intent, 1)
             } else */
            if (options[item] == "Elegir de galería (Imagen menor a 200kb)") {
                val intent = Intent(
                    Intent.ACTION_PICK,
                    MediaStore.Images.Media.EXTERNAL_CONTENT_URI
                )

                startActivityForResult(intent, 2)
            } else if (options[item] == "Cancel") {
                dialog.dismiss()
            }
        })
        builder.show()
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == RESULT_OK) {
            /*if (requestCode == 1) {
                var f =
                    File(Environment.getExternalStorageDirectory().toString())
                for (temp in f.listFiles()) {
                    if (temp.name == "temp.jpg") {
                        f = temp
                        break
                    }
                }
                try {
                    val bitmap: Bitmap
                    val bitmapOptions = BitmapFactory.Options()
                    bitmap = BitmapFactory.decodeFile(
                        f.absolutePath,
                        bitmapOptions
                    )
                    ivUserPhoto.setImageBitmap(bitmap)
                    val path = (Environment
                        .getExternalStorageDirectory()
                        .toString() + File.separator
                            + "Phoenix" + File.separator + "default")
                    f.delete()
                    var outFile: OutputStream? = null
                    val file =
                        File(path, System.currentTimeMillis().toString() + ".jpg")
                    try {
                        outFile = FileOutputStream(file)
                        bitmap.compress(Bitmap.CompressFormat.JPEG, 85, outFile)
                        outFile.flush()
                        outFile.close()
                    } catch (e: FileNotFoundException) {
                        e.printStackTrace()
                    } catch (e: IOException) {
                        e.printStackTrace()
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            } else*/
            if (requestCode == 2) {
                val selectedImage = data!!.data!!
                val filePath =
                    arrayOf(MediaStore.Images.Media.DATA)
                val c = context!!.contentResolver.query(selectedImage, filePath, null, null, null)
                c!!.moveToFirst()
                val columnIndex: Int = c.getColumnIndex(filePath[0])
                val picturePath: String = c.getString(columnIndex)

                c.close()
                val thumbnail = BitmapFactory.decodeFile(picturePath)
                Log.e(
                    " path:",
                    picturePath + ""
                )
                ivUserPhoto.setPadding(0, 0, 0, 0)
                ivUserPhoto.setImageBitmap(thumbnail)
                filePathUser = picturePath
                profileViewModel.updatePhotoOfUser(user.customerId, picturePath)
            }
        }
    }

    private fun observeUpdatePhoto() {
        profileViewModel.updatePhoto.observe(viewLifecycleOwner, Observer {
            when (it.status) {
                Status.SUCCESS -> {
                    if (it.data) {
                        user.pathImage = it.message
                        PreferencesManager.getInstance().setUser(user)
                        setView(user)
                        listener!!.hideLoading()
                    }
                }

                Status.ERROR -> {
                    Snackbar.make(profileview, it.message!!, Snackbar.LENGTH_LONG).show()
                    listener!!.hideLoading()
                }

                Status.LOADING -> {
                    listener!!.showLoading()
                }
            }
        })
    }

    private fun requestPermissions() {
        val contextProvider =
            ActivityCompat.shouldShowRequestPermissionRationale(
                activity!!,
                android.Manifest.permission.READ_EXTERNAL_STORAGE
            )

        if (contextProvider) {
            Toast.makeText(
                activity!!.applicationContext,
                "Los permisos son requeridos para obtener la imagen",
                Toast.LENGTH_SHORT
            ).show()
        }
        permissionRequest()
    }

    private fun permissionRequest() {
        ActivityCompat.requestPermissions(
            activity!!,
            arrayOf(android.Manifest.permission.READ_EXTERNAL_STORAGE),
            REQUEST_CODE_STORAGE
        )
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            REQUEST_CODE_STORAGE -> {
                if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    selectImage()
                } else {
                    Toast.makeText(
                        activity!!.applicationContext,
                        "No aceptó los permisos",
                        Toast.LENGTH_SHORT
                    ).show()
                }
            }
        }

    }

    interface OnFragmentInteractionListener {
        fun showLoading()
        fun hideLoading()
        fun onBackPressed()
    }

    private fun updatePassword() {
        btnChangePassword.setOnClickListener {
            val dialog = Dialog(context!!)
            dialog.setCancelable(true)
            dialog.setContentView(R.layout.layout_dialog_new_password)
            dialog.window?.setLayout(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT
            )
            dialog.window?.setBackgroundDrawableResource(android.R.color.transparent)
            val inputMail = dialog.findViewById(R.id.inputMail) as TextInputEditText
            val inputNewPassword = dialog.findViewById(R.id.inputNewPassword) as TextInputEditText
            val inputOldPassword = dialog.findViewById(R.id.inputOldPassword) as TextInputEditText
            val btnSearch = dialog.findViewById(R.id.btnSearch) as Button
            val btnCancelar = dialog.findViewById(R.id.btnCancelar) as Button
            val boxNewPassword = dialog.findViewById(R.id.boxNewPassword) as TextInputLayout

            inputMail.setText(PreferencesManager.getInstance().getUser().email)
            btnSearch.setOnClickListener {
                if (inputNewPassword.text.toString().length>= MINIMUM_NEW_PASSWORD_LENGTH) {
                    val changePasswordDtoBody = ChangePasswordDtoBody(
                        inputMail.text.toString(),
                        inputNewPassword.text.toString(),
                        inputOldPassword.text.toString(),
                        USERTYPE
                    )
                    profileViewModel.updatePassword(changePasswordDtoBody)
                    dialog.dismiss()
                    //servicio (email)
                }

                else {
                    boxNewPassword.error=MESSAGE_BAD_PASSWORD_LENGTH
                }
            }

            btnCancelar.setOnClickListener {
                dialog.dismiss()
            }

            dialog.show()

        }
    }

    private fun observeUpdatePassword() {
        profileViewModel.updatePassword.observe(viewLifecycleOwner, Observer {
            when (it.status) {
                Status.SUCCESS -> {
                    listener!!.hideLoading()
                    Snackbar.make(
                        profileview,
                        "Contraseña actualizada correctamente.",
                        Snackbar.LENGTH_LONG
                    ).show()
                }

                Status.ERROR -> {
                    listener!!.hideLoading()
                    Snackbar.make(
                        profileview,
                        "Credenciales no válidas, intenta nuevamente.",
                        Snackbar.LENGTH_LONG
                    ).show()
                }

                Status.LOADING -> {
                    listener!!.showLoading()
                }
            }
        })
    }

    private fun goToUpdateDataEstablishment() {
        val intent = Intent(activity, UpdateDataUserActivity::class.java)
        startActivity(intent)
    }
}