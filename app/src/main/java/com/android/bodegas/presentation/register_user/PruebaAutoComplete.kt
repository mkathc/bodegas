package com.android.bodegas.presentation.register_user

import com.android.bodegas.R
import android.os.Bundle
import android.widget.Toast
import com.android.bodegas.presentation.BaseActivity
import com.google.android.gms.common.api.Status

import com.google.android.libraries.places.api.Places
import com.google.android.libraries.places.api.model.Place
import com.google.android.libraries.places.widget.AutocompleteSupportFragment
import com.google.android.libraries.places.widget.listener.PlaceSelectionListener
import java.util.*

class PruebaAutoComplete : BaseActivity(), PlaceSelectionListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.pruebalayout)

        if (!Places.isInitialized()) {
            Places.initialize(applicationContext, "AIzaSyC80nCXO5MlZ2yAuMFxDBWdZTSwwpSAx3A")
        }

       // val placesClient = Places.createClient(this)

        val autocompleteFragment = supportFragmentManager.findFragmentById(R.id.autocomplete_fragment) as AutocompleteSupportFragment?

        autocompleteFragment!!.setPlaceFields(
            Arrays.asList(
                Place.Field.ID,
                Place.Field.NAME,
                Place.Field.LAT_LNG,
                Place.Field.ADDRESS,
                Place.Field.ADDRESS_COMPONENTS
            )
        )
        autocompleteFragment.setOnPlaceSelectedListener(this)
    }

    override fun onPlaceSelected(p0: Place) {
        Toast.makeText(applicationContext,""+p0!!.name+p0!!.latLng+" ,"+p0.address+" ,"+p0.addressComponents,Toast.LENGTH_LONG).show();
    }

    override fun onError(p0: Status) {
        Toast.makeText(applicationContext,""+p0.toString(),Toast.LENGTH_LONG).show();
    }
}
