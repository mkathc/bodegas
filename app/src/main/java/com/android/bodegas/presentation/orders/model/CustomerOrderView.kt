package com.android.bodegas.presentation.orders.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class CustomerOrderView(
    val orderId: String,
    val deliveryType: String,
    val orderDate:String,
    val shippingDateFrom: String,
    val status: String,
    val establishment:EstablishmentOrdersView,
    val total: String,
    val orderDetails:List<OrdersItemView>,
    val udpateUser: String,
    val deliveryCharge:String,
    val customerAmount: String,
    val paymentMethodCustomerMessage: String,
    val paymentMethodDescription: String,
    val paymentMethodEstablishmentMessage: String,
    val paymentMethodPaymentMethodId: Int,
    val paymentMethodRequestAmount: Boolean

) : Parcelable