package com.android.bodegas.presentation.save_order.mapper

interface SaveViewMapper<V, D> {

    fun mapToUseCase(type: V): D

    fun mapToView(type: D): V
}