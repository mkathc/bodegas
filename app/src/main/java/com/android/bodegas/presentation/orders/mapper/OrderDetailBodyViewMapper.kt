package com.android.bodegas.presentation.orders.mapper

import com.android.bodegas.domain.customerOrders.OrderDetailBody
import com.android.bodegas.domain.customerOrders.OrderDetails
import com.android.bodegas.presentation.Mapper
import com.android.bodegas.presentation.orders.model.OrderDetailBodyView
import com.android.bodegas.presentation.orders.model.OrdersItemView
import com.android.bodegas.presentation.save_order.mapper.SaveViewMapper


class OrderDetailBodyViewMapper : SaveViewMapper<OrderDetailBodyView, OrderDetailBody> {
    override fun mapToUseCase(type: OrderDetailBodyView): OrderDetailBody {
        return OrderDetailBody(
            type.orderDetailId,
            type.state
        )
    }

    override fun mapToView(type: OrderDetailBody): OrderDetailBodyView {
        return OrderDetailBodyView(
            type.orderDetailId,
            type.state
        )
    }

}
