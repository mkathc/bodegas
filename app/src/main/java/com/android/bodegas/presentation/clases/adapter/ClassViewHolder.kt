package com.android.bodegas.presentation.clases.adapter

import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.row_classes.view.*

class ClassViewHolder( itemView: View, viewHolderListener: ViewHolderListener):
    RecyclerView.ViewHolder(itemView) {

    //inyectamos la data
    val name_category_class : TextView = itemView.tvNameClass
    val imageClass: ImageView = itemView.ivImageClass

    init {
        itemView.setOnClickListener {
            viewHolderListener.onClick(
                layoutPosition
            )
        }
    }

    interface ViewHolderListener {
        fun onClick(
            position: Int
        )
    }
}