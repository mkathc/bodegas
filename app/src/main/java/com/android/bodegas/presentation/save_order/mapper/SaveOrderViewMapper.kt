package com.android.bodegas.presentation.save_order.mapper

import com.android.bodegas.domain.products.Products
import com.android.bodegas.domain.save_order.SaveOrder
import com.android.bodegas.presentation.Mapper
import com.android.bodegas.presentation.products.model.ProductsView
import com.android.bodegas.presentation.save_order.model.SaveOrderView

class SaveOrderViewMapper : SaveViewMapper<SaveOrderView, SaveOrder> {

    override fun mapToView(type: SaveOrder): SaveOrderView {
        return SaveOrderView(
            type.id,
            type.storeProductId,
            type.price,
            type.quantity,
            type.subtotal,
            type.unitMeasure,
            type.observation,
            type.name,
            type.imageProduct
        )
    }

    override fun mapToUseCase(type: SaveOrderView): SaveOrder {
        return SaveOrder(
            type.id,
            type.storeProductId,
            type.price,
            type.quantity,
            type.subtotal,
            type.unitMeasure,
            type.observation,
            type.name,
            type.imageProduct
        )
    }

}