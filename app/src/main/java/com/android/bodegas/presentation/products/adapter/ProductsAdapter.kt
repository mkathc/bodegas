package com.android.bodegas.presentation.products.adapter


import android.content.Context
import android.util.Log
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import com.android.bodegas.R
import com.android.bodegas.presentation.products.model.ProductsView
import com.android.bodegas.presentation.supplier.model.SupplierView
import com.bumptech.glide.Glide


class ProductsAdapter(
    private val establishment : SupplierView,
    private val viewHolderListener: ProductsViewHolder.ViewHolderListener
) : RecyclerView.Adapter<ProductsViewHolder>() {

    private val productsList = mutableListOf<ProductsView>()
    private lateinit var context: Context
    private lateinit var establishmentProduct : SupplierView

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProductsViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.row_products, parent, false)
        context = parent.context
        establishmentProduct = establishment
        return ProductsViewHolder(view, viewHolderListener)
    }

    override fun getItemCount(): Int {
        return productsList.size
    }

    override fun onBindViewHolder(holder: ProductsViewHolder, position: Int) {
        val product = productsList[position]
        holder.bind(product, context, establishmentProduct)
    }


    fun setProductsList(poolList: List<ProductsView>) {
        this.productsList.clear()
        this.productsList.addAll(poolList)
        notifyDataSetChanged()
    }


}