package com.android.bodegas.presentation.clases

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.android.bodegas.domain.clases.GetClassesBySupplierId
import com.android.bodegas.domain.util.Resource
import com.android.bodegas.domain.util.Status
import com.android.bodegas.presentation.clases.mapper.ClassViewMapper
import com.android.bodegas.presentation.clases.model.ClassView
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlin.coroutines.CoroutineContext

class ClassViewModel constructor(
    //obtengo los datos del dominio
    //uso mi mapper dominio-presentacion
    private val getClassesBySupplierId: GetClassesBySupplierId,
    private val classesViewMaper: ClassViewMapper
) : ViewModel(), CoroutineScope {

    override val coroutineContext: CoroutineContext
        get() = Dispatchers.IO

    private val _listClasses = MutableLiveData<Resource<List<ClassView>>>()
    val listClasses: LiveData<Resource<List<ClassView>>> = _listClasses

    fun getClassesBySupplierId(
        establishmentId: Int,
        page: String,
        size: String,
        sortBy: String
    ) {
        viewModelScope.launch {
            _listClasses.value =
                Resource(Status.LOADING, mutableListOf(), "")

            val classesResult =
                getClassesBySupplierId.getClassesBySupplierId(establishmentId, page, size, sortBy)

            if (classesResult.status == Status.SUCCESS) {

                val classesResultData = classesResult.data
                if (classesResultData.isNotEmpty()) {
                    val classView = classesResultData.map { classesViewMaper.mapToView(it) }
                    _listClasses.value = Resource(Status.SUCCESS, classView, "")
                } else {
                    _listClasses.value = Resource(Status.SUCCESS, mutableListOf(), "")
                }
            } else {
                _listClasses.value = Resource(Status.ERROR, mutableListOf(), "")
            }

        }
    }
}