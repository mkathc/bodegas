package com.android.bodegas.presentation.supplier.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ScheduleView(
    val range: String,
    val startTime: String ? = "",
    val endTime: String ? = ""
) : Parcelable