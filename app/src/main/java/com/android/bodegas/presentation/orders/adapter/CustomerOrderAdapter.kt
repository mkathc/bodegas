package com.android.bodegas.presentation.orders.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.android.bodegas.R
import com.android.bodegas.presentation.orders.model.CustomerOrderView

class CustomerOrderAdapter  (private val viewHolderListener: CustomerOrderViewHolder.ViewHolderListener):
RecyclerView.Adapter<CustomerOrderViewHolder>() {

    private val customerOrdersList = mutableListOf<CustomerOrderView>()
    private lateinit var context: Context

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CustomerOrderViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.row_customer_order, parent, false)
        context = parent.context
        return CustomerOrderViewHolder(view, viewHolderListener)
    }

    override fun getItemCount(): Int {
        if (customerOrdersList != null)
            return customerOrdersList.size
        return 0
    }

    override fun onBindViewHolder(holder: CustomerOrderViewHolder, position: Int) {
        val customerOrderView = customerOrdersList[position]
        holder.bind(customerOrderView)
    }

    fun setStoreList(poolList: List<CustomerOrderView>) {
        this.customerOrdersList.clear()
        this.customerOrdersList.addAll(poolList)
        notifyDataSetChanged()
    }
}