package com.android.bodegas.presentation.profile

import com.google.gson.annotations.SerializedName

data class UpdateCustomerBody(
    @SerializedName("address")
    var address: String,

    @SerializedName("country")
    val country: String,
    // val country: String,

    @SerializedName("departament")
    val departament: String,

    @SerializedName("district")
    val district: String,

    @SerializedName("dni")
    val dni: String,

    @SerializedName("lastNameMaternal")
    val lastNameMaternal: String,

    @SerializedName("lastNamePaternal")
    val lastNamePaternal: String,

    @SerializedName("name")
    val name: String,

    @SerializedName("phoneNumber")
    val phoneNumber: String,

    @SerializedName("province")
    val province: String,

    @SerializedName("ruc")
    val ruc: String,

    @SerializedName("updateUser")
    val updateUser: String,

    @SerializedName("urbanization")
    val urbanization: String,

    @SerializedName("latitude")
    var latitude: Double,

    @SerializedName("longitude")
    var longitude: Double

)