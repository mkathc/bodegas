package com.android.bodegas.presentation.supplier.mapper

import com.android.bodegas.domain.supplier.PaymentMethod
import com.android.bodegas.domain.supplier.Schedule
import com.android.bodegas.domain.supplier.Supplier
import com.android.bodegas.presentation.Mapper
import com.android.bodegas.presentation.supplier.model.PaymentMethodView
import com.android.bodegas.presentation.supplier.model.ScheduleView
import com.android.bodegas.presentation.supplier.model.SupplierView

class SupplierViewMapper(
    private val scheduleViewMapper: ScheduleViewMapper,
    private val paymentMethodViewMapper: PaymentMethodViewMapper
) : Mapper<SupplierView, Supplier> {

    override fun mapToView(type: Supplier): SupplierView {
        return SupplierView(
            type.establishmentId,
            type.businessName,
            type.address,
            type.establishmentType,
            type.latitude,
            type.longitude,
            getListSchedules(type.shippingSchedule),
            getListSchedules(type.operationSchedule),
            type.dni,
            type.pathImage,
            getHasDelivery(type.delivery),
            getListPaymentMethods(type.paymentMethod),
            type.deliveryCharge
        )
    }

    private fun getHasDelivery(hasDelivery:String):Boolean{
       return hasDelivery == "S"
    }

    private fun getListSchedules(list: List<Schedule>): List<ScheduleView>{
        val listSchedule = mutableListOf<ScheduleView>()
        list.forEach {
            listSchedule.add(scheduleViewMapper.mapToView(it))
        }
        return listSchedule
    }

    private fun getListPaymentMethods(list: List<PaymentMethod>): List<PaymentMethodView>{
        val listPaymentMethods = mutableListOf<PaymentMethodView>()
        list.forEach {
            listPaymentMethods.add(paymentMethodViewMapper.mapToView(it))
        }
        return listPaymentMethods
    }

}