package com.android.bodegas.presentation.supplier.adapter

import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.android.bodegas.presentation.supplier.model.SupplierView
import kotlinx.android.synthetic.main.row_suppliers.view.*

class SupplierViewHolder(itemView: View, viewHolderListener: ViewHolderListener) :
    RecyclerView.ViewHolder(itemView) {
    val storeName: TextView = itemView.tvNameStore
    val storeAddress: TextView = itemView.tvAddressStore
    val storeSchedule: TextView = itemView.tvScheduleStore
    val storeDelivery: TextView = itemView.tvHasDelivery
    val imageStore: ImageView = itemView.ivImageStore
    val holder: ViewHolderListener = viewHolderListener

    fun bind(supplierView: SupplierView) {
        itemView.setOnClickListener {
            holder.onClick(
                layoutPosition,
                supplierView.establishmentType
            )
        }
    }

    interface ViewHolderListener {
        fun onClick(
            position: Int,
            establishmentType:String
        )
    }
}

