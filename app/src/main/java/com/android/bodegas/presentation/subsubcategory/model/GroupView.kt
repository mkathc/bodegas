package com.android.bodegas.presentation.subsubcategory.model

import android.os.Parcelable
import com.android.bodegas.presentation.products.model.ProductsView
import kotlinx.android.parcel.Parcelize

@Parcelize
data class GroupView(
    val total: Int,
    var productList: List<ProductsView> = mutableListOf(),
    var ssubCategotyList: List<SSubcategoryView> = mutableListOf()
): Parcelable