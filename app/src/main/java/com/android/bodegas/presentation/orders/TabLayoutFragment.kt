package com.android.bodegas.presentation.orders

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.viewpager.widget.ViewPager
//import android.support.design.widget.TabLayout;

import com.android.bodegas.R
import com.google.android.material.tabs.TabLayout


class TabLayoutFragment : Fragment() {

    private lateinit var customerOrderCollectionPagerAdapter: CustomerOrderCollectionPagerAdapter
    private lateinit var viewPager: ViewPager

    companion object {
        const val TAG = "TabLayoutFragment"
        @JvmStatic
        fun newInstance() =
            TabLayoutFragment().apply {

            }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_prueba, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        customerOrderCollectionPagerAdapter = CustomerOrderCollectionPagerAdapter(childFragmentManager)
        viewPager = view.findViewById(R.id.pager)
        viewPager.adapter = customerOrderCollectionPagerAdapter
        val tabLayout = view.findViewById(R.id.tab_layout) as TabLayout
        tabLayout.setupWithViewPager(viewPager)

        viewPager?.addOnPageChangeListener(object : ViewPager.SimpleOnPageChangeListener() {
            //override only methods you need, not all of them
            override fun onPageSelected(position: Int) {
            }
        })
    }

}
