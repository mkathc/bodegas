package com.android.bodegas.presentation.save_order

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.android.bodegas.data.PreferencesManager
import com.android.bodegas.domain.generate_order.GenerateOrder
import com.android.bodegas.domain.save_order.GetOrderByUser
import com.android.bodegas.domain.save_order.SaveOrderForSend
import com.android.bodegas.domain.util.DateTimeHelper
import com.android.bodegas.domain.util.Resource
import com.android.bodegas.domain.util.Status
import com.android.bodegas.presentation.products.model.ProductsView
import com.android.bodegas.presentation.save_order.mapper.SaveOrderViewMapper
import com.android.bodegas.presentation.save_order.model.SaveOrderView
import com.android.bodegas.presentation.supplier.model.ScheduleView
import kotlinx.coroutines.*
import java.util.*
import kotlin.coroutines.CoroutineContext

class SaveOrderViewModel constructor(
    private val getOrderByUser: GetOrderByUser,
    private val saveOrderForSend: SaveOrderForSend,
    private val saveOrderViewMapper: SaveOrderViewMapper,
    private val generateOrder: GenerateOrder
) : ViewModel(), CoroutineScope {

    override val coroutineContext: CoroutineContext
        get() = Dispatchers.IO

    private val _listProducts = MutableLiveData<Resource<List<SaveOrderView>>>()
    val listProducts: LiveData<Resource<List<SaveOrderView>>> = _listProducts

    private val _totalValue = MutableLiveData<Resource<Double>>()
    val totalValue: LiveData<Resource<Double>> = _totalValue

    private val _generateOrderResult = MutableLiveData<Resource<String>>()
    val generateOrderResult: LiveData<Resource<String>> = _generateOrderResult

    var scheduleSelected = String()
    var paymentMethodSelected = String()
    var orderList = mutableListOf<SaveOrderView>()

    fun addProductsForOrder(productsView: ProductsView, quantity: Double) {
        viewModelScope.launch {
            val order = getOrderByUser.getOrderById(
                productsView.storeProductId.toString() + "-" + DateTimeHelper.parseCalendar(Calendar.getInstance())
            )

            if (order.status == Status.SUCCESS) {
                saveOrderForSend.updateOrder(
                    saveOrderViewMapper.mapToUseCase(
                        getValuerSaveOrder(
                            productsView,
                            quantity
                        )
                    )
                )
            } else {
                saveOrderForSend.saveOrder(
                    saveOrderViewMapper.mapToUseCase(
                        getValuerSaveOrder(
                            productsView,
                            quantity
                        )
                    )
                )
            }
            getOrdersByUser()
            getTotalValue()
        }

    }

    private fun getValuerSaveOrder(productsView: ProductsView, quantity: Double): SaveOrderView {
        val saveOrderView = SaveOrderView("", 0, 0.0, 0.0, 0.0, "", "", "", "")
        saveOrderView.id = ""
        saveOrderView.storeProductId = productsView.storeProductId
        saveOrderView.price = productsView.price
        saveOrderView.quantity = quantity
        saveOrderView.subtotal = String.format("%.2f", productsView.price * quantity).toDouble()
        saveOrderView.unitMeasure = productsView.unitMeasure
        saveOrderView.observation = ""
        saveOrderView.name = productsView.name
        saveOrderView.imageProduct = productsView.imageProduct.toString()
        return saveOrderView
    }


    fun updateProductsForOrder(saveOrderView: SaveOrderView, quantity: Double) {
        saveOrderView.quantity = quantity
        viewModelScope.launch {
            val order = getOrderByUser.getOrderById(
                saveOrderView.storeProductId.toString() + "-" + DateTimeHelper.parseCalendar(
                    Calendar.getInstance()
                )
            )

            if (order.status == Status.SUCCESS) {
                saveOrderForSend.updateOrder(
                    saveOrderViewMapper.mapToUseCase(
                        saveOrderView
                    )
                )
            } else {
                saveOrderForSend.saveOrder(
                    saveOrderViewMapper.mapToUseCase(
                        saveOrderView
                    )
                )
            }

            getOrdersByUser()
            getTotalValue()
        }

    }


    fun getOrdersByUser() {
        _listProducts.value = Resource(Status.LOADING, mutableListOf(), "")
        viewModelScope.launch {
            val listOrder = getOrderByUser.getOrder()
            val listOrderView = mutableListOf<SaveOrderView>()
            if (listOrder.data.isNotEmpty()) {
                listOrder.data.forEach {
                    listOrderView.add(saveOrderViewMapper.mapToView(it))
                }
            }else{
                PreferencesManager.getInstance().cleanEstablishment()
            }
            orderList = listOrderView
            val resource = Resource(listOrder.status, listOrderView, listOrder.message)
            _listProducts.value = resource
        }
    }

    fun getTotalValue() {
        _totalValue.value = Resource(Status.LOADING, 0.0, "")
        viewModelScope.launch {
            var total = 0.0
            val totalList = getOrderByUser.getOrder()
            totalList.data.forEach {
                total += it.subtotal
            }
            _totalValue.value = Resource(Status.SUCCESS, total, "")
        }
    }

    fun generateOrderForUser(
        deliveryType: String,
        totalValue: String,
        startTime: String,
        endTime: String,
        paymentMethodId: Int,
        deliveryCharge:Double,
        customerAmount: Double
    ) {
        _generateOrderResult.value = Resource(Status.LOADING, "", "")
        viewModelScope.launch {
            val generateOrderResponse =
                generateOrder.generateOrder(deliveryType, totalValue, startTime, endTime, paymentMethodId, deliveryCharge, customerAmount)
            val resource = Resource(
                generateOrderResponse.status,
                generateOrderResponse.data,
                generateOrderResponse.message
            )
            _generateOrderResult.value = resource
        }

    }

    fun deleteOrders() {
        viewModelScope.launch {
            getOrderByUser.deleteOrder()
            getOrdersByUser()
            PreferencesManager.getInstance().cleanEstablishment()

        }
    }

    fun deleteOrderById(orderId: String) {
        viewModelScope.launch {
            getOrderByUser.deleteOrderById(orderId)
            getOrdersByUser()
            getTotalValue()
        }
    }
}