package com.android.bodegas.presentation.orders

import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter

class CustomerOrderCollectionPagerAdapter(fm: FragmentManager) : FragmentStatePagerAdapter(fm){

    override fun getCount(): Int  = 2

    override fun getItem(i: Int): Fragment {

        when (i){
            0-> {
                val fragment = ListaInformadosFragment()
                fragment.arguments = Bundle().apply {
                    putInt(ARG_OBJECT, i + 1)
                    return fragment
                }
            }

            else ->{
                val fragment =  ListaEntregadosFragment()
                fragment.arguments = Bundle().apply {
                    putInt(ARG_OBJECT, i + 1)
                    return fragment
                }
            }

        }

    }

    override fun getPageTitle(position: Int): CharSequence {
        var nombre = position+1;
        return if(nombre==1) "Informados"
        else
            "Entregados"
    }


}

private const val ARG_OBJECT = "object"
private const val TITULO1 = "TITULO1"
private const val TITULO2 = "TITULO2"