package com.android.bodegas.presentation.splash

import android.os.Bundle
import android.os.Handler
import com.android.bodegas.R
import com.android.bodegas.data.PreferencesManager
import com.android.bodegas.presentation.BaseActivity
import com.android.bodegas.presentation.login.LoginActivity
import com.android.bodegas.presentation.main.MainActivity

class SplashActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        showView()
    }

    private fun showView() {
        val handler = Handler()
        handler.postDelayed({
            if (!PreferencesManager.getInstance().getIsInvited()) {
                loginSuccessfully()
            } else {
                nextActivity(null, LoginActivity::class.java, true)
            }
        }, 1000)
    }


    private fun loginSuccessfully() {
        nextActivity(null, MainActivity::class.java, true)
    }

}
