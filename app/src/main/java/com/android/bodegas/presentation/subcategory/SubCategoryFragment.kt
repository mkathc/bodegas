package com.android.bodegas.presentation.subcategory

import android.content.Context
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager

import com.android.bodegas.R
import com.android.bodegas.domain.util.Status
import com.android.bodegas.presentation.products.model.ProductsView
import com.android.bodegas.presentation.subsubcategory.model.SSubcategoryView
import com.android.bodegas.presentation.subcategory.adapter.SubCategoryAdapter
import com.android.bodegas.presentation.subcategory.adapter.SubCategoryViewHolder
import com.android.bodegas.presentation.subcategory.model.SubCategoryView
import com.android.bodegas.presentation.supplier.model.SupplierView
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.fragment_subcategory.*
import org.koin.androidx.viewmodel.ext.android.viewModel

const val ESTABLISHMENT = "establishment"
const val CATEGORY_ID = "category_id"

class SubcategoryFragment : Fragment(), SubCategoryViewHolder.ViewHolderListener {

    private var listener: OnFragmentInteractionListener? = null
    private val subcategoryViewModel: SubCategoryViewModel by viewModel()
    private var subCategoryViewList: MutableList<SubCategoryView> = mutableListOf()
    private var adapter = SubCategoryAdapter(this)

    private lateinit var establishment: SupplierView
    private var categoryId: Int = 0
    private var subCategoryName = String()

    companion object {
        const val TAG = "SubcategoryFragment"

        @JvmStatic
        fun newInstance(establishment: SupplierView, categoryId: Int) =
            SubcategoryFragment().apply {
                arguments = Bundle().apply {
                    putParcelable(ESTABLISHMENT, establishment)
                    putInt(CATEGORY_ID, categoryId)
                }
            }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        establishment = arguments!!.getParcelable(ESTABLISHMENT)!!
        categoryId = arguments!!.getInt(CATEGORY_ID)!!
        observeSubSubCategoryList()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_subcategory, container, false)
    }

    override fun onResume() {
        super.onResume()
        observeSubCategoryList()
    }

    private fun observeSubCategoryList() {
        subcategoryViewModel.listSubCategory.observe(this, Observer {
            when (it.status) {

                Status.SUCCESS -> {
                    listener?.hideLoading()
                    subCategoryViewList = it.data.toMutableList()
                    if (subCategoryViewList.isEmpty()) {
                        containerEmptyState.visibility = View.VISIBLE
                        rvSubcategories.visibility = View.GONE
                    } else {
                        containerEmptyState.visibility = View.GONE
                        rvSubcategories.visibility = View.VISIBLE
                        setRecyclerView()
                    }
                }
                Status.ERROR -> {
                    listener?.hideLoading()
                    Snackbar.make(
                        subCategoryView,
                        getString(R.string.generic_error),
                        Snackbar.LENGTH_LONG
                    ).show()
                }
                Status.LOADING -> {
                    listener?.showLoading()
                }
            }

        })

        subcategoryViewModel.getSubCategories(establishment.storeId, categoryId, "", "", "")
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnFragmentInteractionListener) {
            listener = context
        } else {
            throw RuntimeException("$context must implement OnFragmentInteractionListener")
        }
    }


    private fun setRecyclerView() {
        val linearLayoutManager = LinearLayoutManager(context)
        rvSubcategories.layoutManager = linearLayoutManager
        rvSubcategories.adapter = adapter
        adapter.setStoreList(subCategoryViewList)
    }

    override fun onClick(position: Int) {
        val subCategoryView = subCategoryViewList[position]
        subCategoryName = subCategoryView.name
        subcategoryViewModel.getSubSubCategories(establishment.storeId, subCategoryView.id)
    }

    private fun observeSubSubCategoryList() {
        subcategoryViewModel.listSSubCategory.observe(this, Observer {
            when (it.status) {
                Status.SUCCESS -> {
                    if (it.data.productList.isNotEmpty()) {
                        listener!!.replaceProductsFragment(
                            establishment,
                            it.data.productList,
                            subCategoryName,
                            true
                        )
                    } else {
                        listener!!.replaceSSubCategoryFragment(
                            it.data.ssubCategotyList,
                            subCategoryName,
                            establishment
                        )
                    }
                }
                Status.ERROR -> {
                    listener?.hideLoading()
                    Snackbar.make(
                        subCategoryView,
                        getString(R.string.generic_error),
                        Snackbar.LENGTH_LONG
                    ).show()
                }
                Status.LOADING -> {
                    listener?.showLoading()
                }
            }
        })
    }

    interface OnFragmentInteractionListener {
        fun replaceProductsFragment(
            establishment: SupplierView,
            productsList: List<ProductsView>,
            subCategoryName: String,
            isFromSubCategory: Boolean
        )

        fun replaceSSubCategoryFragment(ssList: List<SSubcategoryView>, subCategoryName: String, establishment: SupplierView)
        fun showLoading()
        fun hideLoading()
    }


}
