package com.android.bodegas.presentation.products.mapper

import com.android.bodegas.domain.products.Products
import com.android.bodegas.presentation.Mapper
import com.android.bodegas.presentation.products.model.ProductsView

class ProductsViewMapper : Mapper<ProductsView, Products> {

    override fun mapToView(type: Products): ProductsView {
        return ProductsView(
            type.storeProductId,
            type.price,
            type.status,
            type.code,
            type.name,
            type.description,
            type.unitMeasure,
            getStock(type.stock),
            type.imageProduct
        )
    }

    private fun getStock(value: String):String{
       return if(value == "N"){
            "AGOTADO"
        }else{
           ""
       }
    }

}