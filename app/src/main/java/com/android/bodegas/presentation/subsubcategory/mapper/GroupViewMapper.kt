package com.android.bodegas.presentation.subsubcategory.mapper

import com.android.bodegas.domain.products.Products
import com.android.bodegas.domain.sub_sub_category.Group
import com.android.bodegas.domain.sub_sub_category.SSubCategory
import com.android.bodegas.presentation.Mapper
import com.android.bodegas.presentation.products.mapper.ProductsViewMapper
import com.android.bodegas.presentation.products.model.ProductsView
import com.android.bodegas.presentation.subsubcategory.model.GroupView
import com.android.bodegas.presentation.subsubcategory.model.SSubcategoryView

class GroupViewMapper(
    private val productsViewMapper: ProductsViewMapper,
    private val sSubcategoryViewMapper: SSubcategoryViewMapper
) : Mapper<GroupView, Group> {

    override fun mapToView(type: Group): GroupView {
        return GroupView(
            type.total,
            getProductsViewList(type.storeProducts),
            getSSList(type.subSubCategories)
        )
    }

    private fun getProductsViewList(list: List<Products>): List<ProductsView> {
        val productsList = ArrayList<ProductsView>()
        if (!list.isNullOrEmpty()) {
            list.forEach {
                productsList.add(productsViewMapper.mapToView(it))
            }
        }
        return productsList
    }

    private fun getSSList(list: List<SSubCategory>): List<SSubcategoryView> {
        val ssubCategoryList = ArrayList<SSubcategoryView>()
        if (!list.isNullOrEmpty()) {
            list.forEach {
                ssubCategoryList.add(sSubcategoryViewMapper.mapToView(it))
            }
        }
        return ssubCategoryList
    }
}

