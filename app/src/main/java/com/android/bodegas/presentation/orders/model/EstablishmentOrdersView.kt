package com.android.bodegas.presentation.orders.model

import android.os.Parcelable
import com.android.bodegas.presentation.supplier.model.PaymentMethodView
import com.android.bodegas.presentation.supplier.model.ScheduleView
import kotlinx.android.parcel.Parcelize

@Parcelize
data class EstablishmentOrdersView (
    val establishmentId: Int,
    val establishmentEmail: String,
    val establishmentName: String,
    val hasDelivery: Boolean,
    val storeScheduleShipping:List<ScheduleView>,
    val storeScheduleOperation:List<ScheduleView>,
    val paymentMethod:List<PaymentMethodView>,
    val deliveryCharge: Boolean,
    val establishmentAddress: String
) : Parcelable

