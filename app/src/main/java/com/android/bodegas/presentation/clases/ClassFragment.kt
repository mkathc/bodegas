package com.android.bodegas.presentation.clases

import android.content.Context
import android.os.Bundle
import android.view.*
import android.widget.AutoCompleteTextView
import androidx.fragment.app.Fragment
import androidx.appcompat.widget.SearchView
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.android.bodegas.R
import com.android.bodegas.domain.util.Status
import com.android.bodegas.presentation.clases.adapter.ClassAdapter
import com.android.bodegas.presentation.clases.adapter.ClassViewHolder
import com.android.bodegas.presentation.clases.model.ClassView
import com.android.bodegas.presentation.products.SearchProductsViewModel
import com.android.bodegas.presentation.products.adapter.ProductsAdapter
import com.android.bodegas.presentation.products.adapter.ProductsViewHolder
import com.android.bodegas.presentation.products.model.ProductsView
import com.android.bodegas.presentation.supplier.model.SupplierView
import org.koin.androidx.viewmodel.ext.android.viewModel
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.fragment_class.*

const val ESTABLISHMENT = "establisment"

class ClassFragment : Fragment(), ClassViewHolder.ViewHolderListener {

    private val classesViewModel: ClassViewModel by viewModel()

    private var classViewList: MutableList<ClassView> = mutableListOf()
    private var stringList = ArrayList<CharSequence>()
    private var adapter = ClassAdapter(this)

    private var newClassList = ArrayList<ClassView>()

    private lateinit var establishment: SupplierView

    private var listener: OnFragmentInteractionListener? = null

    companion object {
        const val TAG = "ClassFragment"

        @JvmStatic
        fun newInstance(establishment: SupplierView) =
            ClassFragment().apply {
                arguments = Bundle().apply {
                    putParcelable(ESTABLISHMENT, establishment)
                }
            }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        establishment = arguments!!.getParcelable(ESTABLISHMENT)!!
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_class, container, false)
    }



    private fun findClassInList(name: CharSequence) {
        for (i in 0 until classViewList.size) {
            if (name == classViewList[i].name) {
                newClassList.add(classViewList[i])
            }
        }
    }

    private fun getStringsArray() {
        classViewList.forEach {
            stringList.add(it.name)
        }
    }

    override fun onResume() {
        super.onResume()
        observeListClasses()
    }

    private fun observeListClasses() {
        classesViewModel.listClasses.observe(this, Observer {
            when (it.status) {
                Status.SUCCESS -> {
                    listener?.hideLoading()
                    classViewList = it.data.toMutableList()
                    if(classViewList.isEmpty()){
                        containerEmptyState.visibility = View.VISIBLE
                        rvClasses.visibility = View.GONE
                    }else{
                        containerEmptyState.visibility = View.GONE
                        rvClasses.visibility = View.VISIBLE
                        setRecyclerView()
                    }
                    getStringsArray()
                }

                Status.ERROR -> {
                    listener?.hideLoading()
                    Snackbar.make(
                        classView,
                        getString(R.string.generic_error),
                        Snackbar.LENGTH_LONG
                    ).show()
                }
                Status.LOADING -> {
                    listener?.showLoading()
                }
            }
        })
        classesViewModel.getClassesBySupplierId(establishment.storeId, "", "", "")
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnFragmentInteractionListener) {
            listener = context
        } else {
            throw RuntimeException("$context must implement OnFragmentInteractionListener")
        }
    }

    private fun setRecyclerView() {
        var linearLayoutManager = LinearLayoutManager(context)
        rvClasses.layoutManager = linearLayoutManager
        rvClasses.adapter = adapter
        adapter.setStoreList(classViewList)
    }

    override fun onClick(position: Int) {
        val classView = classViewList[position]
        listener?.replaceByCategoryFragment(establishment, classView.id, classView.name)
    }


    interface OnFragmentInteractionListener {
        fun replaceByCategoryFragment(establishment: SupplierView, classId: Int, className: String)
        fun showLoading()
        fun hideLoading()
    }


}
