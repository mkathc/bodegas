package com.android.bodegas.presentation.subcategory.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.android.bodegas.R
import com.android.bodegas.presentation.subcategory.model.SubCategoryView
import com.bumptech.glide.Glide

class SubCategoryAdapter (private val viewHolderListener: SubCategoryViewHolder.ViewHolderListener)  :
    RecyclerView.Adapter<SubCategoryViewHolder> () {

    private val subcat_list = mutableListOf<SubCategoryView>()
    private lateinit var context: Context

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SubCategoryViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.row_subcategories, parent, false)
        context = parent.context
        return SubCategoryViewHolder(view, viewHolderListener)
    }

    override fun getItemCount(): Int {
        return subcat_list.size
    }

    override fun onBindViewHolder(holder: SubCategoryViewHolder, position: Int) {
        val _subcat = subcat_list[position]
        holder.name_subcategory.text = _subcat.name
        if(_subcat.pathImage!!.isNotEmpty()){
            Glide.with(context).load(_subcat.pathImage).into(holder.imageSubCategory)
        }else{
            holder.imageSubCategory.visibility = View.INVISIBLE
        }
    }

    fun setStoreList(poolist: List<SubCategoryView>){
        this.subcat_list.clear()
        this.subcat_list.addAll(poolist)
    }

}
