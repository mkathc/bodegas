package com.android.bodegas.presentation.register_user.mapper

import com.android.bodegas.domain.district.District
import com.android.bodegas.presentation.Mapper
import com.android.bodegas.presentation.register_user.model.DistrictView

class DistrictViewMapper: Mapper<DistrictView, District> {
    override fun mapToView(type: District): DistrictView {
        return DistrictView(
            type.id,
            type.name,
            type.province_id,
            type.department_id
        )
    }
}