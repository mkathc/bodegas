package com.android.bodegas.presentation.subcategory.mapper

import com.android.bodegas.domain.subcategory.Subcategory
import com.android.bodegas.presentation.Mapper
import com.android.bodegas.presentation.subcategory.model.SubCategoryView

class SubCategoryViewMapper  : Mapper<SubCategoryView, Subcategory> {
    override fun mapToView(type: Subcategory): SubCategoryView {
        return SubCategoryView(
            type.id,
            type.description,
            type.name,
            type.pathImage
        )
    }
}

