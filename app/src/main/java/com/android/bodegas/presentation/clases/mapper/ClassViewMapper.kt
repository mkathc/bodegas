package com.android.bodegas.presentation.clases.mapper

import com.android.bodegas.domain.clases.Class
import com.android.bodegas.presentation.Mapper
import com.android.bodegas.presentation.clases.model.ClassView

class ClassViewMapper : Mapper<ClassView, Class> {
    override fun mapToView(type: Class): ClassView {
        return ClassView(
            type.id,
            type.description,
            type.name,
            type.pathImage
        )
    }
}