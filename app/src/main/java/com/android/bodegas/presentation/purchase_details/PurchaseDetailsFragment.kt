package com.android.bodegas.presentation.purchase_details

import android.app.DatePickerDialog
import android.app.Dialog
import android.content.Context
import android.content.DialogInterface
import android.os.Build
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.ArrayAdapter
import android.widget.Button
import android.widget.DatePicker
import android.widget.EditText
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AlertDialog
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import com.android.bodegas.R
import com.android.bodegas.data.PreferencesManager
import com.android.bodegas.domain.util.DateTimeHelper
import com.android.bodegas.domain.util.Status
import com.android.bodegas.presentation.save_order.SaveOrderViewModel
import com.android.bodegas.presentation.supplier.model.PaymentMethodView
import com.android.bodegas.presentation.supplier.model.ScheduleView
import com.google.android.material.snackbar.Snackbar
import com.google.android.material.textfield.TextInputEditText
import com.google.android.material.textfield.TextInputLayout
import kotlinx.android.synthetic.main.fragment_purchase_details.*
import kotlinx.coroutines.channels.consumesAll
import org.koin.androidx.viewmodel.ext.android.viewModel
import java.text.SimpleDateFormat
import java.util.*


class PurchaseDetailsFragment : Fragment(), DatePickerDialog.OnDateSetListener {

    companion object {
        const val TAG = "PurchaseDetailsFragment"

        @JvmStatic
        fun newInstance() =
            PurchaseDetailsFragment().apply {
                arguments = Bundle().apply {
                }
            }
    }

    private val CASH_AS_PAYMENT_METHOD ="Efectivo"
    private val HAS_CHARGE_FOR_DELIVERY ="Si"
    private val NO_HAVE_CHARGE_FOR_DELIVERY ="No"
    private val saveOrderViewModel: SaveOrderViewModel by viewModel()
    private var listener: OnFragmentInteractionListener? = null
    private var totalValue = ""
    private var isDelivery = false
    private var dateSelected = ""
    private var startTime = ""
    private var endTime = ""
    private var actualDay = 0
    private var isToday = true
    private var paymentMethodId = 0
    private var DELIVERY_CHARGE_BY_DEFAULT= 0.0
    private var CUSTOMER_AMOUNT_WHEN_PAYMENT_METHOD_IS_NOT_CASH = 0.0
    private var customerAmount = CUSTOMER_AMOUNT_WHEN_PAYMENT_METHOD_IS_NOT_CASH //it will be different from 0 if payment method is equals to "Efectivo"
    private var tvTotalCompraInDouble = 0.0
    private var NO_PAYMENT_METHOD_IS_SELECTED = "-"
    private var PAYMENT_METHOD_ID_FOR_RECOJO_EN_TIENDA = 4 //Hack to permit "RECOJO EN TIENDA". Need to know which Id should be used
    private var CUSTOMER_AMOUNT_FOR_RECOJO_EN_TIENDA = 0.0
    var paymentMethodsList = mutableListOf<PaymentMethodView>()
    lateinit var detailPaymethod: String

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnFragmentInteractionListener) {
            listener = context
        } else {
            throw RuntimeException("$context must implement OnFragmentInteractionListener")
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_purchase_details, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        listener!!.showLoading()
        setOnClickListener()
        observeValueTotal()
        observerGenerateOrder()
        setView()
    }

    private fun observeValueTotal() {
        saveOrderViewModel.totalValue.observe(viewLifecycleOwner, Observer {
            when (it.status) {
                Status.SUCCESS -> {
                    tvTotalCompra.visibility = View.VISIBLE
                    tvTotalCompra.text = String.format("%s %.2f", "s/", it.data)
                    tvTotalCompraInDouble = it.data.toDouble()
                    totalValue = it.data.toString()
                    listener!!.hideLoading()
                }

                Status.ERROR -> {
                    tvTotalCompra.visibility = View.GONE
                }

                Status.LOADING -> {
                    tvTotalCompra.visibility = View.GONE
                }
            }
        })
        saveOrderViewModel.getTotalValue()
    }

    private fun setView() {
        tvDaySelected.text = DateTimeHelper.parseCalendar(Calendar.getInstance())
        tvRecojoDaySelected.text = DateTimeHelper.parseCalendar(Calendar.getInstance())
        dateSelected = DateTimeHelper.parseCalendarToSaveOrder(Calendar.getInstance())
        tvDirection.text = PreferencesManager.getInstance().getUser().address
        isDelivery = PreferencesManager.getInstance().getEstablishmentSelected().hasDelivery
        tvTitleStoreName.text = PreferencesManager.getInstance().getEstablishmentSelected().storeName
        Log.e("error", "------------------------------------------------------------")
        Log.e("error", "VALOR A PINTAR:::::: "+ PreferencesManager.getInstance().getEstablishmentSelected()+ " ::::" +PreferencesManager.getInstance().getEstablishmentSelected().deliveryCharge)
        Log.e("error", "VALOR A PINTAR PreferencesManager.getInstance().getEstablishmentSelected().storeAddress:::::: "+ PreferencesManager.getInstance().getEstablishmentSelected().storeAddress)

        Log.e("error", "------------------------------------------------------------")
        if(PreferencesManager.getInstance().getEstablishmentSelected().deliveryCharge) {
            tvBooleanChargeForDelivery.text=HAS_CHARGE_FOR_DELIVERY
        }
        else tvBooleanChargeForDelivery.text = NO_HAVE_CHARGE_FOR_DELIVERY

        if (PreferencesManager.getInstance().getEstablishmentSelected().hasDelivery) {
            isDelivery = true
            tvIsDelivery.visibility = View.VISIBLE
            tvIsDelivery.setTextColor(ContextCompat.getColor(context!!, R.color.colorPrimary))
            tvInStore.setTextColor(ContextCompat.getColor(context!!, R.color.text_gray_color))
            startTime = ""
            endTime = ""
        } else {
            isDelivery = false
            tvIsDelivery.visibility = View.GONE
            tvInStore.setTextColor(ContextCompat.getColor(context!!, R.color.colorPrimary))
            lineDelivery.visibility = View.GONE
            lineRecojo.visibility = View.VISIBLE
            containerDelivery.visibility = View.INVISIBLE
            containerRecojo.visibility = View.VISIBLE
            startTime = DateTimeHelper.parseCalendarToSaveOrder(Calendar.getInstance())
            endTime =  DateTimeHelper.parseCalendarToSaveOrder(Calendar.getInstance())
        }

        tvScheduleRecojoSelected.text = PreferencesManager.getInstance()
            .getEstablishmentSelected().storeScheduleOperation[0].range

        tvRecojoDirection.text = PreferencesManager.getInstance().getEstablishmentSelected().storeAddress
    }


    private fun setOnClickListener() {
        tvSelectSchedule.setOnClickListener {
            val shippingList =
                PreferencesManager.getInstance().getEstablishmentSelected().storeScheduleShipping
            val listRanges = validateDateForDelivery(shippingList, isToday)
            if (listRanges.isNotEmpty()) {
                getScheduleDialog(listRanges)
            } else {
                Snackbar.make(
                    orderDetailView,
                    "No hay horarios de entrega para hoy, por favor seleccione otro día",
                    Snackbar.LENGTH_SHORT
                ).show()
            }
        }

        btnSendOrder.setOnClickListener {
            info()
            if(isDelivery) {
                info()
                if(startTime.isEmpty()) {
                    Snackbar.make(
                        orderDetailView,
                        "Debe seleccionar un horario de entrega",
                        Snackbar.LENGTH_SHORT
                    ).show()
                }
                else if(getTrueIfPaymentMethodIsNotSelected()) {
                    Snackbar.make(
                        orderDetailView,
                        "Debe seleccionar un método de pago",
                        Snackbar.LENGTH_SHORT
                    ).show()
                }
                else if(getTrueIfPaymentMethodIsEffectiveAndCashIsRequired()) {
                    Snackbar.make(
                        orderDetailView,
                        "Digita el monto a pagar en efectivo",
                        Snackbar.LENGTH_SHORT
                    ).show()
                }
                else {
                    saveOrderViewModel.generateOrderForUser(
                        getTypeDelivery(),
                        totalValue,
                        DateTimeHelper.parseFinalDate("$dateSelected $startTime"),
                        DateTimeHelper.parseFinalDate("$dateSelected $endTime"),
                        paymentMethodId,
                        DELIVERY_CHARGE_BY_DEFAULT, //this is 0.0 because it will be set by establishment
                        customerAmount
                    )
                }
            }

            else {
                info()
                if(getTrueIfPaymentMethodIsNotSelected()) {
                    Snackbar.make(
                        orderDetailView,
                        "Debe seleccionar un método de pago",
                        Snackbar.LENGTH_SHORT
                    ).show()
                }
                else if(getTrueIfPaymentMethodIsEffectiveAndCashIsRequired()) {
                    Snackbar.make(
                        orderDetailView,
                        "Digita el monto a pagar en efectivo",
                        Snackbar.LENGTH_SHORT
                    ).show()
                }
                else if(getTrueIfPaymentMethodIsEffectiveAndCashIsRequired()) {
                    Snackbar.make(
                        orderDetailView,
                        "Digita el monto a pagar en efectivo",
                        Snackbar.LENGTH_SHORT
                    ).show()
                }

                else {
                    saveOrderViewModel.generateOrderForUser(
                        getTypeDelivery(),
                        totalValue,
                        DateTimeHelper.parseFinalDate("$dateSelected $startTime"),
                        DateTimeHelper.parseFinalDate("$dateSelected $endTime"),
                        paymentMethodId,
                        DELIVERY_CHARGE_BY_DEFAULT,
                        customerAmount
                    )
                }
            }

        }

        tvIsDelivery.setOnClickListener {
            isDelivery = true
            tvIsDelivery.setTextColor(ContextCompat.getColor(context!!, R.color.colorPrimary))
            tvInStore.setTextColor(ContextCompat.getColor(context!!, R.color.text_gray_color))
            lineDelivery.visibility = View.VISIBLE
            lineRecojo.visibility = View.GONE
            containerDelivery.visibility = View.VISIBLE
            containerRecojo.visibility = View.INVISIBLE
            startTime = ""
            endTime = ""
        }

        tvInStore.setOnClickListener {
            isDelivery = false
            tvIsDelivery.setTextColor(ContextCompat.getColor(context!!, R.color.text_gray_color))
            tvInStore.setTextColor(ContextCompat.getColor(context!!, R.color.colorPrimary))
            lineDelivery.visibility = View.GONE
            lineRecojo.visibility = View.VISIBLE
            containerDelivery.visibility = View.INVISIBLE
            containerRecojo.visibility = View.VISIBLE
            startTime = DateTimeHelper.parseCalendarToSaveOrder(Calendar.getInstance())
            endTime =  DateTimeHelper.parseCalendarToSaveOrder(Calendar.getInstance())
        }

        tvSelectDay.setOnClickListener {
            obtenerFecha()
        }

        tvSelectPayMethod.setOnClickListener{
            paymentMethodsList = PreferencesManager.getInstance().getEstablishmentSelected().paymentMethods.toMutableList()
            if (paymentMethodsList.isNotEmpty()) {
                getPaymentMethodsDialog(paymentMethodsList)
            } else {
                //VALIDAR CUANDO NO EXITE MÉTODO, AL GENERAR IGUAL ME INDICA QUE NO HE SELECCIONADO NINGÚN MÉTODO DE
                // PAGO PORQUE MI ESTABLECIMIENTO NO TIENE UNA LISTA DE MÉTODOS
                paymentMethodId = PAYMENT_METHOD_ID_FOR_RECOJO_EN_TIENDA
                Snackbar.make(
                    orderDetailView,
                    "No existen métodos de pago para este establecimiento, se va a guardar por defecto en efectivo",
                    Snackbar.LENGTH_SHORT
                ).show()
            }
        }

        tvInputAmountToPay.setOnClickListener{
            mostrarVentanaIngresoMontoEfectivo()
        }
    }

    private fun getTrueIfPaymentMethodIsEffectiveAndCashIsRequired(): Boolean {
        return (tvPaymentMethodSelected.text.contains(CASH_AS_PAYMENT_METHOD) && customerAmount==0.0 )
    }

    private fun info() {
        Log.e("error", "tvPaymentMethodSelected: "+ tvPaymentMethodSelected.text.toString())
        Log.e("error", "customerAmount: "+ customerAmount.toString())
    }

    private fun getTrueIfPaymentMethodIsNotSelected(): Boolean {
        return if(paymentMethodId == 0 ){
             tvPaymentMethodSelected.text.toString().equals(NO_PAYMENT_METHOD_IS_SELECTED)
        }else{
            false
        }
    }

    private fun obtenerFecha() {
        val calendar: Calendar = Calendar.getInstance()
        val day = calendar.get(Calendar.DAY_OF_MONTH)
        val month = calendar.get(Calendar.MONTH)
        val year = calendar.get(Calendar.YEAR)
        val datePickerDialog =
            DatePickerDialog(context!!, this, year, month, day)
        actualDay = day
        datePickerDialog.datePicker.minDate = calendar.timeInMillis
        datePickerDialog.datePicker.maxDate = calendar.timeInMillis + (1000 * 60 * 60 * 24 * 2)
        datePickerDialog.show()
        //aqui restablecemos el valor horario a cero!!
        startTime = ""
        tvScheduleSelected.text = ""
    }

    override fun onDateSet(view: DatePicker?, year: Int, month: Int, dayOfMonth: Int) {
        val date = year.toString() + "-" + (month + 1) + "-" + dayOfMonth
        val parseDate = DateTimeHelper.parseCompleteDate(date)
        tvDaySelected.text = parseDate
        dateSelected = date
        isToday = actualDay == dayOfMonth
    }

    private fun getTypeDelivery(): String {
        return if (isDelivery) {
            "E"
        } else {
            "R"
        }
    }

    private fun getScheduleDialog(listRanges: List<ScheduleView>) {
        val builder = AlertDialog.Builder(context!!)
        builder.setTitle("Selecciona tu horario de entrega")
        val values = ArrayAdapter<String>(context!!, R.layout.row_schedule_select)

        listRanges.forEach {
            values.add(it.range)
        }

        builder.setAdapter(values, DialogInterface.OnClickListener { dialog, which ->
            val scheduleSelected = values.getItem(which)
            saveOrderViewModel.scheduleSelected = scheduleSelected.toString()
            tvScheduleSelected.text = scheduleSelected.toString()
            getStartTimeFromList(scheduleSelected.toString())
        })

        val dialog = builder.create()
        dialog.show()
    }

    private fun getPaymentMethodsDialog(paymentMethodsList: List<PaymentMethodView>) {
        val builder = AlertDialog.Builder(context!!)
        builder.setTitle("Selecciona un método de pago")
        val values = ArrayAdapter<String>(context!!, R.layout.row_schedule_select)

        paymentMethodsList.forEach {
            values.add(it.description)
        }

        builder.setAdapter(values) { dialog, which ->
            val paymentMethodSelected = values.getItem(which)
            checkIfRequiredAmountToPayInCash(paymentMethodSelected)
            saveOrderViewModel.paymentMethodSelected = paymentMethodSelected.toString()
            tvPaymentMethodSelected.text =  String.format("%s - %s", paymentMethodSelected.toString(), findCustomerMessage(paymentMethodSelected.toString()))
            getPaymentMethodIdFromList(paymentMethodSelected.toString())
        }

        val dialog = builder.create()
        dialog.show()
    }

    private fun getStartTimeFromList(valueSelected: String) {
        PreferencesManager.getInstance().getEstablishmentSelected().storeScheduleShipping.forEach {
            if (it.range == valueSelected) {
                startTime = it.startTime!!
                endTime = it.endTime!!
            }
        }
    }

    private fun getPaymentMethodIdFromList(valueSelected: String) {
        PreferencesManager.getInstance().getEstablishmentSelected().paymentMethods.forEach {
            if (it.description == valueSelected){
                paymentMethodId = it.paymentMethodId
            }
        }

    }

    private fun findCustomerMessage(paymentSelected: String): String{
        var message = ""
        paymentMethodsList.forEach {
            if (paymentSelected == it.description){
                message = it.customerMessage
            }
        }
        return message
    }

    private fun observerGenerateOrder() {
        saveOrderViewModel.generateOrderResult.observe(viewLifecycleOwner, Observer {
            when (it.status) {
                Status.SUCCESS -> {
                    listener!!.hideLoading()
                    saveOrderViewModel.deleteOrders()
                    if(isDelivery){
                        getSuccessDeliveryDialog().show()
                    }else{
                        getSuccessDialog().show()
                    }
                }

                Status.ERROR -> {
                    listener!!.hideLoading()
                    Snackbar.make(
                        orderDetailView,
                        "Ha ocurrido un error al generar la orden",
                        Snackbar.LENGTH_SHORT
                    ).show()
                }

                Status.LOADING -> {
                    listener!!.showLoading()
                }
            }
        })
    }

    private fun validateDateForDelivery(
        list: List<ScheduleView>,
        isToday: Boolean
    ): List<ScheduleView> {

        val newList = mutableListOf<ScheduleView>()
        if (isToday) {
            list.forEach {
                val format = SimpleDateFormat("HH:mm")
                val actualHour = format.format(Calendar.getInstance().time)

                val dateFromList = format.parse(it.startTime!!)
                val parseDateFromList = format.format(dateFromList!!)
                when {
                    actualHour < parseDateFromList -> {
                        newList.add(it)
                    }
                    actualHour.compareTo(parseDateFromList) == 0 -> {
                        newList.add(it)
                    }
                    else -> {
                    }
                }
            }
        } else {
            list.forEach {
                newList.add(it)
            }
        }

        return newList
    }

    private fun getSuccessDeliveryDialog(): Dialog {
        PreferencesManager.getInstance().cleanEstablishment()
        return AlertDialog.Builder(context!!)
            .setIcon(R.drawable.ic_check)
            .setCancelable(false)
            .setTitle("Orden Generada con éxito")
            .setMessage("Su orden será enviada a la brevedad por el proveedor seleccionado")
            .setPositiveButton(
                "Aceptar",
                DialogInterface.OnClickListener { dialog, whichButton ->
                    listener!!.onBackPressed()
                })
            .create()

    }

    private fun getSuccessDialog(): Dialog {
        PreferencesManager.getInstance().cleanEstablishment()
        return AlertDialog.Builder(context!!)
            .setIcon(R.drawable.ic_check)
            .setCancelable(false)
            .setTitle("Orden Generada con éxito")
            .setMessage("Su orden será preparada a la brevedad por el proveedor seleccionado")
            .setPositiveButton(
                "Aceptar",
                DialogInterface.OnClickListener { dialog, whichButton ->
                    listener!!.onBackPressed()
                })
            .create()

    }

    interface OnFragmentInteractionListener {
        fun showLoading()
        fun hideLoading()
        fun onBackPressed()
    }

    private fun checkIfRequiredAmountToPayInCash(paymentMethodSelected: String?) {
        customerAmount = 0.0
        if(paymentMethodSelected == CASH_AS_PAYMENT_METHOD) {
            boxAmountToPay.visibility = View.VISIBLE
            tvInputAmountToPay.visibility = View.VISIBLE
            tvAmountToPayInCash.visibility = View.VISIBLE
        }
        else {
            boxAmountToPay.visibility = View.GONE
            tvInputAmountToPay.visibility = View.GONE
            tvAmountToPayInCash.visibility = View.GONE
        }
    }

    private fun mostrarVentanaIngresoMontoEfectivo() {
        val dialog = Dialog(context!!)
        dialog.setCancelable(true)
        dialog.setContentView(R.layout.layout_monto_a_pagar)
        dialog.window?.setLayout(
            ViewGroup.LayoutParams.MATCH_PARENT,
            ViewGroup.LayoutParams.WRAP_CONTENT
        )
        dialog.window?.setBackgroundDrawableResource(android.R.color.transparent)

        val boxPrice = dialog.findViewById(R.id.boxPrice) as TextInputLayout
        val valAmountInCash = dialog.findViewById(R.id.etAmountInCash) as TextInputEditText
        val btnAddCashAmount = dialog.findViewById(R.id.btnAddCashAmount) as Button

        valAmountInCash.addDecimalLimiter(2)
        btnAddCashAmount.setOnClickListener{
            if(valAmountInCash.text.isNullOrEmpty()) {
                boxPrice.error="Digita una cantidad válida"
                tvAmountToPayInCash.text = CUSTOMER_AMOUNT_WHEN_PAYMENT_METHOD_IS_NOT_CASH.toString()
            }
            else if(valAmountInCash.text.toString().toDouble()< tvTotalCompraInDouble) {
                boxPrice.error="Digita una cantidad mayor o igual a "+tvTotalCompra.text.toString()
                tvAmountToPayInCash.text = CUSTOMER_AMOUNT_WHEN_PAYMENT_METHOD_IS_NOT_CASH.toString()
            }
            else {
                hideKeyboardAfterClickOnButton()
                customerAmount = valAmountInCash.text.toString().toDouble()
                tvAmountToPayInCash.text ="S/ " + customerAmount.toString()
                dialog.dismiss()
            }
        }
        dialog.show()
    }


    fun EditText.addDecimalLimiter(maxLimit: Int = 2) {

        this.addTextChangedListener(object : TextWatcher {

            override fun afterTextChanged(s: Editable?) {
                val str = this@addDecimalLimiter.text!!.toString()
                if (str.isEmpty()) return
                val str2 = decimalLimiter(str, maxLimit)

                if (str2 != str) {
                    this@addDecimalLimiter.setText(str2)
                    val pos = this@addDecimalLimiter.text!!.length
                    this@addDecimalLimiter.setSelection(pos)
                }
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

            }

        })
    }

    fun EditText.decimalLimiter(string: String, MAX_DECIMAL: Int): String {

        var str = string
        if (str[0] == '.') str = "0$str"
        val max = str.length

        var rFinal = ""
        var after = false
        var i = 0
        var up = 0
        var decimal = 0
        var t: Char

        val decimalCount = str.count{ ".".contains(it) }

        if (decimalCount > 1)
            return str.dropLast(1)

        while (i < max) {
            t = str[i]
            if (t != '.' && !after) {
                up++
            } else if (t == '.') {
                after = true
            } else {
                decimal++
                if (decimal > MAX_DECIMAL)
                    return rFinal
            }
            rFinal += t
            i++
        }
        return rFinal
    }

    fun hideKeyboard() {
        val imm =
            activity!!.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(view!!.windowToken, 0);
    }

    fun isOpenKeyboard(): Boolean {
        val imm =
            activity!!.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        return imm.isAcceptingText
    }

    fun hideKeyboardAfterClickOnButton()  {
        if (isOpenKeyboard()){
            hideKeyboard()
        }
    }
}