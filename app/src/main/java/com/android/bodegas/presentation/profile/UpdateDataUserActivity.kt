package com.android.bodegas.presentation.profile

import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.lifecycle.Observer
import com.android.bodegas.R
import com.android.bodegas.data.PreferencesManager
import com.android.bodegas.domain.util.Status
import com.android.bodegas.presentation.BaseActivity
import com.android.bodegas.presentation.register_user.RegistroViewModel
import com.android.bodegas.presentation.register_user.model.DepartmentView
import com.android.bodegas.presentation.register_user.model.DistrictView
import com.android.bodegas.presentation.register_user.model.ProvinceView
import com.google.android.material.snackbar.Snackbar
import com.google.android.material.textfield.TextInputEditText
import com.google.android.material.textfield.TextInputLayout
import org.koin.androidx.viewmodel.ext.android.viewModel
import kotlinx.android.synthetic.main.activity_update_data_user.*

class UpdateDataUserActivity  : BaseActivity() {

    private val profileViewModel: ProfileViewModel by viewModel()
    var latitudeSelected: String = ""
    var longitudeSelected: String = ""

    private val registerViewModel: RegistroViewModel by viewModel()

    private var departmentViewList: MutableList<DepartmentView> = mutableListOf()
    private var stringDeparmentList = ArrayList<CharSequence>()

    private var provinceViewList: MutableList<ProvinceView> = mutableListOf()
    private var stringProvinceList = ArrayList<CharSequence>()

    private var districtViewList: MutableList<DistrictView> = mutableListOf()
    private var stringDistrictList = ArrayList<CharSequence>()

    var selectedDepartmentId: String = ""
    var selectedProvinceId: String = ""

    val user2 = PreferencesManager.getInstance().getUser()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_update_data_user)
        asa()
        getDepartments()
        observeUpdateDataUser()
        chargeData()
        updateData()
        updateAddress()
        cancelaryRegresar()
    }

    private fun chargeData() {
        val user = PreferencesManager.getInstance().getUser()
        direccionInput.setText(user.address)
      //  country
        departamentoinput.setText(user.departament)
        distritoinput.setText(user.district)
        input_dni.setText(user.dni)
        input_ape_materno.setText(user.lastNameMaternal)
        input_ape_paterno.setText(user.lastNamePaternal)
        inputName.setText(user.name)
        input_phone.setText(user.phoneNumber)
        provinciainput.setText(user.province)
       // ruc
       // updt
        inputreferencia.setText(user.urbanization)
        latitudeSelected = user.latitude.toString()
        longitudeSelected = user.longitude.toString()

    }

    private fun updateData(){
        btnGuardarCambios.setOnClickListener{
            if(validacionCampos()){
                val updateBody = UpdateCustomerBody(
                    direccionInput.text.toString(),
                    "PE",
                    departamentoinput.text.toString(),
                    distritoinput.text.toString(),
                    input_dni.text.toString(),
                    input_ape_materno.text.toString(),
                    input_ape_paterno.text.toString(),
                    inputName.text.toString(),
                    input_phone.text.toString(),
                    provinciainput.text.toString(),
                    "12345678911",
                    input_dni.text.toString(),
                    inputreferencia.text.toString(),
                    latitudeSelected.toDouble(),
                    longitudeSelected.toDouble()
                )
                Log.d("db","ID: "+ PreferencesManager.getInstance().getUser().customerId)
                Log.d("db","data enviada: "+ updateBody.toString())
                profileViewModel.updateDataUser(PreferencesManager.getInstance().getUser().customerId, updateBody)
            }
        }
    }

    private fun observeUpdateDataUser(){
        profileViewModel.updateUserEntity.observe( this, Observer {
            when(it.status){
                Status.SUCCESS -> {
                    Snackbar.make(
                        updateProfileview,
                        "Datos actualizados correctamente.",
                        Snackbar.LENGTH_LONG
                    ).show()
                    PreferencesManager.getInstance().cleanUpdateUser()
                    finish()
                }
                Status.ERROR -> {

                    Snackbar.make(
                        updateProfileview,
                        "Error al actualizar, intenta nuevamente.",
                        Snackbar.LENGTH_LONG
                    ).show()
                }
                Status.LOADING -> {
                }
            }
        })
    }



    fun updateAddress() {
        boton_set_direccion.setOnClickListener{
            saveDataInLocalStorage()
            nextActivity(null, UpdateDataAddressActivity::class.java, true)
        }
    }

    override fun onResume() {
        super.onResume()
        loadDataFromLocalStorage()
    }

    private fun loadDataFromLocalStorage() {
        val registerBody = PreferencesManager.getInstance().getBodyUpdateUser()
        if (!registerBody.address.equals("")) direccionInput.setText(registerBody.address)
        if (!registerBody.name.equals("")) inputName.setText(registerBody.name)
        if (registerBody.departament != "") departamentoinput.setText(registerBody.departament)
        if (registerBody.district != "") distritoinput.setText(registerBody.district)
        if (registerBody.dni != "") input_dni.setText(registerBody.dni)
        if (registerBody.lastNameMaternal != "") input_ape_materno.setText(registerBody.lastNameMaternal)
        if (registerBody.lastNamePaternal != "") input_ape_paterno.setText(registerBody.lastNamePaternal)
        if (registerBody.latitude!=0.0) latitudeSelected = registerBody.latitude.toString()
        if (registerBody.longitude!=0.0) longitudeSelected = registerBody.longitude.toString()
        if (registerBody.name != "") inputName.setText(registerBody.name)
        if (registerBody.phoneNumber != "") input_phone.setText(registerBody.phoneNumber)
        if (registerBody.province != "") provinciainput.setText(registerBody.province)
        if (registerBody.urbanization != "") inputreferencia.setText(registerBody.urbanization)
    }

    private fun saveDataInLocalStorage() {
        val updateBody = UpdateCustomerBody(
            direccionInput.text.toString(),
            "PE",
            departamentoinput.text.toString(),
            distritoinput.text.toString(),
            input_dni.text.toString(),
            input_ape_materno.text.toString(),
            input_ape_paterno.text.toString(),
            inputName.text.toString(),
            input_phone.text.toString(),
            provinciainput.text.toString(),
            "12345678911",
            "2020",
            inputreferencia.text.toString(),
            latitudeSelected.toDouble(),
            longitudeSelected.toDouble()
        )
        PreferencesManager.getInstance().saveBodyUpdateUser(updateBody)
    }

    private fun asa(){
        boton_set_departamento.setOnClickListener {
            showDialogDepartments()
        }
        boton_set_provincia.setOnClickListener {
            showDialogProvincia()
        }
        boton_set_distrito.setOnClickListener {
            showDialogDistrito()
        }
    }

    private fun validateDataToRegister(): Boolean {
        return validarAlerts(departamentoinput, departamentobox) &&
                validarAlerts(provinciainput, provinciabox) &&
                validarAlerts(distritoinput, distritobox) &&
                extensionValidation(inputreferencia, boxreferencia, 5)
    }

    private fun extensionValidation(
        campo_input: TextInputEditText,
        inputTitle: TextInputLayout,
        minimunLenght: Int
    ): Boolean {
        val campo = campo_input.text.toString()
        inputTitle.error = null
        if ((campo.isEmpty()) || campo.length < minimunLenght) {
            inputTitle.error = "Introduce una referencia más extensa."
            return false
        }
        return true
    }

    private fun validarAlerts(
        campo_input: TextInputEditText,
        title_input: TextInputLayout
    ): Boolean {
        val campo = campo_input.text.toString()
        title_input.error = null
        if (campo == "Seleccionar") {
            title_input.error = "Selecciona un item de la lista."
            return false
        }
        return true
    }


    private fun showDialogDepartments() {
        lateinit var dialog: AlertDialog
        val array: Array<CharSequence> =
            stringDeparmentList.map { i -> i as CharSequence }.toTypedArray()
        val builder = AlertDialog.Builder(this)
        var selectedValue: CharSequence = ""
        builder.setTitle("Elige un departamento.")
        builder.setSingleChoiceItems(array, -1)
        { _, which ->
            selectedValue = array[which]
            departamentoinput.setText(selectedValue)
            dialog.dismiss()
            boton_set_provincia.isEnabled = true
            provinciainput.setText("Seleccionar")
            distritoinput.setText("Seleccionar")

            for (i in departmentViewList) {
                if (i.name == selectedValue) {
                    selectedDepartmentId = i.id
                }
            }
            getProvinces()
        }
        dialog = builder.create()
        dialog.show()
    }

    private fun getProvinces() {
        registerViewModel.provinces.observe(this, Observer {
            when (it.status) {
                Status.SUCCESS -> {
                    provinceViewList = it.data.toMutableList()
                    getProvinceStringsArray()
                    // hideLoading()
                }
                Status.ERROR -> {
                    Toast.makeText(
                        applicationContext,
                        "Ocurrió un error al cargar las provincias",
                        Toast.LENGTH_LONG
                    ).show()
                    // hideLoading()
                }
                Status.LOADING -> {
                    // showLoading()
                }
            }
        })
        registerViewModel.getProvinces()
    }

    private fun getDepartments() {
        registerViewModel.departments.observe(this, Observer {
            when (it.status) {
                Status.SUCCESS -> {
                    departmentViewList = it.data.toMutableList()
                    getDepartmentStringsArray()
                    //   hideLoading()
                }
                Status.ERROR -> {
                    //  hideLoading()
                    Snackbar.make(
                        updateProfileview,
                        "Ocurrió un error al cargar los departamentos",
                        Snackbar.LENGTH_LONG
                    ).show()
                }

                Status.LOADING -> {
                    //   showLoading()
                }
            }
        })
        registerViewModel.getDepartments()
    }

    private fun getDepartmentStringsArray() {
        stringDeparmentList.clear()
        departmentViewList.forEach {
            stringDeparmentList.add(it.name)
        }
    }

    private fun getProvinceStringsArray() {
        stringProvinceList.clear()
        provinceViewList.forEach {
            if (it.department_id == selectedDepartmentId) {
                stringProvinceList.add(it.name)
            }
        }
    }

    private fun showDialogProvincia() {
        lateinit var dialog: AlertDialog
        val array: Array<CharSequence> =
            stringProvinceList.map { i -> i as CharSequence }.toTypedArray()
        val builder = AlertDialog.Builder(this)
        builder.setTitle("Elige una provincia.")
        var selectedValue: CharSequence = ""
        builder.setSingleChoiceItems(array, -1) { _, which ->
            selectedValue = array[which]
            provinciainput.setText(selectedValue)
            dialog.dismiss()

            boton_set_distrito.isEnabled = true
            distritoinput.setText("Seleccionar")

            for (i in provinceViewList) {
                if (i.name == selectedValue) {
                    selectedProvinceId = i.id
                }
            }
            obtenerDistritos()
        }
        dialog = builder.create()
        dialog.show()
    }

    private fun obtenerDistritos() {
        registerViewModel.districts.observe(this, Observer {
            when (it.status) {
                Status.SUCCESS -> {
                    districtViewList = it.data.toMutableList()
                    getDistrictsStringsArray()
                    //  hideLoading()

                }
                Status.ERROR -> {
                    Snackbar.make(
                        updateProfileview,
                        "Ocurrió un error al cargar los distritos",
                        Snackbar.LENGTH_LONG
                    ).show()
                    //   hideLoading()
                }
                Status.LOADING -> {
                    //  showLoading()
                }
            }
        })
        registerViewModel.getDistricts()
    }

    private fun getDistrictsStringsArray() {
        stringDistrictList.clear()
        districtViewList.forEach {
            if (it.department_id == selectedDepartmentId && it.province_id == selectedProvinceId) {
                stringDistrictList.add(it.name)
            }
        }
    }

    private fun showDialogDistrito() {
        lateinit var dialog: AlertDialog
        val array: Array<CharSequence> =
            stringDistrictList.map { i -> i as CharSequence }.toTypedArray()
        val builder = AlertDialog.Builder(this)
        builder.setTitle("Elige un distrito.")
        var selectedValue: CharSequence = ""
        builder.setSingleChoiceItems(array, -1) { _, which ->
            selectedValue = array[which]
            distritoinput.setText(selectedValue)
            dialog.dismiss()
        }
        dialog = builder.create()
        dialog.show()
    }

    fun validacionCampos(): Boolean{
        return validarNombre() && validarApeMaterno() && validarApePaterno() && validarNumeroIdentidad() && validarNumeroTelefono() && validateDataToRegister()
    }

    private fun validarNombreInt(): Boolean {
        return  (inputName.text.toString().isNotEmpty() &&  inputName.text.toString().length >=2)
    }

    private fun validarNombre(): Boolean {
        if (!validarNombreInt()) boxBusinessName.error="Digita un nombre de minimo 2 caracteres."
        return validarNombreInt()
    }

    private fun validarApeMaternoInt(): Boolean {
        return  (input_ape_materno.text.toString().isNotEmpty() &&  input_ape_materno.text.toString().length >=2)
    }

    private fun validarApeMaterno(): Boolean {
        if (!validarApeMaternoInt()) box_ape_materno.error="Digita un apellido materno de mínimo 2 caracteres."
        return validarApeMaternoInt()
    }

    private fun validarApePaternoInt(): Boolean {
        return  (input_ape_paterno.text.toString().isNotEmpty() &&  input_ape_paterno.text.toString().length >=2)
    }

    private fun validarApePaterno(): Boolean {
        if (!validarApePaternoInt()) box_ape_paterno.error="Digita un apellido paterno de mínimo 2 caracteres."
        return validarApePaternoInt()
    }

    private fun validarNumeroIdentidadInt(): Boolean {
        return  (input_dni.text.toString().isNotEmpty() &&  input_dni.text.toString().length.equals(8))
    }

    private fun validarNumeroIdentidad(): Boolean {
        if (!validarNumeroIdentidadInt()) input_dni.error="Digita un dni de 8 caracteres."
        return validarNumeroIdentidadInt()
    }

    private fun validarNumeroTelefonoInt(): Boolean {
        return  (input_phone.text.toString().isNotEmpty() &&  input_phone.text.toString().length.equals(9))
    }

    private fun validarNumeroTelefono(): Boolean {
        if (!validarNumeroTelefonoInt()) input_phone.error="Digita un teléfono de 9 caracteres."
        return validarNumeroTelefonoInt()
    }

    private fun cancelaryRegresar(){
        btnCancelarCambios.setOnClickListener{
            PreferencesManager.getInstance().cleanUpdateUser()
           /* val intent = Intent(this, MainActivity::class.java)
            startActivity(intent)*/
            finish()
        }
    }

}
