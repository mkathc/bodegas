package com.android.bodegas.presentation.orders.mapper

import com.android.bodegas.domain.customerOrders.OrderDetails
import com.android.bodegas.presentation.Mapper
import com.android.bodegas.presentation.orders.model.OrdersItemView


class OrderDetailsViewMapper : Mapper<OrdersItemView, OrderDetails> {
    override fun mapToView(type: OrderDetails): OrdersItemView {
        return OrdersItemView(
            type.orderDetailOrderDetailId,
            type.orderDetailUnitMeasure,
            type.orderDetailQuantity,
            type.orderDetailPrice,
            getStatus(type.orderDetailStatus),
            type.orderDetailObservation,
            type.orderDetailSubTotal,
            type.storeProductStoreProductId,
            type.storeProductPrice,
            type.storeProductStatus,
            type.productTemplateProductTemplateId,
            type.productTemplateCode,
            type.productTemplateName,
            type.productTemplateDescription,
            type.productTemplateUnitMeasure,
            type.productTemplateStatus,
            type.productTemplatePathImage
        )
    }

    private fun getStatus(value:String):String{
        return if(value == "N"){
            "No atendido"
        }else{
            "Atendido"
        }
    }
}
