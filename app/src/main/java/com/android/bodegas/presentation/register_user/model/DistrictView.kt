package com.android.bodegas.presentation.register_user.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class DistrictView(
    val id: String,
    val name: String,
    val province_id: String,
    val department_id: String
) : Parcelable
