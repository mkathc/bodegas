package com.android.bodegas.presentation.login

import com.google.gson.annotations.SerializedName

data class ForgetPassDTO(
    @SerializedName("email")
    var email: String
)