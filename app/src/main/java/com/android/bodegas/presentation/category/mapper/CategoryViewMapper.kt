package com.android.bodegas.presentation.category.mapper

import com.android.bodegas.domain.category.Category
import com.android.bodegas.presentation.Mapper
import com.android.bodegas.presentation.category.model.CategoryView

class CategoryViewMapper : Mapper<CategoryView, Category> {
    override fun mapToView(type: Category): CategoryView {
        return CategoryView(
            type.id,
            type.description,
            type.name,
            type.pathImage
        )
    }

}