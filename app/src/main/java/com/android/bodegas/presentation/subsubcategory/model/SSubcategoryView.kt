package com.android.bodegas.presentation.subsubcategory.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class SSubcategoryView(
    val id: Int,
    val description: String,
    val name: String,
    val pathImage:String?=""
) : Parcelable