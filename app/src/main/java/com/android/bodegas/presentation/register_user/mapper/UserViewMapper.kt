package com.android.bodegas.presentation.register_user.mapper

import com.android.bodegas.domain.user.create.model.User
import com.android.bodegas.presentation.Mapper
import com.android.bodegas.presentation.register_user.model.UserView

open class UserViewMapper :
    Mapper<UserView, User> {

    override fun mapToView(type: User): UserView {
        return UserView(
            type.customerId,
            type.creationDate,
            type.creationUser,
            type.departament,
            type.district,
            type.dni,
            type.email,
            type.address,
            type.lastNameMaternal,
            type.lastNamePaternal,
            type.latitude,
            type.longitude,
            type.name,
            type.phoneNumber,
            type.province,
            type.ruc,
            type.status,
            type.urbanization,
            type.pathImage
        )
    }
}