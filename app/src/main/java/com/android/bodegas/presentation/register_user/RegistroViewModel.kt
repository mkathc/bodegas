package com.android.bodegas.presentation.register_user

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.android.bodegas.data.PreferencesManager
import com.android.bodegas.domain.department.GetDepartments
import com.android.bodegas.domain.district.GetDistricts
import com.android.bodegas.domain.province.GetProvinces
import com.android.bodegas.domain.user.create.RegisterNewUser
import com.android.bodegas.domain.user.create.model.User
import com.android.bodegas.domain.user.login.LoginTokenUser
import com.android.bodegas.domain.user.login.LoginUser
import com.android.bodegas.domain.util.Resource
import com.android.bodegas.domain.util.Status
import com.android.bodegas.presentation.login.LoginViewModel
import com.android.bodegas.presentation.login.mapper.UserLoginTokenViewMapper
import com.android.bodegas.presentation.login.model.UserLoginTokenView
import com.android.bodegas.presentation.register_user.mapper.*
import com.android.bodegas.presentation.register_user.model.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlin.coroutines.CoroutineContext

class RegistroViewModel constructor(
    private val registerUser: RegisterNewUser,
    private val getDepartments: GetDepartments,
    private val departmentViewMapper: DepartmentViewMapper,
    private val getProvinces: GetProvinces,
    private val provincesViewMapper: ProvinceViewMapper,
    private val getDistricts: GetDistricts,
    private val districtsViewMapper: DistrictViewMapper,
    private val userViewMapper: UserViewMapper,
    private val registerUserBodyViewMapper: RegisterUserBodyViewMapper,
    private val userLoginTokenViewMapper: UserLoginTokenViewMapper,
    private val loginUser: LoginUser
) :
    ViewModel(), CoroutineScope {

    override val coroutineContext: CoroutineContext
        get() = Dispatchers.IO

    private val _registerNewUser = MutableLiveData<Resource<UserLoginTokenView>>()
    val registerNewUser: LiveData<Resource<UserLoginTokenView>> = _registerNewUser

    private val _departments = MutableLiveData<Resource<List<DepartmentView>>>()
    val departments: LiveData<Resource<List<DepartmentView>>> = _departments

    private val _provinces = MutableLiveData<Resource<List<ProvinceView>>>()
    val provinces: LiveData<Resource<List<ProvinceView>>> = _provinces

    private val _districts = MutableLiveData<Resource<List<DistrictView>>>()
    val districts: LiveData<Resource<List<DistrictView>>> = _districts

    fun registerUser(registerUserBodyView: RegisterUserBodyView) {
        _registerNewUser.value = Resource(
            Status.LOADING,
            UserLoginTokenView("",""),
            ""
        )
        viewModelScope.launch {

            val registerResult = registerUser.registerUser(
                registerUserBodyViewMapper.mapToUseCase(registerUserBodyView)
            )

            if (registerResult.status == Status.SUCCESS) {
                val userLogintokenView = userLoginTokenViewMapper.mapToView(registerResult.data)
                PreferencesManager.getInstance().isInvited(false)
                PreferencesManager.getInstance().setCustomerLoginTokenSelected(userLogintokenView)
                _registerNewUser.value = Resource(registerResult.status, userLogintokenView, registerResult.message)
                //getDataUser(PreferencesManager.getInstance().getCustomerLoginTokenSelected().idEntity.toInt())
            } else {
                _registerNewUser.value = Resource(
                    registerResult.status,
                    userLoginTokenViewMapper.mapToView(registerResult.data),
                    "Error al registrar: " + registerResult.message
                )
            }
        }
    }

    fun getDepartments() {
        viewModelScope.launch {
            val departmentResult = getDepartments.getDepartments("", "")
            _departments.value = Resource(Status.LOADING, mutableListOf(), "")
            if (departmentResult.status == Status.SUCCESS) {
                val departmentResultData = departmentResult.data
                if (departmentResultData.isNotEmpty()) {
                    val departmentView =
                        departmentResultData.map { departmentViewMapper.mapToView(it) }
                    _departments.value = Resource(Status.SUCCESS, departmentView, "")
                } else {
                    _departments.value = Resource(Status.SUCCESS, mutableListOf(), "")
                }
            } else {
                _departments.value = Resource(Status.ERROR, mutableListOf(), "")
            }
        }
    }

    fun getProvinces() {
        viewModelScope.launch {
            val provinceResult = getProvinces.getProvince("", "", "")
            _provinces.value = Resource(Status.LOADING, mutableListOf(), "")
            if (provinceResult.status == Status.SUCCESS) {
                val provinceResultData = provinceResult.data
                if (provinceResultData.isNotEmpty()) {
                    val provinceView = provinceResultData.map { provincesViewMapper.mapToView(it) }
                    _provinces.value = Resource(Status.SUCCESS, provinceView, "")
                } else {
                    _provinces.value = Resource(Status.SUCCESS, mutableListOf(), "")
                }
            } else {
                _provinces.value = Resource(Status.ERROR, mutableListOf(), "")
            }
        }
    }

    fun getDistricts() {
        viewModelScope.launch {
            val districtResult = getDistricts.getDistrict("", "", "", "")
            _districts.value = Resource(Status.LOADING, mutableListOf(), "")
            if (districtResult.status == Status.SUCCESS) {
                val districtResultData = districtResult.data
                if (districtResultData.isNotEmpty()) {
                    val districtView = districtResultData.map { districtsViewMapper.mapToView(it) }
                    _districts.value = Resource(Status.SUCCESS, districtView, "")
                } else {
                    _districts.value = Resource(Status.SUCCESS, mutableListOf(), "")
                }
            } else {
                _districts.value = Resource(Status.ERROR, mutableListOf(), "")
            }
        }
    }

    private fun getDataUser(customerId: Int) {
        viewModelScope.launch {
            val loginResult = loginUser.loginUser(customerId)
            Log.e("error", "valor de loginResult: "+loginResult.message)
            if (loginResult.status == Status.SUCCESS) {
                PreferencesManager.getInstance().setUser(userViewMapper.mapToView(loginResult.data))
            }else{
            }
        }
    }
}