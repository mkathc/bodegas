package com.android.bodegas.presentation.supplier.adapter


import android.content.Context
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import com.android.bodegas.R
import com.android.bodegas.presentation.supplier.model.SupplierView
import com.bumptech.glide.Glide


class SupplierAdapter(
    private val viewHolderListener: SupplierViewHolder.ViewHolderListener
) : RecyclerView.Adapter<SupplierViewHolder>() {

    private val storesList = mutableListOf<SupplierView>()
    private lateinit var context : Context

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SupplierViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.row_suppliers, parent, false)
        context = parent.context
        return SupplierViewHolder(view, viewHolderListener)
    }

    override fun getItemCount(): Int {
        return storesList.size
    }

    override fun onBindViewHolder(holder: SupplierViewHolder, position: Int) {
        val store = storesList[position]
        holder.storeName.text = store.storeName
        holder.storeAddress.text = store.storeAddress
        holder.storeSchedule.text = store.storeScheduleOperation[0].range
        holder.bind(store)
      /*  if (store.hasDelivery) {
            holder.storeDelivery.visibility = View.VISIBLE
        } else {
            holder.storeDelivery.visibility = View.GONE
        }*/

        Glide.with(context).load(store.image).into(holder.imageStore)

    }

    fun setStoreList(poolList: List<SupplierView>) {
        this.storesList.clear()
        this.storesList.addAll(poolList)
    }


}