package com.android.bodegas.presentation.login.mapper

import com.android.bodegas.domain.user.login.model.UserLoginToken
import com.android.bodegas.presentation.Mapper
import com.android.bodegas.presentation.login.model.UserLoginTokenView

class UserLoginTokenViewMapper (): Mapper<UserLoginTokenView, UserLoginToken> {
    override fun mapToView(type: UserLoginToken): UserLoginTokenView {
        return UserLoginTokenView(
            type.idEntity,
            type.token
        )
    }
}