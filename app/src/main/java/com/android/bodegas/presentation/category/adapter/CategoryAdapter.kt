package com.android.bodegas.presentation.category.adapter

import com.android.bodegas.R
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.android.bodegas.presentation.category.model.CategoryView
import com.bumptech.glide.Glide

class CategoryAdapter (private val viewHolderListener: CategoryViewHolder.ViewHolderListener):
    RecyclerView.Adapter<CategoryViewHolder>()
{
    private val categoriesList = mutableListOf<CategoryView>()
    private lateinit var context: Context

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CategoryViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.row_categories, parent, false)
        context = parent.context
        return CategoryViewHolder(view, viewHolderListener)
    }

    override fun getItemCount(): Int {
        return categoriesList.size
    }

    override fun onBindViewHolder(holder: CategoryViewHolder, position: Int) {
        val class_ = categoriesList[position]
        holder.name_category.text = class_.name
        if(class_.pathImage!!.isNotEmpty()){
            Glide.with(context).load(class_.pathImage).into(holder.imageCategory)
        }else{
            holder.imageCategory.visibility = View.INVISIBLE
        }
    }

    fun setStoreList(poolList: List<CategoryView>) {
        this.categoriesList.clear()
        this.categoriesList.addAll(poolList)
    }
}