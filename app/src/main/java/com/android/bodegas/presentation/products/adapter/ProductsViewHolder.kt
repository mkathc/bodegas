package com.android.bodegas.presentation.products.adapter

import android.content.Context
import android.util.Log
import android.view.View
import android.widget.*
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.CircularProgressDrawable
import com.android.bodegas.R
import com.android.bodegas.data.PreferencesManager
import com.android.bodegas.presentation.products.model.ProductsView
import com.android.bodegas.presentation.supplier.model.SupplierView
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.row_products.view.*

class ProductsViewHolder(itemView: View, viewHolderListener: ViewHolderListener) :
    RecyclerView.ViewHolder(itemView) {
    val productName: TextView = itemView.tvNameProduct
    val productDescription: TextView = itemView.tvDescriptionProduct
    val productPrice: TextView = itemView.tvSubTotalPrice
    val imageProduct: ImageView = itemView.ivImageProduct
    val containerAdd: ConstraintLayout = itemView.btnAddToCart
    val containerAddQuantity: ConstraintLayout = itemView.btnAddQuantityToCart
    val btnAdd: Button = itemView.btnAdd
    val btnRemove: Button = itemView.btnRemove
    val tvQuantity: TextView = itemView.tvQuantity
    val tvQuantityUnit: TextView = itemView.tvUnit
    val tvStock: TextView = itemView.tvStock
    val holder: ViewHolderListener = viewHolderListener
    val tvUnidadMedida: TextView = itemView.tvUnidadMedida

    fun bind(product: ProductsView, context: Context, establishment: SupplierView) {
        productName.text = product.name
        productDescription.text = product.description
        productPrice.text = String.format("%s %.2f", "s/", product.price)
        tvUnidadMedida.text = product.unitMeasure.toUpperCase()

        if(product.stock.isNotEmpty()){
            tvStock.visibility = View.VISIBLE
            tvStock.text = product.stock
            containerAdd.isEnabled = false
        }else{
            tvStock.visibility = View.GONE
            containerAdd.isEnabled = true
        }

        if (product.imageProduct!!.isNotEmpty()) {

            val circularProgressDrawable = CircularProgressDrawable(context)
            circularProgressDrawable.strokeWidth = 5f
            circularProgressDrawable.centerRadius = 30f
            circularProgressDrawable.start()

            Glide.with(context)  // this
                .load(product.imageProduct)
                .placeholder(circularProgressDrawable)
                .error(context.getDrawable(R.drawable.ic_image_black_24dp))
                .into(imageProduct)
        }

        if(product.quantitySelected != 0.0){
            containerAdd.visibility = View.GONE
            containerAddQuantity.visibility = View.VISIBLE
            tvQuantity.text = product.quantitySelected.toString()
            tvQuantityUnit.text = product.unitMeasure
            if (product.unitMeasure == "Kg" || product.unitMeasure == "KG") {
                buttonWeightBehavior(tvQuantity.text.toString().toDouble())
            }else{
                buttonsBehavior(tvQuantity.text.toString().toDouble())
            }
        }else{
            containerAdd.visibility = View.VISIBLE
            containerAddQuantity.visibility = View.GONE
        }

        containerAdd.setOnClickListener {
            val actualEstablishment = PreferencesManager.getInstance()
                .getEstablishmentSelected()
            if(actualEstablishment.storeId == 0){
                if (PreferencesManager.getInstance().getIsInvited()) {
                    holder.onClickChangeProduct(layoutPosition, 0.0)
                } else {
                    containerAdd.visibility = View.GONE
                    containerAddQuantity.visibility = View.VISIBLE
                    tvQuantityUnit.text = product.unitMeasure
                    if (product.unitMeasure == "Kg" || product.unitMeasure == "KG") {
                        buttonWeightBehavior(1.0)
                    }
                    holder.onClickChangeProduct(layoutPosition, 1.0)
                    PreferencesManager.getInstance().setEstablishmentSelected(establishment)
                }
            }else{
                if (establishment.storeId != actualEstablishment.storeId) {
                    Toast.makeText(
                        context,
                        "Solo puede hacer la compra de una tienda a la vez, actualmente tiene un pedido de la tienda " +
                                actualEstablishment.storeName,
                        Toast.LENGTH_LONG
                    ).show()

                } else {
                    if (PreferencesManager.getInstance().getIsInvited()) {
                        holder.onClickChangeProduct(layoutPosition, 0.0)
                    } else {
                        containerAdd.visibility = View.GONE
                        containerAddQuantity.visibility = View.VISIBLE
                        tvQuantityUnit.text = product.unitMeasure
                        if (product.unitMeasure == "Kg" || product.unitMeasure == "KG") {
                            buttonWeightBehavior(1.0)
                        }
                        holder.onClickChangeProduct(layoutPosition, 1.0)
                    }
                }
            }

        }

        btnAdd.setOnClickListener {
            var value = tvQuantity.text.toString().toDouble()
            if (product.unitMeasure == "Kg" || product.unitMeasure == "KG") {
                value += 0.25
                tvQuantity.text = value.toString()
                buttonWeightBehavior(value)
                holder.onClickChangeProduct(layoutPosition, value)
            } else {
                value++
                tvQuantity.text = value.toString()
                buttonsBehavior(value)
                holder.onClickChangeProduct(layoutPosition, value)
            }

        }

        btnRemove.setOnClickListener {
            var value = tvQuantity.text.toString().toDouble()
            if (product.unitMeasure == "Kg" || product.unitMeasure == "KG") {
                value -= 0.25
                tvQuantity.text = value.toString()
                buttonWeightBehavior(value)
                holder.onClickChangeProduct(layoutPosition, value)
            } else {
                value--
                tvQuantity.text = value.toString()
                buttonsBehavior(value)
                holder.onClickChangeProduct(layoutPosition, value)
            }
        }

        itemView.setOnClickListener {
            holder.onClickProducts(
                layoutPosition
            )
        }
    }

    private fun buttonsBehavior(value: Double) {
        if (value <= 1.0) {
            btnRemove.isEnabled = false
            btnAdd.isEnabled = true
        } else {
            btnRemove.isEnabled = true
            btnAdd.isEnabled = true
        }
    }

    private fun buttonWeightBehavior(value: Double) {
        if (value <= 0.25) {
            btnRemove.isEnabled = false
            btnAdd.isEnabled = true
        } else {
            btnRemove.isEnabled = true
            btnAdd.isEnabled = true
        }
    }

    interface ViewHolderListener {
        fun onClickProducts(
            position: Int
        )

        fun onClickChangeProduct(
            position: Int,
            quantity: Double
        )

    }
}

