package com.android.bodegas.presentation

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.inputmethod.InputMethodManager


@SuppressLint("Registered")
open class BaseActivity : AppCompatActivity() {

    protected fun nextActivity(
        bundle: Bundle?,
        activity: Class<*>,
        destroy: Boolean
    ) {
        val intent = Intent(applicationContext, activity)
        if (bundle != null) {
            intent.putExtras(bundle)
        }

        startActivity(intent)

        if (destroy) {
            finish()
        }
    }

    open fun hideKeyboard() {
        val imm =
            applicationContext.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(currentFocus!!.windowToken, 0)
    }


    fun isOpenKeyboard(): Boolean {
        val imm =
            applicationContext.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        return imm.isAcceptingText
    }


}
