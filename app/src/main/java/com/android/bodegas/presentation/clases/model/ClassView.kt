package com.android.bodegas.presentation.clases.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ClassView(
    val id: Int,
    val description: String,
    val name: String,
    val pathImage:String?=""
) : Parcelable