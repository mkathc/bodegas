package com.android.bodegas.presentation.detail_orders

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.android.bodegas.domain.customerOrders.OrderDetailBody
import com.android.bodegas.domain.customerOrders.UpdateOrderStatus
import com.android.bodegas.domain.duplicate_order.DuplicateOrder
import com.android.bodegas.domain.duplicate_order.GetDuplicateOrder
import com.android.bodegas.domain.generate_order.GenerateOrder
import com.android.bodegas.domain.save_order.GetOrderByUser
import com.android.bodegas.domain.save_order.SaveOrderForSend
import com.android.bodegas.domain.util.DateTimeHelper
import com.android.bodegas.domain.util.Resource
import com.android.bodegas.domain.util.Status
import com.android.bodegas.presentation.orders.model.OrdersItemView
import com.android.bodegas.presentation.products.model.ProductsView
import com.android.bodegas.presentation.save_order.mapper.SaveOrderViewMapper
import com.android.bodegas.presentation.save_order.model.SaveOrderView
import kotlinx.coroutines.*
import java.util.*
import kotlin.coroutines.CoroutineContext

class DetailOrderViewModel constructor(
    private val getOrderByUser: GetOrderByUser,
    private val saveOrderForSend: SaveOrderForSend,
    private val saveOrderViewMapper: SaveOrderViewMapper,
    private val updateOrderStatus: UpdateOrderStatus,
    private val getDuplicateOrder: GetDuplicateOrder
) : ViewModel(), CoroutineScope {

    override val coroutineContext: CoroutineContext
        get() = Dispatchers.IO

    private val _orderByUser = MutableLiveData<Resource<Boolean>>()
    val orderByUser: LiveData<Resource<Boolean>> = _orderByUser

    private val _totalValue = MutableLiveData<Resource<Double>>()
    val totalValue: LiveData<Resource<Double>> = _totalValue

    private val _generateOrderResult = MutableLiveData<Resource<String>>()
    val generateOrderResult: LiveData<Resource<String>> = _generateOrderResult

    var scheduleSelected = String()

    private val _updateOrder = MutableLiveData<Resource<Boolean>>()
    val updateOrder: LiveData<Resource<Boolean>> = _updateOrder

    fun addProductsForOrder(ordersItemView: OrdersItemView, establishmentId: Int, orderId: Int) {
        viewModelScope.launch {
            val order = getOrderByUser.getOrderById(
                ordersItemView.storeProductStoreProductId + "-" + DateTimeHelper.parseCalendar(Calendar.getInstance())
            )
            if (order.status == Status.SUCCESS) {
                saveOrderForSend.updateOrder(
                    saveOrderViewMapper.mapToUseCase(
                        getValuerSaveOrder(
                            ordersItemView
                        )
                    )
                )
            } else {
                saveOrderForSend.saveOrder(
                    saveOrderViewMapper.mapToUseCase(
                        getValuerSaveOrder(
                            ordersItemView
                        )
                    )
                )
            }
            getTotalValue()
        }
    }

    private fun getValuerSaveOrder(ordersItemView: OrdersItemView): SaveOrderView {
        val saveOrderView = SaveOrderView("", 0, 0.0, 0.0, 0.0, "", "", "", "")
        saveOrderView.id = ""
        saveOrderView.storeProductId = ordersItemView.storeProductStoreProductId.toInt()
        saveOrderView.price = ordersItemView.orderDetailPrice.toDouble()
        saveOrderView.quantity = ordersItemView.orderDetailQuantity.toDouble()
        saveOrderView.subtotal = String.format("%.2f",ordersItemView.orderDetailPrice.toDouble() * ordersItemView.orderDetailQuantity.toDouble()).toDouble()
        saveOrderView.unitMeasure = ordersItemView.orderDetailUnitMeasure
        saveOrderView.observation = ""
        saveOrderView.name = ordersItemView.productTemplateName
        saveOrderView.imageProduct = ordersItemView.productTemplatePathImage
        return saveOrderView
    }

    fun getOrdersByUser(establishmentId: Int, orderId: Int) {
        viewModelScope.launch {

            val listOrder = getOrderByUser.getOrder()
            if (listOrder.data.isNotEmpty()) {
                _orderByUser.value = Resource(Status.ERROR, true , "")
            }else{
                _orderByUser.value = Resource(Status.SUCCESS, false , "")
            }
        }
    }

    private fun getTotalValue() {
        _totalValue.value = Resource(Status.LOADING, 0.0, "")
        viewModelScope.launch {
            var total = 0.0
            val totalList = getOrderByUser.getOrder()
            totalList.data.forEach {
                total += it.subtotal
            }
            _totalValue.value = Resource(Status.SUCCESS, total, "")
        }
    }

    fun updateStateOrder(orderId: Int, state:String, list:List<OrdersItemView>){
        _updateOrder.value = Resource(Status.LOADING, false, "")
        viewModelScope.launch {
            val sendList = mutableListOf<OrderDetailBody>()
            list.forEach {
                sendList.add(OrderDetailBody(it.orderDetailOrderDetailId.toInt(), "N"))
            }
            val result = updateOrderStatus.updateStaterOrder(orderId, state, sendList)

            if (result.status == Status.SUCCESS){
                _updateOrder.value = Resource(Status.SUCCESS, result.data, result.message)
            }else{
                _updateOrder.value = Resource(Status.ERROR, result.data, result.message)
            }
        }
    }
}