package com.android.bodegas.data.network.generate_order.mapper

import com.android.bodegas.data.network.Mapper
import com.android.bodegas.data.network.generate_order.model.ListOrderBody
import com.android.bodegas.data.repository.generate_order.model.ListOrderEntity

class GenerateOrderEntityMapper : Mapper<ListOrderEntity, ListOrderBody> {

    override fun mapFromRemote(type: ListOrderEntity): ListOrderBody {
        return ListOrderBody(
            type.observation,
            type.price,
            type.quantity,
            type.storeProductId,
            type.subtotal,
            type.unitMeasure
        )
    }

}