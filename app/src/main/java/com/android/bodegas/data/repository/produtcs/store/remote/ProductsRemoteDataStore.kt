package com.android.bodegas.data.repository.produtcs.store.remote

import com.android.bodegas.data.repository.produtcs.model.ProductsEntity
import com.android.bodegas.data.repository.produtcs.store.ProductsDataStore
import com.android.bodegas.domain.util.Resource


class ProductsRemoteDataStore constructor(private val productsRemote: ProductsRemote) :
    ProductsDataStore {

    override suspend fun getProducts(
        establishmentId: Int,
        supplierDocument: String,
        subCategoryId: Int
    ): Resource<List<ProductsEntity>> {
        return productsRemote.getProducts(establishmentId, supplierDocument, subCategoryId)
    }

    override suspend fun searchProducts(searchText: String, storeId:Int): Resource<List<ProductsEntity>> {
        return productsRemote.searchProducts(searchText,storeId)
    }

}