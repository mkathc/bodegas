package com.android.bodegas.data.network.subcategory.mapper

import com.android.bodegas.data.network.Mapper
import com.android.bodegas.data.network.subcategory.model.SubCategoryResponse
import com.android.bodegas.data.repository.subcategory.model.SubCategoryEntity

class SubcategoryEntityMapper : Mapper<SubCategoryResponse, SubCategoryEntity> {

    override fun mapFromRemote(type: SubCategoryResponse): SubCategoryEntity {
        return SubCategoryEntity(
            type.id,
            type.description,
            type.name,
            type.pathImage
        )
    }
}