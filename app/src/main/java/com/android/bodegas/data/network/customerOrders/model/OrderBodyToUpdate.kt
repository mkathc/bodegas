package com.android.bodegas.data.network.customerOrders.model

import com.android.bodegas.data.network.supplier.model.ScheduleResponse
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


class OrderBodyToUpdate (
    @SerializedName("state")
    val state: String,
    @SerializedName("updateUser")
    val updateUser: String,
    @SerializedName("stateOrderDetailList")
    val stateOrderDetailList: List<OrderDetailBodyToSend>
)