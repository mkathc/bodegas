package com.android.bodegas.data.network.duplicateorder.model

import com.android.bodegas.data.network.ResponseBodySuccess
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class DuplicateOrderBody : ResponseBodySuccess() {
    @SerializedName("data")
    @Expose
    var data: DuplicateOrderItem? = null
}


class DuplicateOrderItem(
    @SerializedName("duplicateOrder")
    @Expose
    var duplicateOrder: DuplicateOrderResponse
)