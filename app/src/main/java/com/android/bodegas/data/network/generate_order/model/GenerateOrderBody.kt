package com.android.bodegas.data.network.generate_order.model

import com.google.gson.annotations.SerializedName

class GenerateOrderBody(
    @SerializedName("creationUser")
    val creationUser: String,
    @SerializedName("deliveryType")
    val deliveryType: String,
    @SerializedName("establishmentId")
    val establishmentId: Int,
    @SerializedName("orderDetailRequest")
    val orderDetailRequest: List<ListOrderBody>,
    @SerializedName("shippingDateFrom")
    val shippingDateFrom: String,
    @SerializedName("shippingDateUntil")
    val shippingDateUntil: String,
    @SerializedName("total")
    val total: Double,
    @SerializedName("paymentMethodId")
    val paymentMethodId: Int,
    @SerializedName("deliveryCharge")
    val deliveryCharge:Double,
    @SerializedName("customerAmount")
    val customerAmount:Double
)
