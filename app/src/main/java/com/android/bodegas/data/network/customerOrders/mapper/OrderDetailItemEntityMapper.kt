package com.android.bodegas.data.network.customerOrders.mapper

import com.android.bodegas.data.network.Mapper
import com.android.bodegas.data.network.customerOrders.model.OrderDetailItem
import com.android.bodegas.data.repository.customerOrders.model.OrderDetailItemEntity

class OrderDetailItemEntityMapper : Mapper<OrderDetailItem, OrderDetailItemEntity> {

    override fun mapFromRemote(type: OrderDetailItem): OrderDetailItemEntity {
        return OrderDetailItemEntity(
            type.orderDetailId.toString(),
            type.unitMeasure,
            type.quantity.toString(),
            type.price.toString(),
            type.status,
            type.observation,
            type.subTotal.toString(),
            type.storeProduct.storeProductId.toString(),
            type.storeProduct.price.toString(),
            type.storeProduct.status,
            type.storeProduct.productTemplate.productTemplateId.toString(),
            type.storeProduct.productTemplate.code,
            type.storeProduct.productTemplate.name,
            type.storeProduct.productTemplate.description,
            type.storeProduct.productTemplate.unitMeasure,
            type.storeProduct.productTemplate.status,
            type.storeProduct.productTemplate.pathImage
        )
    }


}