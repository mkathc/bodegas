package com.android.bodegas.data.network.allPaymentMethods

import com.android.bodegas.data.network.allPaymentMethods.model.AllPaymentMethodsListBody
import retrofit2.Response
import retrofit2.http.GET

interface AllPaymentMethodsServices {

    @GET("/common/paymentmethods")
    suspend fun getAllPaymentMethods(): Response<AllPaymentMethodsListBody>
}