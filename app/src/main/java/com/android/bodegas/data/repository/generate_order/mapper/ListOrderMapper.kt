package com.android.bodegas.data.repository.generate_order.mapper

import com.android.bodegas.data.repository.Mapper
import com.android.bodegas.data.repository.generate_order.model.ListOrderEntity
import com.android.bodegas.data.repository.save_order.model.SaveOrderEntity

class ListOrderMapper : Mapper<SaveOrderEntity, ListOrderEntity> {

    override fun mapFromEntity(type: SaveOrderEntity): ListOrderEntity {
        return ListOrderEntity(
            "",
            type.price,
            type.quantity,
            type.storeProductId,
            type.subtotal,
            type.unitMeasure
        )
    }

    override fun mapToEntity(type: ListOrderEntity): SaveOrderEntity {
        return SaveOrderEntity(
            "type.id",
            type.storeProductId,
            type.price,
            type.quantity,
            type.subtotal,
            type.unitMeasure,
            type.observation,
           " type.name",
            "type.imageProduct"
        )
    }

}