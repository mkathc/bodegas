package com.android.bodegas.data.repository.sub_sub_category.store.remote

import com.android.bodegas.data.repository.sub_sub_category.model.GroupEntity
import com.android.bodegas.data.repository.sub_sub_category.store.SSubCategoryDataStore
import com.android.bodegas.domain.util.Resource


class SSubCategoryRemoteDataStore constructor(private val sSubCategoyRemote: SSubCategoryRemote) :
    SSubCategoryDataStore {

    override suspend fun getSSubCategory(
        establishmentId: Int,
        subCategoryId: Int
    ): Resource<GroupEntity> {
        return  sSubCategoyRemote.getSSubCategory(establishmentId, subCategoryId)
    }

}