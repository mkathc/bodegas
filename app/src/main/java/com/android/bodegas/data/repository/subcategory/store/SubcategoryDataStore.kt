package com.android.bodegas.data.repository.subcategory.store

import com.android.bodegas.data.repository.subcategory.model.SubCategoryEntity
import com.android.bodegas.domain.util.Resource

interface SubcategoryDataStore {
    suspend fun getSubcategories(
        establishmentId: Int,
        category_id: Int,
        page: String,
        size: String,
        sortBy: String
    ) : Resource<List<SubCategoryEntity>>
}