package com.android.bodegas.data.repository.departments.model

class DepartmentEntity (
    val id: String,
    val name: String
)