package com.android.bodegas.data.db.save_order.dao

import androidx.room.*
import com.android.bodegas.data.db.save_order.model.SaveOrderDb
import com.android.bodegas.data.db.user.model.UserDb

@Dao
abstract class SaveOrderDao {

    @Query(SaveOrderConstants.QUERY_ORDER)
    abstract suspend fun getOrder(): List<SaveOrderDb>

    @Query(SaveOrderConstants.QUERY_ORDER_BY_ID)
    abstract suspend fun getOrderById(orderId:String): SaveOrderDb

    @Query(SaveOrderConstants.DELETE_ORDER)
    abstract suspend fun deleteOrder()

    @Query(SaveOrderConstants.DELETE_ORDER_BY_ID)
    abstract suspend fun deleteOrderById(orderId: String)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract suspend fun insertOrder(saveOrderDb: SaveOrderDb)

    @Update
    abstract suspend fun updateOrder( saveOrderDb: SaveOrderDb)

}