package com.android.bodegas.data.repository.supplier.mapper

import com.android.bodegas.data.repository.Mapper
import com.android.bodegas.data.repository.supplier.model.PaymentMethodEntity
import com.android.bodegas.data.repository.supplier.model.ScheduleEntity
import com.android.bodegas.data.repository.supplier.model.SupplierEntity
import com.android.bodegas.domain.supplier.PaymentMethod
import com.android.bodegas.domain.supplier.Schedule
import com.android.bodegas.domain.supplier.Supplier


class SupplierMapper(
    private val scheduleMapper: ScheduleMapper,
    private val paymentMethodMapper: PaymentMethodMapper
) : Mapper<SupplierEntity, Supplier> {

    override fun mapFromEntity(type: SupplierEntity): Supplier {
        return Supplier(
            type.establishmentId,
            type.address,
            type.establishmentType,
            type.businessName,
            type.cardNumber,
            type.cardOperator,
            type.departament,
            type.district,
            type.dni,
            type.email,
            type.lastNameMaternal,
            type.lastNamePaternal,
            type.latitude,
            type.longitude,
            type.name,
            type.phoneNumber,
            type.province,
            type.ruc,
            type.status,
            type.urbanization,
            type.delivery,
            type.pathImage,
            type.deliveryCharge,
            getListSchedules(type.shippingSchedule),
            getListSchedules(type.operationSchedule),
            getListPaymentMethods(type.paymentMethod)
        )
    }

    override fun mapToEntity(type: Supplier): SupplierEntity {
        return SupplierEntity(
            type.establishmentId,
            type.address,
            type.establishmentType,
            type.businessName,
            type.cardNumber,
            type.cardOperator,
            type.departament,
            type.district,
            type.dni,
            type.email,
            type.lastNameMaternal,
            type.lastNamePaternal,
            type.latitude,
            type.longitude,
            type.name,
            type.phoneNumber,
            type.province,
            type.ruc,
            type.status,
            type.urbanization,
            type.delivery,
            type.pathImage,
            type.deliveryCharge,
            getListSchedulesFromUsesCase(type.shippingSchedule),
            getListSchedulesFromUsesCase(type.operationSchedule),
            getListPaymentMethodsFromUsesCase(type.paymentMethod)
        )
    }

    private fun getListSchedules(list: List<ScheduleEntity>): List<Schedule>{
        val listSchedule = mutableListOf<Schedule>()
        list.forEach {
            listSchedule.add(scheduleMapper.mapFromEntity(it))
        }
        return listSchedule
    }

    private fun getListPaymentMethods(list: List<PaymentMethodEntity>): List<PaymentMethod>{
        val listPaymentMethods = mutableListOf<PaymentMethod>()
        list.forEach {
            listPaymentMethods.add(paymentMethodMapper.mapFromEntity(it))
        }
        return listPaymentMethods
    }

    private fun getListSchedulesFromUsesCase(list: List<Schedule>): List<ScheduleEntity>{
        val listSchedule = mutableListOf<ScheduleEntity>()
        list.forEach {
            listSchedule.add(scheduleMapper.mapToEntity(it))
        }
        return listSchedule
    }

    private fun getListPaymentMethodsFromUsesCase(list: List<PaymentMethod>): List<PaymentMethodEntity>{
        val listPaymentMethods = mutableListOf<PaymentMethodEntity>()
        list.forEach {
            listPaymentMethods.add(paymentMethodMapper.mapToEntity(it))
        }
        return listPaymentMethods
    }
}