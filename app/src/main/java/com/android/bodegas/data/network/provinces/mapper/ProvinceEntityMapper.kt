package com.android.bodegas.data.network.provinces.mapper

import com.android.bodegas.data.network.Mapper
import com.android.bodegas.data.network.provinces.model.ProvinceResponse
import com.android.bodegas.data.repository.provinces.model.ProvinceEntity

class ProvinceEntityMapper : Mapper<ProvinceResponse, ProvinceEntity> {
    override fun mapFromRemote(type: ProvinceResponse): ProvinceEntity {
        return ProvinceEntity(
            type.id,
            type.name,
            type.department_id
        )
    }
}