package com.android.bodegas.data.repository.save_order

import com.android.bodegas.data.repository.save_order.mapper.SaveOrderMapper
import com.android.bodegas.data.repository.save_order.store.db.SaveOrderDbDataStore
import com.android.bodegas.domain.save_order.SaveOrder
import com.android.bodegas.domain.save_order.SaveOrderRepository
import com.android.bodegas.domain.util.Resource
import com.android.bodegas.domain.util.Status
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.coroutineScope
import kotlin.coroutines.CoroutineContext

class SaveOrderDataRepository constructor(
    private val saverOrderDbDataStore: SaveOrderDbDataStore,
    private val saveOrderMapper: SaveOrderMapper
) : SaveOrderRepository, CoroutineScope {

    private val job = Job()
    override val coroutineContext: CoroutineContext = Dispatchers.IO + job


    override suspend fun getOrder(): Resource<List<SaveOrder>> {
        return coroutineScope {
            val orderListResponse = saverOrderDbDataStore.getOrder()
            var resource: Resource<List<SaveOrder>> =
                Resource(orderListResponse.status, mutableListOf(), orderListResponse.message)

            if (orderListResponse.status == Status.SUCCESS) {
                val orderList = mutableListOf<SaveOrder>()
                orderListResponse.data.forEach {
                    orderList.add(saveOrderMapper.mapFromEntity(it))
                }
                resource = Resource(Status.SUCCESS, orderList, orderListResponse.message)
            } else {
                if (orderListResponse.status == Status.ERROR) {
                    resource = Resource(Status.ERROR, mutableListOf(), orderListResponse.message)
                }
            }
            return@coroutineScope resource
        }
    }

    override suspend fun getOrderById(orderId: String): Resource<SaveOrder> {
        return coroutineScope {
            val orderResponse = saverOrderDbDataStore.getOrderById(orderId)
            if (orderResponse.status == Status.SUCCESS) {
                return@coroutineScope Resource(
                    Status.SUCCESS,
                    saveOrderMapper.mapFromEntity(orderResponse.data),
                    orderResponse.message
                )
            } else {
                return@coroutineScope Resource(
                    Status.ERROR,
                    saveOrderMapper.mapFromEntity(orderResponse.data),
                    orderResponse.message
                )
            }

        }
    }

    override suspend fun saveOrder(saveOrder: SaveOrder) {
        coroutineScope {
            saverOrderDbDataStore.saveOrder(saveOrderMapper.mapToEntity(saveOrder))
        }
    }

    override suspend fun updateOrder(saveOrder: SaveOrder) {
        coroutineScope {
            saverOrderDbDataStore.updateOrder(saveOrderMapper.mapToEntity(saveOrder))
        }
    }

    override suspend fun deleteOrder() {
        coroutineScope {
            saverOrderDbDataStore.deleteOrder()
        }
    }

    override suspend fun deleteOrderById(orderId: String) {
        coroutineScope {
            saverOrderDbDataStore.deleteOrderByID(orderId)
        }
    }
}