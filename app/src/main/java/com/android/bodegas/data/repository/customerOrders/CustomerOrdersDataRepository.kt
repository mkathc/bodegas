package com.android.bodegas.data.repository.customerOrders

import android.util.Log
import com.android.bodegas.data.repository.customerOrders.mapper.CustomerOrdersMapper
import com.android.bodegas.data.repository.customerOrders.mapper.OrderDetailBodyMapper
import com.android.bodegas.data.repository.customerOrders.model.CustomerOrdersEntity
import com.android.bodegas.data.repository.customerOrders.model.OrderDetailBodyEntity
import com.android.bodegas.data.repository.customerOrders.store.remote.CustomerOrdersRemoteDataStorage
import com.android.bodegas.domain.customerOrders.CustomerOrderRepository
import com.android.bodegas.domain.customerOrders.CustomerOrders
import com.android.bodegas.domain.customerOrders.OrderDetailBody
import com.android.bodegas.domain.util.Resource
import com.android.bodegas.domain.util.Status
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.coroutineScope
import kotlin.coroutines.CoroutineContext

class CustomerOrdersDataRepository (
    private val customerOrdersRemoteDataStore: CustomerOrdersRemoteDataStorage,
    private val customerOrdersMapper: CustomerOrdersMapper,
    private val orderDetailBodyMapper: OrderDetailBodyMapper
) : CustomerOrderRepository, CoroutineScope {

    private val job = Job()
    override val coroutineContext: CoroutineContext = Dispatchers.IO + job

    override suspend fun getCustomerOrdersByCustomerId(customerId: Int): Resource<List<CustomerOrders>> {
        return coroutineScope {
            val customerOrdersList = customerOrdersRemoteDataStore.getCustomerOrdersByCustomerId(customerId)
            var resource : Resource<List<CustomerOrders>> = Resource(customerOrdersList.status, mutableListOf(), customerOrdersList.message)
            if(customerOrdersList.status == Status.SUCCESS)  {
                val mutableListcustomerOrders = mapClassList(customerOrdersList.data)
                resource = Resource(Status.SUCCESS, mutableListcustomerOrders, customerOrdersList.message)
            }
            else {
                if(customerOrdersList.status == Status.ERROR) {
                    resource = Resource(customerOrdersList.status, mutableListOf(), customerOrdersList.message)
                }
            }
            return@coroutineScope resource
        }
    }

    private fun mapClassList(customerOrdersEntityList: List<CustomerOrdersEntity>) : List<CustomerOrders> {
        val customerOrdersList = mutableListOf<CustomerOrders>()
        customerOrdersEntityList.forEach{
            customerOrdersList.add(customerOrdersMapper.mapFromEntity(it))
        }
        return customerOrdersList
    }

    override suspend fun updateStateOrder(orderId:Int, state:String, list: List<OrderDetailBody>): Resource<Boolean> {
        return coroutineScope {
            val sendList = mutableListOf<OrderDetailBodyEntity>()
            list.forEach {
                sendList.add(orderDetailBodyMapper.mapToEntity(it))
            }
            val result = customerOrdersRemoteDataStore.updateStateOrder(orderId, state, sendList)
            return@coroutineScope Resource(result.status, result.data, result.message)
        }
    }

    override suspend fun getFileByOrder(orderId: Int): Resource<String> {
        return coroutineScope {
            val result = customerOrdersRemoteDataStore.getFileByOrder(orderId)
            return@coroutineScope Resource(result.status, result.data, result.message)
        }
    }
}