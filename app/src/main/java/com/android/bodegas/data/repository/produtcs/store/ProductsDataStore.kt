package com.android.bodegas.data.repository.produtcs.store

import com.android.bodegas.data.repository.produtcs.model.ProductsEntity
import com.android.bodegas.data.repository.supplier.model.SupplierEntity
import com.android.bodegas.domain.util.Resource

interface ProductsDataStore {
    suspend fun getProducts(
        establishmentId: Int,
        supplierDocument: String,
        subCatedoryId: Int
    ): Resource<List<ProductsEntity>>

    suspend fun searchProducts(
        searchText: String,
        storeId:Int
    ): Resource<List<ProductsEntity>>
}