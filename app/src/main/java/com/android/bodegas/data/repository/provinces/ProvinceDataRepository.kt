package com.android.bodegas.data.repository.provinces

import com.android.bodegas.data.repository.provinces.mapper.ProvinceMapper
import com.android.bodegas.data.repository.provinces.model.ProvinceEntity
import com.android.bodegas.data.repository.provinces.store.remote.ProvinceRemoteDataStorage
import com.android.bodegas.domain.province.Province
import com.android.bodegas.domain.province.ProvinceRepository
import com.android.bodegas.domain.util.Resource
import com.android.bodegas.domain.util.Status
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.coroutineScope
import kotlin.coroutines.CoroutineContext

class ProvinceDataRepository (
    private val provinceRemoteDataStore: ProvinceRemoteDataStorage,
    private val provinceMapper: ProvinceMapper
) : ProvinceRepository, CoroutineScope {

    private val job = Job()
    override val coroutineContext: CoroutineContext = Dispatchers.IO + job

    override suspend fun getProvinces(id: String, name: String, department_id: String): Resource<List<Province>> {
        return coroutineScope {
            val provinceList = provinceRemoteDataStore.getProvinces(id,  name,department_id)
            var resource : Resource<List<Province>> = Resource(provinceList.status, mutableListOf(), provinceList.message)
            if(provinceList.status == Status.SUCCESS)  {
                val provinceListdeparments = mapClassList(provinceList.data)
                resource = Resource(Status.SUCCESS, provinceListdeparments, provinceList.message)
            }
            else {
                if(provinceList.status == Status.ERROR) {
                    resource = Resource(provinceList.status, mutableListOf(), provinceList.message)
                }
            }
            return@coroutineScope resource
        }
    }

    private fun mapClassList(provinceEntityList: List<ProvinceEntity>) : List<Province> {
        val provinceList = mutableListOf<Province>()
        provinceEntityList.forEach{
            provinceList.add(provinceMapper.mapFromEntity(it))
        }
        return provinceList
    }
}