package com.android.bodegas.data.repository.subcategory.model

class SubCategoryEntity (
    val id: Int,
    val description: String,
    val name: String,
    val pathImage:String ? = ""
)