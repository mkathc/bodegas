package com.android.bodegas.data.network.customerOrders

import android.util.Log
import com.android.bodegas.data.PreferencesManager
import com.android.bodegas.data.network.customerOrders.mapper.CustomerOrdersEntityMapper
import com.android.bodegas.data.network.customerOrders.mapper.OrderDetailBodyEntityMapper
import com.android.bodegas.data.network.customerOrders.model.DataItem
import com.android.bodegas.data.network.customerOrders.model.OrderBodyToUpdate
import com.android.bodegas.data.network.customerOrders.model.OrderDetailBodyToSend
import com.android.bodegas.data.repository.customerOrders.model.CustomerOrdersEntity
import com.android.bodegas.data.repository.customerOrders.model.OrderDetailBodyEntity
import com.android.bodegas.data.repository.customerOrders.store.remote.CustomerOrdersRemote
import com.android.bodegas.domain.util.Resource
import com.android.bodegas.domain.util.Status
import kotlinx.coroutines.*
import java.net.UnknownHostException
import kotlin.coroutines.CoroutineContext

class CustomerOrdersRemoteImpl constructor(
    private val customerOrdersServices: CustomerOrdersService,
    private val customerOrdersEntityMapper: CustomerOrdersEntityMapper,
    private val orderDetailBodyEntityMapper: OrderDetailBodyEntityMapper
) : CustomerOrdersRemote, CoroutineScope {
    private val NO_CONTENT:Int= 204
    private val job = Job()
    override val coroutineContext: CoroutineContext = Dispatchers.IO + job

    override suspend fun getCustomerOrdersByCustomerId(
        customerId: Int
    ): Resource<List<CustomerOrdersEntity>> {
        return coroutineScope {
            try {
                val result = customerOrdersServices.getOrdersByCustomer(customerId, "-creationDate")

                if (result.isSuccessful) {
                    if(result.code() == NO_CONTENT) {
                        return@coroutineScope Resource(Status.SUCCESS,  listOf<CustomerOrdersEntity>(), "") }
                    else{
                        result.body()?.let {
                            val customerOrdersEntityList = mapList(result.body()!!.data)
                            customerOrdersEntityList?.let {
                            return@coroutineScope Resource(Status.SUCCESS, customerOrdersEntityList, "")
                            }
                        }
                    }
                }
                return@coroutineScope Resource(Status.ERROR, listOf<CustomerOrdersEntity>(), "")


            } catch (e: UnknownHostException) {
                return@coroutineScope Resource(
                    Status.ERROR,
                    listOf<CustomerOrdersEntity>(),
                    "Not Connection"
                )
            } catch (e: Throwable) {
                e.printStackTrace()
                return@coroutineScope Resource(Status.ERROR, listOf<CustomerOrdersEntity>(), "")
            }
        }
    }

    override suspend fun updateStateOrder(orderId: Int, state:String, list: List<OrderDetailBodyEntity>): Resource<Boolean> {
        return coroutineScope {
            val sendListOrder = mutableListOf<OrderDetailBodyToSend>()
            list.forEach {
                sendListOrder.add(orderDetailBodyEntityMapper.mapFromRemote(it))
            }
            val orderBodyToUpdate = OrderBodyToUpdate(
                state, PreferencesManager.getInstance().getUser().dni, sendListOrder
            )
            val result = customerOrdersServices.updateStateOrder(
                PreferencesManager.getInstance().getUser().customerId, orderId, orderBodyToUpdate)
            if(result.isSuccessful){
                val body = result.body()!!.success
                if(body){
                    return@coroutineScope Resource(Status.SUCCESS,  true, "")
                }else{
                    return@coroutineScope Resource(Status.SUCCESS, false, result.body()!!.message)
                }
            }else{
                return@coroutineScope Resource(Status.ERROR,  false, "")
            }
        }
    }

    override suspend fun getFileByOrder(orderId: Int): Resource<String> {
        return coroutineScope {
            val result = customerOrdersServices.getFileByOrder(orderId, "Customer")
            if(result.isSuccessful){
                if(result.body()!!.success){
                    val body = result.body()!!.fileResponse!!.file.fileDownloadUri
                    Log.e("error", "----------------------------------------------------------------")
                    Log.e("error", "url de File:::> "+ result.body()!!.fileResponse!!.file.fileDownloadUri.toString())
                    Log.e("error", "----------------------------------------------------------------")
                    return@coroutineScope Resource(
                        Status.SUCCESS,
                        body,
                        body
                    )
                }
                else{
                    return@coroutineScope Resource(
                        Status.ERROR,
                        "",
                        "Ha ocurrido un error al obtener su reporte, intente nuevamente por favor"
                    )
                }
            }else{
                return@coroutineScope Resource(
                    Status.ERROR,
                    "",
                    "Ha ocurrido un error al obtener su reporte, intente nuevamente por favor"
                )
            }
        }
    }

    private fun mapList(customerOrdersListBody: DataItem?): List<CustomerOrdersEntity>? {
        val customerOrdersList = ArrayList<CustomerOrdersEntity>()
        customerOrdersListBody?.orders?.forEach {
            customerOrdersList.add(customerOrdersEntityMapper.mapFromRemote(it))
        }
        return customerOrdersList
    }

    /*private suspend fun getMockedProducts(): Resource<List<ProductsEntity>> {
        return coroutineScope {
            delay(3000)
            try {
                val gson = Gson()
                val inputStream: InputStream = context.assets.open("mocked_products_list")
                val inputStreamReader = InputStreamReader(inputStream, StandardCharsets.UTF_8)
                val productsList: ProductsListBody =
                    gson.fromJson(inputStreamReader, ProductsListBody::class.java)
                val productsEntityList = mutableListOf<ProductsEntity>()
                productsList.productsListBody?.storeProducts?.forEach { it ->
                    productsEntityList.add(customerOrdersEntityMapper.mapFromRemote(it))
                }
                return@coroutineScope Resource<List<ProductsEntity>>(
                    Status.SUCCESS,
                    productsEntityList,
                    "Mocked list"
                )
            } catch (e: Throwable) {
                e.printStackTrace()
                return@coroutineScope Resource(Status.ERROR, listOf<ProductsEntity>(), "")
            }

        }
    }*/
}