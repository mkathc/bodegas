package com.android.bodegas.data.network.district

import com.android.bodegas.data.network.district.model.DistrictListBody
import retrofit2.http.GET
import retrofit2.Response

interface DistrictService {
    @GET("/districts")
    suspend fun getDistricts() : Response<DistrictListBody>
}
