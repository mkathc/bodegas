package com.android.bodegas.data.repository.customerOrders.store.remote

import com.android.bodegas.data.repository.customerOrders.model.CustomerOrdersEntity
import com.android.bodegas.data.repository.customerOrders.model.OrderDetailBodyEntity
import com.android.bodegas.domain.customerOrders.OrderDetailBody
import com.android.bodegas.domain.util.Resource

interface CustomerOrdersRemote {
    suspend fun getCustomerOrdersByCustomerId (
        customerId: Int
    ): Resource<List<CustomerOrdersEntity>>

    suspend fun updateStateOrder(
        orderId: Int,
        state:String,
        list: List<OrderDetailBodyEntity>
    ): Resource<Boolean>

    suspend fun getFileByOrder(
        orderId: Int
    ): Resource<String>
}