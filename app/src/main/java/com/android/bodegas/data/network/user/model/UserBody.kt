package com.android.bodegas.data.network.user.model

import com.google.gson.annotations.SerializedName

class UserBody(
    val email: String,
    val password: String,
    @SerializedName("userType")
    val userType: String = "C")
