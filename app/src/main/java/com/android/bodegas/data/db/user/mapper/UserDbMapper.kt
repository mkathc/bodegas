package com.android.bodegas.data.db.user.mapper

import com.android.bodegas.data.db.Mapper
import com.android.bodegas.data.db.user.model.UserDb
import com.android.bodegas.data.repository.user.model.LoginEntity
import com.android.bodegas.data.repository.user.model.UserEntity

class UserDbMapper : Mapper<UserDb, UserEntity> {

    override fun mapFromDb(type: UserDb): UserEntity {
        return UserEntity( type.customerId,
            type.creationDate,
            type.creationUser,
            type.departament,
            type.district,
            type.dni,
            type.email,
            type.address,
            type.lastNameMaternal,
            type.lastNamePaternal,
            type.latitude,
            type.longitude,
            type.name,
            type.phoneNumber,
            type.province,
            type.ruc,
            type.status,
            type.urbanization,
            type.pathImage)
    }

    override fun mapToDb(type: UserEntity): UserDb {
        return UserDb(
            type.customerId,
            type.creationDate,
            type.creationUser,
            type.departament,
            type.district,
            type.dni,
            type.email,
            type.address,
            type.lastNameMaternal,
            type.lastNamePaternal,
            type.latitude,
            type.longitude,
            type.name,
            type.phoneNumber,
            type.province,
            type.ruc,
            type.status,
            type.urbanization,
            type.pathImage)
    }
}