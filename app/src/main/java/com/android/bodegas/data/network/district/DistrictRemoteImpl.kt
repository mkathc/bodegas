package com.android.bodegas.data.network.district

import android.content.Context
import com.android.bodegas.data.network.district.mapper.DistrictEntityMapper
import com.android.bodegas.data.network.district.model.DistrictItem
import com.android.bodegas.data.network.district.model.DistrictListBody
import com.android.bodegas.data.repository.districts.model.ProvinceEntity
import com.android.bodegas.data.repository.districts.store.remote.DistrictRemote
import com.android.bodegas.domain.util.Resource
import com.android.bodegas.domain.util.Status
import com.google.gson.Gson
import kotlinx.coroutines.*
import java.io.InputStream
import java.io.InputStreamReader
import java.net.UnknownHostException
import java.nio.charset.StandardCharsets
import kotlin.coroutines.CoroutineContext


class DistrictRemoteImpl constructor(private val districtServices: DistrictService,
                                     private val districtEntityMapper: DistrictEntityMapper,
                                     private val context: Context
) : DistrictRemote, CoroutineScope {

    private val job = Job()
    override val coroutineContext: CoroutineContext = Dispatchers.IO + job

    override suspend fun getDistricts(
        id: String,
        name: String,
        province_id: String,
        department_id: String
    ): Resource<List<ProvinceEntity>> {
        return coroutineScope {
            try{
                /*val result = districtServices.getDistricts()

                if (result.isSuccessful) {
                    result.body()?.let {
                        val districtEntityList = mapList(result.body()!!.districtListBody)
                        districtEntityList?.let {
                            return@coroutineScope Resource(Status.SUCCESS, districtEntityList, "")
                        }
                    }
                }
                return@coroutineScope Resource(Status.ERROR, listOf<ProvinceEntity>(), "")*/
                return@coroutineScope getMockedDistricts()
            }
            catch(e: UnknownHostException){
                return@coroutineScope Resource(
                    Status.ERROR,
                    listOf<ProvinceEntity>(),
                    "Not Connection"
                )
            }
            catch(e: Throwable){
                e.printStackTrace()
                return@coroutineScope Resource(Status.ERROR, listOf<ProvinceEntity>(),"")
            }
        }
    }

    private fun mapList(districtListBody: DistrictItem?): List<ProvinceEntity>? {
        val districtList = ArrayList<ProvinceEntity>()
        districtListBody?.districts?.forEach{
            districtList.add(districtEntityMapper.mapFromRemote(it))
        }
        return districtList
    }

    private suspend fun getMockedDistricts(): Resource<List<ProvinceEntity>> {
        return coroutineScope {
            delay(1000)
            try{
                val gson = Gson()
                val inputStream: InputStream = context.assets.open("peru_distritos")
                val inputStreamReader = InputStreamReader(inputStream, StandardCharsets.UTF_8)
                val districtList: DistrictListBody = gson.fromJson(inputStreamReader, DistrictListBody::class.java)
                val districtEntityList = mutableListOf<ProvinceEntity>()
                districtList.districtListBody?.districts?.forEach{ it -> districtEntityList.add(districtEntityMapper.mapFromRemote(it))}

                return@coroutineScope Resource<List<ProvinceEntity>>(
                    Status.SUCCESS,
                    districtEntityList,
                    "Mocked classes list"
                )
            }
            catch (e: Throwable){
                e.printStackTrace()
                return@coroutineScope Resource(Status.ERROR, listOf<ProvinceEntity>(), "")
            }
        }
    }
}

