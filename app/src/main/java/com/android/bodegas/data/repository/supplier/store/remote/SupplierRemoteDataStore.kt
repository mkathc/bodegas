package com.android.bodegas.data.repository.supplier.store.remote

import com.android.bodegas.data.repository.supplier.model.SupplierEntity
import com.android.bodegas.data.repository.supplier.store.SupplierDataStore
import com.android.bodegas.domain.util.Resource


class SupplierRemoteDataStore constructor(private val supplierRemote: SupplierRemote) :
    SupplierDataStore {

    override suspend fun getSupplierByType(
        type: Int,
        latitude: String,
        longitud: String,
        radius: Int
    ): Resource<List<SupplierEntity>> {
        return supplierRemote.getSupplierByType(type,latitude,longitud,radius)
    }

}