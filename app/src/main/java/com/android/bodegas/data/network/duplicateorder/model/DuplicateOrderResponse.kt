package com.android.bodegas.data.network.duplicateorder.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class DuplicateOrderResponse(
    @SerializedName("establishmentId")
    @Expose
    var establishmentId: Int,
    @SerializedName("flagAllAvailableProducts")
    @Expose
    var flagAllAvailableProducts: Boolean,
    @SerializedName("numberOfAvailableProducts")
    @Expose
    var numberOfAvailableProducts: Double,
    @SerializedName("orderDetailRequest")
    @Expose
    var orderDetailRequest: List<DuplicateOrderItemBody>,
    @SerializedName("totalAmountOfAvailableProducts")
    @Expose
    var totalAmountOfAvailableProducts: Double
)