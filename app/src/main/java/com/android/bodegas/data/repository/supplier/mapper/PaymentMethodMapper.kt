package com.android.bodegas.data.repository.supplier.mapper

import com.android.bodegas.data.repository.Mapper
import com.android.bodegas.data.repository.supplier.model.PaymentMethodEntity
import com.android.bodegas.data.repository.supplier.model.ScheduleEntity
import com.android.bodegas.data.repository.supplier.model.SupplierEntity
import com.android.bodegas.domain.supplier.PaymentMethod
import com.android.bodegas.domain.supplier.Schedule
import com.android.bodegas.domain.supplier.Supplier


class PaymentMethodMapper : Mapper<PaymentMethodEntity, PaymentMethod> {

    override fun mapFromEntity(type: PaymentMethodEntity): PaymentMethod {
        return PaymentMethod(
            type.paymentMethodId,
            type.requestedAmount,
            type.description,
            type.customerMessage
        )
    }

    override fun mapToEntity(type: PaymentMethod): PaymentMethodEntity {
        return PaymentMethodEntity(
            type.paymentMethodId,
            type.requestedAmount,
            type.description,
            type.customerMessage
        )
    }

}