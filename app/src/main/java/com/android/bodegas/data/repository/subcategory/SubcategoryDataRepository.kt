package com.android.bodegas.data.repository.subcategory

import android.util.Log

import com.android.bodegas.data.repository.subcategory.mapper.SubCategoryMapper
import com.android.bodegas.data.repository.subcategory.model.SubCategoryEntity
import com.android.bodegas.data.repository.subcategory.store.remote.SubcategoryRemoteDataStorage
import com.android.bodegas.domain.subcategory.Subcategory
import com.android.bodegas.domain.subcategory.SubcategoryRepository
import com.android.bodegas.domain.util.Resource
import com.android.bodegas.domain.util.Status
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.coroutineScope
import kotlin.coroutines.CoroutineContext

class SubcategoryDataRepository(
    private val categoryRemoteDataStore: SubcategoryRemoteDataStorage,
    private val categoryMapper: SubCategoryMapper
) : SubcategoryRepository, CoroutineScope {

    private val job = Job()
    override val coroutineContext: CoroutineContext = Dispatchers.IO + job

    override suspend fun getSubcategories(
        establishmentId: Int,
        category_id: Int,
        page: String,
        size: String,
        sortBy: String
    ): Resource<List<Subcategory>> {
        return coroutineScope {

            val classList =
                categoryRemoteDataStore.getSubcategories( establishmentId, category_id,
                    page,
                    size,
                    sortBy)

            var resource: Resource<List<Subcategory>> =
                Resource(classList.status, mutableListOf(), classList.message)

            if (classList.status == Status.SUCCESS) {
                val mutableListclasses = mapCategoryList(classList.data)
                resource = Resource(Status.SUCCESS, mutableListclasses, classList.message)
            } else {
                if (classList.status == Status.ERROR) {
                    resource = Resource(classList.status, mutableListOf(), classList.message)
                }
            }

            return@coroutineScope resource
        }
    }

    private fun mapCategoryList(categoryEntityList: List<SubCategoryEntity>): List<Subcategory> {
        val categoryList = mutableListOf<Subcategory>()
        categoryEntityList.forEach {
            categoryList.add( categoryMapper.mapFromEntity(it))
        }
        return categoryList
    }
}