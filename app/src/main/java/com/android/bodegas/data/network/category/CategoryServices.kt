package com.android.bodegas.data.network.category

import com.android.bodegas.data.network.category.model.CategoryListBody
import com.android.bodegas.data.network.category.model.CategoryResponse
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface CategoryServices {
    @GET("/establishments/{establishment-id}/classes/{class-id}/categories")
    suspend fun getCategoriesBySupplierId(
        @Path("establishment-id") establishmentId: Int,
        @Path("class-id") class_id: Int
    ) : Response<CategoryListBody>
}
