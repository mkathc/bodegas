package com.android.bodegas.data.network.user.model

import com.android.bodegas.data.network.ResponseBodySuccess
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class FileResponse : ResponseBodySuccess() {
    @SerializedName("data")
    @Expose
    var fileResponse: FileItem? = null
}

class FileItem(
    @SerializedName("file")
    @Expose
    var file: FileDetailItem
)

class FileDetailItem(
    @SerializedName("fileDownloadUri")
    @Expose
    var fileDownloadUri: String
)


