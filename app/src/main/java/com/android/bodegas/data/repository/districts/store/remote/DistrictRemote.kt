package com.android.bodegas.data.repository.districts.store.remote

import com.android.bodegas.data.repository.districts.model.ProvinceEntity
import com.android.bodegas.domain.util.Resource

interface DistrictRemote {
    suspend fun getDistricts (
        id: String,
        name: String,
        province_id: String,
        department_id: String
    ): Resource<List<ProvinceEntity>>
}
