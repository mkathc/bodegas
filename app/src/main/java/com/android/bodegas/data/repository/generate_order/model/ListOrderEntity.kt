package com.android.bodegas.data.repository.generate_order.model

data class ListOrderEntity(
    val observation: String,
    val price: Double,
    val quantity: Double,
    val storeProductId: Int,
    val subtotal: Double,
    val unitMeasure: String
)