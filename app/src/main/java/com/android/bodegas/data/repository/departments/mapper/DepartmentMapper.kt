package com.android.bodegas.data.repository.departments.mapper

import com.android.bodegas.data.repository.Mapper
import com.android.bodegas.data.repository.departments.model.DepartmentEntity
import com.android.bodegas.domain.department.Department

class DepartmentMapper: Mapper<DepartmentEntity,Department> {
    override fun mapFromEntity(type: DepartmentEntity): Department {
        return Department(type.id,type.name)
    }

    override fun mapToEntity(type: Department): DepartmentEntity {
        return DepartmentEntity(type.id,type.name)
    }
}
