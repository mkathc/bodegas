package com.android.bodegas.data.repository.supplier.model

data class PaymentMethodEntity(
    val paymentMethodId: Int,
    val requestedAmount: Boolean,
    val description: String,
    val customerMessage: String
)