package com.android.bodegas.data.network.subcategory.model

import com.android.bodegas.data.network.ResponseBodySuccess
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


class SubCategoryListBody : ResponseBodySuccess() {
    @SerializedName("data")
    @Expose
    var subcategoryListBody: SubCategoryItem? = null
}

class SubCategoryItem (
    @SerializedName("subCategoriesTotal")
    val subCategoriesTotal: Int,
    @SerializedName("subCategories")
    val subCategoriesList: List<SubCategoryResponse>
)