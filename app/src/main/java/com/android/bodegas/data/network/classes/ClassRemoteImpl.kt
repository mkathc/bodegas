package com.android.bodegas.data.network.classes

import android.content.Context
import com.android.bodegas.data.network.classes.mapper.ClassEntityMapper
import com.android.bodegas.data.network.classes.model.ClassItem
import com.android.bodegas.data.network.classes.model.ClassListBody
import com.android.bodegas.data.repository.clases.model.ClassEntity
import com.android.bodegas.data.repository.clases.store.remote.ClassRemote
import com.android.bodegas.domain.util.Resource
import com.android.bodegas.domain.util.Status
import com.google.gson.Gson
import kotlinx.coroutines.*
import java.io.InputStream
import java.io.InputStreamReader
import java.net.UnknownHostException
import java.nio.charset.StandardCharsets
import kotlin.coroutines.CoroutineContext

class ClassRemoteImpl constructor(
    private val classesServices: ClassesServices,
    private val classEntityMapper: ClassEntityMapper,
    private val context: Context
) : ClassRemote, CoroutineScope{

    private val job = Job()
    override val coroutineContext: CoroutineContext = Dispatchers.IO + job

    override suspend fun getClassesBySupplierId(
        establishmentId: Int,
        page: String,
        size: String,
        sortBy: String
    ): Resource<List<ClassEntity>> {
        return coroutineScope {
            try{
                val result = classesServices.getClassesBySupplierId(
                    establishmentId)
                if (result.isSuccessful) {
                    if (result.code() == 200){
                        result.body()?.let {
                            val supplierEntityList = mapList(result.body()!!.classesListBody)
                            supplierEntityList?.let {
                                return@coroutineScope Resource(Status.SUCCESS, supplierEntityList, "")
                            }
                        }
                    }else if (result.code() == 204){
                        return@coroutineScope Resource(Status.SUCCESS, listOf<ClassEntity>(), "")
                    }
                }
                return@coroutineScope Resource(Status.ERROR, listOf<ClassEntity>(), "")
            }

            catch(e: UnknownHostException){
                return@coroutineScope Resource(
                    Status.ERROR,
                    listOf<ClassEntity>(),
                    "Not Connection"
                )
            }

            catch(e: Throwable){
                e.printStackTrace()
                return@coroutineScope Resource(Status.ERROR, listOf<ClassEntity>(),"")
            }
        }
    }

    private fun mapList(classListBody: ClassItem?): List<ClassEntity>? {
        val classList = ArrayList<ClassEntity>()
        classListBody?.classesList?.forEach {
            classList.add(classEntityMapper.mapFromRemote(it))
        }
        return classList
    }


    private suspend fun getMockedClasses(): Resource<List<ClassEntity>>{
       return coroutineScope {
           delay(1000)
           try{
               val gson = Gson()
               val inputStream: InputStream = context.assets.open("mocked_class_list")
               val inputStreamReader = InputStreamReader(inputStream, StandardCharsets.UTF_8)
               val classList: ClassListBody = gson.fromJson(inputStreamReader, ClassListBody::class.java)
               val classEntityList = mutableListOf<ClassEntity>()
               classList.classesListBody?.classesList?.forEach{ it -> classEntityList.add(classEntityMapper.mapFromRemote(it))}

               return@coroutineScope Resource<List<ClassEntity>>(
                   Status.SUCCESS,
                   classEntityList,
                   "Mocked classes list"
               )
           }
           catch (e: Throwable){
               e.printStackTrace()
               return@coroutineScope Resource(Status.ERROR, listOf<ClassEntity>(), "")
           }
       }
   }
}