package com.android.bodegas.data.network.supplier

import com.android.bodegas.data.network.supplier.model.SupplierListBody
import retrofit2.Response
import retrofit2.http.*

interface SupplierServices {

    @GET("/establishments")
    suspend fun getSupplierByType(
        @Query("establishment-type") type: Int,
        @Query("latitude") latitude: String,
        @Query("longitude") longitude: String,
        @Query("radius") radius: Int
    ): Response<SupplierListBody>
}