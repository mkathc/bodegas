package com.android.bodegas.data.network.classes.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class ClassResponse(
    @SerializedName("classId")
    @Expose
    val id: Int,
    @SerializedName("description")
    @Expose
    val description: String,
    @SerializedName("name")
    @Expose
    val name: String,
    @SerializedName("pathImage")
    @Expose
    val pathImage: String ? = ""
)