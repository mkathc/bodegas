package com.android.bodegas.data.network.generate_order.model

import com.google.gson.annotations.SerializedName

class ListOrderBody(
  @SerializedName("observation")
  val observation: String,
  @SerializedName("price")
  val price: Double,
  @SerializedName("quantity")
  val quantity: Double,
  @SerializedName("storeProductId")
  val storeProductId: Int,
  @SerializedName("subtotal")
  val subtotal: Double,
  @SerializedName("unitMeasure")
  val unitMeasure: String
)
