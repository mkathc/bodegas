package com.android.bodegas.data.network.user.model

import com.android.bodegas.data.network.ResponseBodySuccess
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class UserLoginTokenBody : ResponseBodySuccess() {
    @SerializedName("data")
    @Expose
    var data: EstablishmentLoginItem? = null
}

class EstablishmentLoginItem(
    @SerializedName("login")
    @Expose
    var login: UserLoginTokenResponse
)
