package com.android.bodegas.data.repository.user.store.db

import com.android.bodegas.data.repository.user.store.UserDataStore
import com.android.bodegas.data.repository.user.model.LoginEntity
import com.android.bodegas.data.repository.user.model.RegisterUserBodyEntity
import com.android.bodegas.data.repository.user.model.UserEntity
import com.android.bodegas.data.repository.user.model.UserLoginTokenEntity
import com.android.bodegas.domain.user.create.model.User
import com.android.bodegas.domain.util.Resource
import com.android.bodegas.presentation.login.ForgetPassDTO
import com.android.bodegas.presentation.profile.ChangePasswordDtoBody
import com.android.bodegas.presentation.profile.UpdateCustomerBody

class UserDbDataStore constructor(private val userDb: UserDb) :
    UserDataStore {

    override suspend fun loginUserToken(email: String, password: String): Resource<UserLoginTokenEntity> {
        TODO("Not yet implemented")
    }

    override suspend fun loginUser(establishmentId: Int): Resource<UserEntity> {
        return userDb.getUser()
    }

    override suspend fun saveUser(userEntity: UserEntity) {
        userDb.saveUser(userEntity)
    }

    override suspend fun clearUser() {
        userDb.clearAllTables()
    }

    override suspend fun registerUser(registerUserBodyEntity: RegisterUserBodyEntity): Resource<UserLoginTokenEntity> {
        TODO("Not yet implemented")
    }

    override suspend fun updatePhotoUser(customerId: Int, filePath: String): Resource<Boolean> {
        TODO("Not yet implemented")
    }

    override suspend fun updatePassword(
        body: ChangePasswordDtoBody
    ): Resource<Boolean> {
        throw UnsupportedOperationException()
    }

    override suspend fun recoveryPassword( body: ForgetPassDTO): Resource<Boolean> {
        throw UnsupportedOperationException()
    }

    override suspend fun updateUser(customerId: Int, body: UpdateCustomerBody): Resource<UserEntity> {
        throw UnsupportedOperationException()
    }

}