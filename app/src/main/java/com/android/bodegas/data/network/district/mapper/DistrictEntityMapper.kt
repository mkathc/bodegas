package com.android.bodegas.data.network.district.mapper

import com.android.bodegas.data.network.Mapper
import com.android.bodegas.data.network.district.model.DistrictResponse
import com.android.bodegas.data.repository.districts.model.ProvinceEntity

class DistrictEntityMapper : Mapper<DistrictResponse, ProvinceEntity> {
    override fun mapFromRemote(type: DistrictResponse): ProvinceEntity {
        return ProvinceEntity(
            type.id,
            type.name,
            type.province_id,
            type.department_id
        )
    }
}