package com.android.bodegas.data.repository.user.mapper

import com.android.bodegas.data.repository.Mapper
import com.android.bodegas.data.repository.user.model.LoginEntity
import com.android.bodegas.data.repository.user.model.RegisterUserBodyEntity
import com.android.bodegas.data.repository.user.model.UserEntity
import com.android.bodegas.domain.user.create.model.RegisterUserBody
import com.android.bodegas.domain.user.create.model.User
import com.android.bodegas.domain.user.login.model.UserLogin

    class RegisterUserMapper : Mapper<RegisterUserBodyEntity, RegisterUserBody> {
    override fun mapFromEntity(type: RegisterUserBodyEntity): RegisterUserBody {
        return RegisterUserBody(
            type.address,
            type.businessName,
            type.cardNumber,
            type.cardOperator,
            type.country,
            type.creationUser,
            type.department,
            type.district,
            type.dni,
            type.email,
            type.lastNameMaternal,
            type.lastNamePaternal,
            type.latitude,
            type.longitude,
            type.name,
            type.password,
            type.phoneNumber,
            type.province,
            type.ruc,
            type.urbanization
        )
    }

    override fun mapToEntity(type: RegisterUserBody): RegisterUserBodyEntity {
        return RegisterUserBodyEntity(
            type.address,
            type.businessName,
            type.cardNumber,
            type.cardOperator,
            type.country,
            type.creationUser,
            type.department,
            type.district,
            type.dni,
            type.email,
            type.lastNameMaternal,
            type.lastNamePaternal,
            type.latitude,
            type.longitude,
            type.name,
            type.password,
            type.phoneNumber,
            type.province,
            type.ruc,
            type.urbanization)
    }

}