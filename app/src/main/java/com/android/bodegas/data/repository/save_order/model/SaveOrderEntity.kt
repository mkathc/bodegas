package com.android.bodegas.data.repository.save_order.model

data class SaveOrderEntity(
    var id: String,
    val storeProductId: Int,
    val price: Double,
    val quantity: Double,
    val subtotal: Double,
    val unitMeasure: String,
    val observation: String,
    val name: String,
    val imageProduct: String
)