package com.android.bodegas.data.network.customerOrders.model

import com.android.bodegas.data.network.supplier.model.PaymentMethodResponse
import com.android.bodegas.data.network.supplier.model.ScheduleResponse
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


class CustomerListOrdersBodyResponse (
    @SerializedName("creationDate")
    val creationDate: String,
    @SerializedName("creationUser")
    val creationUser: Int,
    @SerializedName("updateDate")
    val updateDate: String,
    @SerializedName("updateUser")
    val updateUser: String,
    @SerializedName("orderId")
    val orderId: Int,
    @SerializedName("establishment")
    val establishment: EstablishmentItem,
    @SerializedName("customer")
    val customer: CustomerItem,
    @SerializedName("orderDetail")
    val orderDetail:List<OrderDetailItem>,
    @SerializedName("deliveryType")
    val deliveryType: String,
    @SerializedName("customerAmount")
    val customerAmount: String?= "",
    @SerializedName("deliveryCharge") //monto a pagar
    val deliveryCharge: String? ="",
    @SerializedName("paymentMethod")
    val paymentMethod: PaymentMethodItem,
    @SerializedName("shippingDateFrom")
    val shippingDateFrom: String? ="",
    @SerializedName("shippingDateUntil")
    val shippingDateUntil: String? ="",
    @SerializedName("status")
    val status: String,
    @SerializedName("total")
    val total: String
)

class EstablishmentItem (
    @SerializedName("establishmentId")
    val establishmentId: Int,
    @SerializedName("address")
    val address: String,
    @SerializedName("dni")
    val dni: String,
    @SerializedName("email")
    val email: String,
    @SerializedName("name")
    val name: String,
    @SerializedName("lastNamePaternal")
    val lastNamePaternal: String,
    @SerializedName("lastNameMaternal")
    val lastNameMaternal: String,
    @SerializedName("phoneNumber")
    val phoneNumber: String,
    @SerializedName("businessName")
    val businessName: String,
    @SerializedName("delivery")
    val delivery: String,
    @SerializedName("deliveryCharge")
    val deliveryCharge: Boolean,
    @SerializedName("shippingSchedule")
    @Expose
    val shippingSchedule: List<ScheduleResponse>,
    @SerializedName("operationSchedule")
    @Expose
    val operationSchedule: List<ScheduleResponse>,
    @SerializedName("paymentMethods")
    @Expose
    val paymentMethods: List<PaymentMethodResponse>
)


class CustomerItem (
    @SerializedName("customerId")
    val customerId: Int,
    @SerializedName("dni")
    val dni: String,
    @SerializedName("email")
    val email: String,
    @SerializedName("name")
    val name: String,
    @SerializedName("lastNamePaternal")
    val lastNamePaternal: String,
    @SerializedName("lastNameMaternal")
    val lastNameMaternal: String,
    @SerializedName("phoneNumber")
    val phoneNumber: String
)

class OrderDetailItem(
    @SerializedName("orderDetailId")
    val orderDetailId: Int,
    @SerializedName("storeProduct")
    val storeProduct: StoreProductItem,
    @SerializedName("unitMeasure")
    val unitMeasure: String,
    @SerializedName("quantity")
    val quantity: Double,
    @SerializedName("price")
    val price: String,
    @SerializedName("status")
    val status: String,
    @SerializedName("observation")
    val observation: String,
    @SerializedName("subTotal")
    val subTotal: String
)

class StoreProductItem (
    @SerializedName("storeProductId")
    val storeProductId: Int,
    @SerializedName("productTemplate")
    val productTemplate: ProductTemplateItem,
    @SerializedName("price")
    val price: Double,
    @SerializedName("status")
    val status: String,
    @SerializedName("stock")
    val stock: String
)

class ProductTemplateItem (
    @SerializedName("productTemplateId")
    val productTemplateId: Int,
    @SerializedName("code")
    val code: String,
    @SerializedName("name")
    val name: String,
    @SerializedName("description")
    val description: String,
    @SerializedName("unitMeasure")
    val unitMeasure: String,
    @SerializedName("status")
    val status: String,
    @SerializedName("pathImage")
    val pathImage: String
)

class PaymentMethodItem (
    @SerializedName("paymentMethodId")
    val paymentMethodId: Int,
    @SerializedName("requestedAmount")
    val requestedAmount: Boolean,
    @SerializedName("description")
    val description: String? ="",
    @SerializedName("customerMessage")
    val customerMessage: String? ="",
    @SerializedName("establishmentMessage")
    val establishmentMessage: String? =""
)