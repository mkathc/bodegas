package com.android.bodegas.data.network.products

import android.content.Context
import android.util.Log
import com.android.bodegas.data.PreferencesManager
import com.android.bodegas.data.network.customerOrders.model.EstablishmentItem
import com.android.bodegas.data.network.products.mapper.ProductsEntityMapper
import com.android.bodegas.data.network.products.model.ProductsItem
import com.android.bodegas.data.network.products.model.ProductsListBody
import com.android.bodegas.data.repository.produtcs.model.ProductsEntity
import com.android.bodegas.data.repository.produtcs.store.remote.ProductsRemote
import com.android.bodegas.domain.util.Resource
import com.android.bodegas.domain.util.Status
import com.google.gson.Gson
import kotlinx.coroutines.*
import java.io.InputStream
import java.io.InputStreamReader
import java.net.UnknownHostException
import java.nio.charset.StandardCharsets
import kotlin.coroutines.CoroutineContext


class ProductsRemoteImpl constructor(
    private val productsServices: ProductsServices,
    private val productsEntityMapper: ProductsEntityMapper,
    private val context: Context
) : ProductsRemote, CoroutineScope {

    private val job = Job()
    override val coroutineContext: CoroutineContext = Dispatchers.IO + job

    override suspend fun getProducts(
        establishmentId: Int,
        supplierDocument: String,
        subCategoryId: Int
    ): Resource<List<ProductsEntity>> {
        return coroutineScope {
            try {
                val result = productsServices.getProducts(
                    establishmentId,
                    subCategoryId
                )

                if (result.isSuccessful) {
                    if(result.code() == 200){
                        result.body()?.let {
                            val productsEntityList = mapList(result.body()!!.productsListBody)
                            productsEntityList?.let {
                                return@coroutineScope Resource(Status.SUCCESS, productsEntityList, "")
                            }
                        }
                    }else if (result.code() == 204){
                        return@coroutineScope Resource(Status.SUCCESS,  listOf<ProductsEntity>(), "")
                    }

                }
                return@coroutineScope Resource(Status.ERROR, listOf<ProductsEntity>(), "")

            } catch (e: UnknownHostException) {
                return@coroutineScope Resource(
                    Status.ERROR,
                    listOf<ProductsEntity>(),
                    "Not Connection"
                )
            } catch (e: Throwable) {
                e.printStackTrace()
                return@coroutineScope Resource(Status.ERROR, listOf<ProductsEntity>(), "")
            }
        }
    }

    private fun mapList(productsListBody: ProductsItem?): List<ProductsEntity>? {
        val productsList = ArrayList<ProductsEntity>()
        productsListBody?.storeProducts?.forEach {
            productsList.add(productsEntityMapper.mapFromRemote(it))
        }
        return productsList
    }

    override suspend fun searchProducts(searchText: String, storeId:Int): Resource<List<ProductsEntity>> {
        return coroutineScope {
            try {
                val result = productsServices.searchProducts(
                    storeId,
                    searchText,
                    "productTemplate.description",
                    0,
                    1000
                )

                if (result.isSuccessful) {
                    if(result.code() == 200){
                            result.body()?.let {
                            val productsEntityList = mapList(result.body()!!.productsListBody)
                            productsEntityList?.let {
                            return@coroutineScope Resource(Status.SUCCESS, productsEntityList, "")
                            }
                        }
                    }

                    else if(result.code() == 204){
                        return@coroutineScope Resource(Status.SUCCESS,  listOf<ProductsEntity>(), "")
                    }
                }
                return@coroutineScope Resource(Status.ERROR, listOf<ProductsEntity>(), "")

            } catch (e: UnknownHostException) {
                return@coroutineScope Resource(
                    Status.ERROR,
                    listOf<ProductsEntity>(),
                    "Not Connection"
                )
            } catch (e: Throwable) {
                e.printStackTrace()
                return@coroutineScope Resource(Status.ERROR, listOf<ProductsEntity>(), "")
            }
        }    }

    private suspend fun getMockedProducts(): Resource<List<ProductsEntity>> {
        return coroutineScope {
            delay(3000)
            try {
                val gson = Gson()
                val inputStream: InputStream = context.assets.open("mocked_products_list")
                val inputStreamReader = InputStreamReader(inputStream, StandardCharsets.UTF_8)
                val productsList: ProductsListBody =
                    gson.fromJson(inputStreamReader, ProductsListBody::class.java)
                val productsEntityList = mutableListOf<ProductsEntity>()
                productsList.productsListBody?.storeProducts?.forEach { it ->
                    productsEntityList.add(productsEntityMapper.mapFromRemote(it))
                }
                return@coroutineScope Resource<List<ProductsEntity>>(
                    Status.SUCCESS,
                    productsEntityList,
                    "Mocked list"
                )
            } catch (e: Throwable) {
                e.printStackTrace()
                return@coroutineScope Resource(Status.ERROR, listOf<ProductsEntity>(), "")
            }

        }
    }
}