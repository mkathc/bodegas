package com.android.bodegas.data.repository.duplicateOrder

import com.android.bodegas.data.repository.duplicateOrder.mapper.DuplicateOrderEntityMapper
import com.android.bodegas.data.repository.duplicateOrder.mapper.ListDuplicateOrderItemMapper
import com.android.bodegas.data.repository.duplicateOrder.model.DuplicateOrderEntity
import com.android.bodegas.data.repository.duplicateOrder.store.DuplicateOrderDataStore
import com.android.bodegas.data.repository.duplicateOrder.store.remote.DuplicateOrderRemoteDataStore
import com.android.bodegas.domain.duplicate_order.DuplicateOrder
import com.android.bodegas.domain.duplicate_order.DuplicateOrderItem
import com.android.bodegas.domain.duplicate_order.DuplicateOrderRepository
import com.android.bodegas.domain.util.Resource
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.coroutineScope
import kotlin.coroutines.CoroutineContext

class DuplicateOrderDataRepository constructor(
    private val duplicateOrderRemoteDataStore: DuplicateOrderRemoteDataStore,
    private val duplicateOrderMethodsMapper: DuplicateOrderEntityMapper
) : DuplicateOrderRepository, CoroutineScope {

    private val job = Job()
    override val coroutineContext: CoroutineContext = Dispatchers.IO + job

    override suspend fun getDuplicateOrder(establishmentId: Int, orderId: Int): Resource<DuplicateOrder> {
        return coroutineScope {
            val duplicateOrderEntity = duplicateOrderRemoteDataStore.getDuplicateOrder(establishmentId, orderId)

            val establishment = duplicateOrderMethodsMapper.mapFromEntity(duplicateOrderEntity.data)

            return@coroutineScope Resource(
                duplicateOrderEntity.status,
                establishment,
                duplicateOrderEntity.message
            )
        }
    }

}