package com.android.bodegas.data.network.provinces

import com.android.bodegas.data.network.district.model.DistrictListBody
import com.android.bodegas.data.network.provinces.model.ProvinceListBody
import retrofit2.Response
import retrofit2.http.GET

interface ProvinceService {
    @GET("/provinces")
    suspend fun getProvinces() : Response<ProvinceListBody>
}
