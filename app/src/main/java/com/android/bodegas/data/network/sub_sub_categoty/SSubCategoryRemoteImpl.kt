package com.android.bodegas.data.network.sub_sub_categoty

import android.content.Context
import com.android.bodegas.data.network.category.model.CategoryItem
import com.android.bodegas.data.network.sub_sub_categoty.mapper.GroupEntityMapper
import com.android.bodegas.data.network.sub_sub_categoty.model.SSubCategoryItem
import com.android.bodegas.data.network.sub_sub_categoty.model.SSubCategoryListBody
import com.android.bodegas.data.repository.category.model.CategoryEntity
import com.android.bodegas.data.repository.sub_sub_category.model.GroupEntity
import com.android.bodegas.data.repository.sub_sub_category.model.SSubCategoryEntity
import com.android.bodegas.data.repository.sub_sub_category.store.remote.SSubCategoryRemote
import com.android.bodegas.domain.util.Resource
import com.android.bodegas.domain.util.Status
import com.google.gson.Gson
import kotlinx.coroutines.*
import java.io.InputStream
import java.io.InputStreamReader
import java.net.UnknownHostException
import java.nio.charset.StandardCharsets
import kotlin.coroutines.CoroutineContext


class SSubCategoryRemoteImpl constructor(
    private val sSubCategoryServices: SSubCategoryServices,
    private val groupEntityMapper: GroupEntityMapper,
    private val context: Context
) : SSubCategoryRemote, CoroutineScope {

    private val job = Job()
    override val coroutineContext: CoroutineContext = Dispatchers.IO + job

    override suspend fun getSSubCategory(
        establishmentId: Int,
        subCategoryId: Int
    ): Resource<GroupEntity> {
        return coroutineScope {
            try {

                val result = sSubCategoryServices.getSSubCategory(
                    establishmentId,
                    subCategoryId
                )
                if (result.isSuccessful) {
                    if(result.code() == 200){
                        result.body()?.let {
                            val groupEntity = mapGroup(result.body()!!.sSubCategoryListBody)
                            groupEntity?.let {
                                return@coroutineScope Resource(Status.SUCCESS, groupEntity, "")
                            }
                        }
                    }else if (result.code() == 204){
                        return@coroutineScope Resource(Status.SUCCESS, GroupEntity(0, mutableListOf(), mutableListOf()), "")
                    }

                }
                return@coroutineScope Resource(Status.ERROR, GroupEntity(0, mutableListOf(), mutableListOf()), "")

            } catch (e: UnknownHostException) {
                return@coroutineScope Resource(
                    Status.ERROR,
                    GroupEntity(0, mutableListOf(), mutableListOf()),
                    "Not Connection"
                )
            } catch (e: Throwable) {
                e.printStackTrace()
                return@coroutineScope Resource(
                    Status.ERROR, GroupEntity(0, mutableListOf(), mutableListOf()),
                    ""
                )
            }
        }
    }


    private fun mapGroup(ssubCategoryListBody: SSubCategoryItem): GroupEntity {
        val ssubCategoryList = groupEntityMapper.mapFromRemote(ssubCategoryListBody)
        return ssubCategoryList
    }

    private suspend fun getMockedSSList(): Resource<GroupEntity> {
        return coroutineScope {
            delay(3000)
            try {
                val gson = Gson()
                val inputStream: InputStream = context.assets.open("mocked_subsubcategories_empty_list")
                val inputStreamReader = InputStreamReader(inputStream, StandardCharsets.UTF_8)
                val ssubCategoryListBody: SSubCategoryListBody =
                    gson.fromJson(inputStreamReader, SSubCategoryListBody::class.java)
                return@coroutineScope Resource(
                    Status.SUCCESS,
                    groupEntityMapper.mapFromRemote(ssubCategoryListBody.sSubCategoryListBody),
                    "Mocked list"
                )
            } catch (e: Throwable) {
                e.printStackTrace()
                return@coroutineScope Resource(
                    Status.ERROR,
                    GroupEntity(0, mutableListOf(), mutableListOf()),
                    ""
                )
            }

        }
    }

}