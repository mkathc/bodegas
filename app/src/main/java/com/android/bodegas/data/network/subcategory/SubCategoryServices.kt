package com.android.bodegas.data.network.subcategory

import com.android.bodegas.data.network.subcategory.model.SubCategoryListBody
import com.android.bodegas.data.network.subcategory.model.SubCategoryResponse
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface SubCategoryServices {
    @GET("/establishments/{establishment-id}/category/{category-id}/subcategories")
    suspend fun getSubCategories(
        @Path("establishment-id") establishmentId: Int,
        @Path("category-id") category_id: Int
    ) : Response<SubCategoryListBody>
}
