package com.android.bodegas.data.repository.generate_order.store.remote

import com.android.bodegas.data.repository.generate_order.model.ListOrderEntity
import com.android.bodegas.data.repository.generate_order.store.GenerateOrderDataStore
import com.android.bodegas.domain.util.Resource


class GenerateOrderRemoteDataStore constructor(private val generateOrderRemote: GenerateOrderRemote) :
    GenerateOrderDataStore {

    override suspend fun generateOrder(
        deliveryType: String,
        total: String,
        listOrder: List<ListOrderEntity>,
        startTime:String,
        endTime:String,
        paymentMethodId:Int,
        deliveryCharge:Double,
        customerAmount: Double
    ): Resource<String> {
        return generateOrderRemote.generateOrder(deliveryType, total, listOrder, startTime, endTime, paymentMethodId, deliveryCharge, customerAmount)
    }

}