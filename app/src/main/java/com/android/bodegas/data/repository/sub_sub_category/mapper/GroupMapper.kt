package com.android.bodegas.data.repository.sub_sub_category.mapper

import com.android.bodegas.data.repository.Mapper
import com.android.bodegas.data.repository.produtcs.mapper.ProductsMapper
import com.android.bodegas.data.repository.produtcs.model.ProductsEntity
import com.android.bodegas.data.repository.sub_sub_category.model.GroupEntity
import com.android.bodegas.data.repository.sub_sub_category.model.SSubCategoryEntity
import com.android.bodegas.domain.products.Products
import com.android.bodegas.domain.sub_sub_category.Group
import com.android.bodegas.domain.sub_sub_category.SSubCategory


class GroupMapper(
    private val productsMapper: ProductsMapper,
    private val sSubCategoryMapper: SSubCategoryMapper
) : Mapper<GroupEntity, Group> {

    override fun mapFromEntity(type: GroupEntity): Group {
        return Group(
            type.total,
            getProductsList(type.storeProducts),
            getSSList(type.subSubCategories)
        )
    }

    override fun mapToEntity(type: Group): GroupEntity {
        return GroupEntity(
            type.total,
            mutableListOf(),
            mutableListOf()
        )
    }

    private fun getProductsList(list: List<ProductsEntity>): List<Products> {
        val productsList = ArrayList<Products>()
        if (!list.isNullOrEmpty()) {
            list.forEach {
                productsList.add(productsMapper.mapFromEntity(it))
            }
        }
        return productsList
    }

    private fun getSSList(list: List<SSubCategoryEntity>): List<SSubCategory> {
        val ssubCategoryList = ArrayList<SSubCategory>()
        if (!list.isNullOrEmpty()) {
            list.forEach {
                ssubCategoryList.add(sSubCategoryMapper.mapFromEntity(it))
            }
        }

        return ssubCategoryList
    }

}