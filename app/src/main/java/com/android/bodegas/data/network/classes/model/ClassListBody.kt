package com.android.bodegas.data.network.classes.model

import com.android.bodegas.data.network.ResponseBodySuccess
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class ClassListBody : ResponseBodySuccess () {
    @SerializedName("data")
    @Expose
    var classesListBody: ClassItem? = null
}

class ClassItem (
    @SerializedName("classesTotal")
    val classesTotal: Int,
    @SerializedName("classes")
    val classesList: List<ClassResponse>
)