package com.android.bodegas.data.db.user

import com.android.bodegas.data.db.AppDatabase
import com.android.bodegas.data.db.user.mapper.UserDbMapper
import com.android.bodegas.data.repository.user.model.LoginEntity
import com.android.bodegas.data.repository.user.model.UserEntity
import com.android.bodegas.data.repository.user.store.db.UserDb
import com.android.bodegas.domain.util.Resource
import com.android.bodegas.domain.util.Status
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.coroutineScope
import kotlin.coroutines.CoroutineContext

class UserDbImpl constructor(
    private val database: AppDatabase,
    private val userDbMapper: UserDbMapper
) : UserDb, CoroutineScope {

    private val job = Job()
    override val coroutineContext: CoroutineContext = Dispatchers.IO + job

    override suspend fun getUser(): Resource<UserEntity> {
        return coroutineScope {
            val userDb = database.userDbDao().getUser()
            val userEntity = userDbMapper.mapFromDb(userDb)
            return@coroutineScope Resource(
                Status.SUCCESS,
                userEntity,
                ""
            )
        }
    }

    override suspend fun saveUser(userEntity: UserEntity) {
        coroutineScope {
            database.userDbDao().insertUser(userDbMapper.mapToDb(userEntity))
        }
    }

    override suspend fun clearAllTables() {
        return coroutineScope {
            database.clearAllTables()
        }
    }

}