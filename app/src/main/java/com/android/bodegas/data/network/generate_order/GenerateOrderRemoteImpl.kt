package com.android.bodegas.data.network.generate_order

import android.util.Log
import com.android.bodegas.data.PreferencesManager
import com.android.bodegas.data.network.ResponseBodyError
import com.android.bodegas.data.network.generate_order.mapper.GenerateOrderEntityMapper
import com.android.bodegas.data.network.generate_order.model.GenerateOrderBody
import com.android.bodegas.data.network.generate_order.model.ListOrderBody
import com.android.bodegas.data.repository.generate_order.model.ListOrderEntity
import com.android.bodegas.data.repository.generate_order.store.remote.GenerateOrderRemote
import com.android.bodegas.data.repository.user.model.LoginEntity
import com.android.bodegas.domain.util.DateTimeHelper
import com.android.bodegas.domain.util.Resource
import com.android.bodegas.domain.util.Status
import com.google.gson.GsonBuilder
import kotlinx.coroutines.*
import java.io.IOException
import java.net.UnknownHostException
import java.util.*
import kotlin.coroutines.CoroutineContext


class GenerateOrderRemoteImpl constructor(
    private val generateOrderServices: GenerateOrderServices,
    private val generateOrderEntityMapper: GenerateOrderEntityMapper
) : GenerateOrderRemote, CoroutineScope {

    private val job = Job()
    override val coroutineContext: CoroutineContext = Dispatchers.IO + job

    override suspend fun generateOrder(
        deliveryType: String,
        total: String,
        listOrder: List<ListOrderEntity>,
        startTime: String,
        endTime: String,
        paymentMethodId:Int,
        deliveryCharge: Double,
        customerAmount: Double
    ): Resource<String> {
        return coroutineScope {
            val listOderBody = mutableListOf<ListOrderBody>()
            listOrder.forEach {
                listOderBody.add(generateOrderEntityMapper.mapFromRemote(it))
            }

            val generateOrderBody = GenerateOrderBody(
                PreferencesManager.getInstance().getUser().dni,
                deliveryType,
                PreferencesManager.getInstance().getEstablishmentSelected().storeId,
                listOderBody,
                startTime,
                endTime,
                total.toDouble(),
                paymentMethodId,
                deliveryCharge,
                customerAmount
            )

            try {
                val response = generateOrderServices.generateOrder(PreferencesManager.getInstance().getUser().customerId, generateOrderBody)
                Log.e("db", "generateOrderBody: "+generateOrderBody)
                if (response.isSuccessful) {
                    val body = response.body()?.orderResponse
                    return@coroutineScope Resource(
                        Status.SUCCESS,
                        body!!.orderId.toString(),
                        response.message()
                    )
                } else {
                    val gson =  GsonBuilder().create()
                    var mError =  ResponseBodyError()
                    try {
                        mError= gson.fromJson(response.errorBody()!!.string(), ResponseBodyError::class.java)
                        return@coroutineScope Resource(
                            Status.ERROR,
                            "",
                            mError.fields[0].fieldName + " - " + mError.fields[0].message
                        )
                    } catch ( e: IOException) {
                        return@coroutineScope Resource(
                            Status.ERROR,
                            "",
                            "Ha ocurrido un error, intente nuevamente por favor"
                        )
                    }
                }
            } catch (e: UnknownHostException) {
                return@coroutineScope Resource(
                    Status.ERROR,
                    "",
                    "Error de conexión"
                )
            } catch (e: Throwable) {
                return@coroutineScope Resource(
                    Status.ERROR,
                    "",
                    "Ha ocurrido un error, intente nuevamente por favor"
                )
            }
        }
    }
}