package com.android.bodegas.data.network.duplicateorder

import com.android.bodegas.data.network.duplicateorder.model.DuplicateOrderBody
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface DuplicateOrderServices {

    @GET("/customers/{establishment-Id}/duplicateOrder/{order-Id}")
    suspend fun getDuplicateOrder(
        @Path("establishment-Id") establishmentId: Int,
        @Path("order-Id") orderId: Int
    ): Response<DuplicateOrderBody>
}