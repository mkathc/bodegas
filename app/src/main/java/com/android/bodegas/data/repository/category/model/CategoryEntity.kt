package com.android.bodegas.data.repository.category.model

class CategoryEntity (
    val id: Int,
    val description: String,
    val name: String,
    val pathImage: String ? = ""
)
