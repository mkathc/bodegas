package com.android.bodegas.data.network.customerOrders

import com.android.bodegas.data.network.ResponseBodySuccess
import com.android.bodegas.data.network.customerOrders.model.CustomerListOrdersBody
import com.android.bodegas.data.network.customerOrders.model.DataItem
import com.android.bodegas.data.network.customerOrders.model.OrderBodyToUpdate
import com.android.bodegas.data.network.customerOrders.model.OrderDetailBodyToSend
import com.android.bodegas.data.network.user.model.FileResponse
import retrofit2.Response
import retrofit2.http.*

interface CustomerOrdersService {

    @GET("/customers/{customer-id}/orders")
    suspend fun getOrdersByCustomer(
        @Path("customer-id") customerId: Int,
        @Query("sortBy") sortBy: String
    ): Response<CustomerListOrdersBody>

    @PUT("/customers/{customer-id}/order/{order-id}")
    suspend fun updateStateOrder(
        @Path("customer-id") customerId: Int,
        @Path("order-id") orderId: Int,
        @Body orderBodyToUpdate: OrderBodyToUpdate
    ): Response<ResponseBodySuccess>


    @GET ("/common/order/{order-id}/excel")
    suspend fun getFileByOrder(
        @Path("order-id") orderId: Int,
        @Query("userType") userType: String
    ): Response<FileResponse>
}
