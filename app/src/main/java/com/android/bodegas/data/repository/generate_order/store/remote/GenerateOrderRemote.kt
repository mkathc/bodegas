package com.android.bodegas.data.repository.generate_order.store.remote

import com.android.bodegas.data.repository.generate_order.model.ListOrderEntity
import com.android.bodegas.domain.util.Resource


interface GenerateOrderRemote {

    suspend fun generateOrder(
        deliveryType: String,
        total: String,
        listOrder: List<ListOrderEntity>,
        startTime: String,
        endTime: String,
        paymentMethodId:Int,
        deliveryCharge:Double,
        customerAmount: Double
    ): Resource<String>

}