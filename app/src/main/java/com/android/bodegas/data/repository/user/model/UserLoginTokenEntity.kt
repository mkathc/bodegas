package com.android.bodegas.data.repository.user.model

data class UserLoginTokenEntity(
    val idEntity: String,
    val token: String
)