package com.android.bodegas.data.repository.customerOrders.mapper

import com.android.bodegas.data.repository.Mapper
import com.android.bodegas.data.repository.customerOrders.model.OrderDetailItemEntity
import com.android.bodegas.domain.customerOrders.OrderDetails

class OrderDetailItemMapper : Mapper<OrderDetailItemEntity, OrderDetails> {

    override fun mapFromEntity(type: OrderDetailItemEntity): OrderDetails {
        return OrderDetails(
        type.orderDetailOrderDetailId,
        type.orderDetailUnitMeasure,
        type.orderDetailQuantity,
        type.orderDetailPrice,
        type.orderDetailStatus,
        type.orderDetailObservation,
        type.orderDetailSubTotal,
        type.storeProductStoreProductId,
        type.storeProductPrice,
        type.storeProductStatus,
        type.productTemplateProductTemplateId,
        type.productTemplateCode,
        type.productTemplateName,
        type.productTemplateDescription,
        type.productTemplateUnitMeasure,
        type.productTemplateStatus,
        type.productTemplatePathImage
        )
    }

    override fun mapToEntity(type: OrderDetails): OrderDetailItemEntity {
        return OrderDetailItemEntity(
            type.orderDetailOrderDetailId,
            type.orderDetailUnitMeasure,
            type.orderDetailQuantity,
            type.orderDetailPrice,
            type.orderDetailStatus,
            type.orderDetailObservation,
            type.orderDetailSubTotal,
            type.storeProductStoreProductId,
            type.storeProductPrice,
            type.storeProductStatus,
            type.productTemplateProductTemplateId,
            type.productTemplateCode,
            type.productTemplateName,
            type.productTemplateDescription,
            type.productTemplateUnitMeasure,
            type.productTemplateStatus,
            type.productTemplatePathImage
        )
    }

}