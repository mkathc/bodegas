package com.android.bodegas.data.network.supplier.mapper

import com.android.bodegas.data.network.Mapper
import com.android.bodegas.data.network.supplier.model.PaymentMethodResponse
import com.android.bodegas.data.network.supplier.model.ScheduleResponse
import com.android.bodegas.data.network.supplier.model.SupplierResponse
import com.android.bodegas.data.repository.supplier.model.PaymentMethodEntity
import com.android.bodegas.data.repository.supplier.model.ScheduleEntity
import com.android.bodegas.data.repository.supplier.model.SupplierEntity

class SupplierEntityMapper(
    private val scheduleEntityMapper: ScheduleEntityMapper,
    private val paymentMethodEntityMapper: PaymentMethodEntityMapper
) : Mapper<SupplierResponse, SupplierEntity> {

    override fun mapFromRemote(type: SupplierResponse): SupplierEntity {
        return SupplierEntity(
            type.establishmentId,
            type.address,
            type.establishmentType.name,
            type.businessName,
            type.cardNumber,
            type.cardOperator,
            type.departament,
            type.district,
            type.dni,
            type.email,
            type.lastNameMaternal,
            type.lastNamePaternal,
            type.latitude,
            type.longitude,
            type.name,
            type.phoneNumber,
            type.province,
            type.ruc,
            type.status,
            type.urbanization,
            type.delivery,
            type.pathImage,
            type.deliveryCharge,
            getListSchedules(type.shippingSchedule),
            getListSchedules(type.operationSchedule),
            getListPaymentMethods(type.paymentMethod)
        )
    }

    private fun getListSchedules(list: List<ScheduleResponse>): List<ScheduleEntity>{
       val listSchedule = mutableListOf<ScheduleEntity>()
        list.forEach {
            listSchedule.add(scheduleEntityMapper.mapFromRemote(it))
        }
        return listSchedule
    }

    private fun getListPaymentMethods(list: List<PaymentMethodResponse>): List<PaymentMethodEntity>{
        val listSchedule = mutableListOf<PaymentMethodEntity>()
        if(list.isNotEmpty()){
            list.forEach {
                listSchedule.add(paymentMethodEntityMapper.mapFromRemote(it))
            }
        }
        return listSchedule
    }

}