package com.android.bodegas.data.network

interface Mapper<in M, out E> {

    fun mapFromRemote(type: M): E

}