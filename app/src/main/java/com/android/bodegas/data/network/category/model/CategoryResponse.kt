package com.android.bodegas.data.network.category.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class CategoryResponse (
    @SerializedName("categoryId")
    @Expose
    val id: Int,
    @SerializedName("description")
    @Expose
    val description: String,
    @SerializedName("name")
    @Expose
    val name: String,
    @SerializedName("pathImage")
    @Expose
    val pathImage: String ? = ""
)