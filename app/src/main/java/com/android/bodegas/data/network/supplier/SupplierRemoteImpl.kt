package com.android.bodegas.data.network.supplier

import android.content.Context
import com.android.bodegas.data.network.supplier.mapper.SupplierEntityMapper
import com.android.bodegas.data.network.supplier.model.SupplierItem
import com.android.bodegas.data.network.supplier.model.SupplierListBody
import com.android.bodegas.data.repository.supplier.model.SupplierEntity
import com.android.bodegas.data.repository.supplier.store.remote.SupplierRemote
import com.android.bodegas.domain.util.Resource
import com.android.bodegas.domain.util.Status
import com.google.gson.Gson
import kotlinx.coroutines.*
import java.io.InputStream
import java.io.InputStreamReader
import java.net.UnknownHostException
import java.nio.charset.StandardCharsets
import kotlin.coroutines.CoroutineContext


class SupplierRemoteImpl constructor(
    private val supplierServices: SupplierServices,
    private val supplierEntityMapper: SupplierEntityMapper,
    private val context: Context
) : SupplierRemote, CoroutineScope {

    private val job = Job()
    override val coroutineContext: CoroutineContext = Dispatchers.IO + job

    override suspend fun getSupplierByType(
        type: Int,
        latitude: String,
        longitude: String,
        radius: Int
    ): Resource<List<SupplierEntity>> {
        return coroutineScope {
            try {
                val result = supplierServices.getSupplierByType(type, latitude, longitude, radius)

                if (result.isSuccessful) {
                    if (result.code() == 200) {
                        if (result.body()!!.success) {
                            result.body()?.let {
                                val supplierEntityList = mapList(result.body()!!.supplierListBody)
                                supplierEntityList?.let {
                                    return@coroutineScope Resource(
                                        Status.SUCCESS,
                                        supplierEntityList,
                                        ""
                                    )
                                }
                            }

                        } else {
                            return@coroutineScope Resource(
                                Status.ERROR,
                                listOf<SupplierEntity>(),
                                ""
                            )
                        }
                    } else {
                        if (result.code() == 204) {
                            return@coroutineScope Resource(
                                Status.SUCCESS,
                                listOf<SupplierEntity>(),
                                "204"
                            )
                        }
                    }
                }

                return@coroutineScope Resource(Status.ERROR, listOf<SupplierEntity>(), "")
            } catch (e: UnknownHostException) {
                return@coroutineScope Resource(
                    Status.ERROR,
                    listOf<SupplierEntity>(),
                    "Not Connection"
                )
            } catch (e: Throwable) {
                e.printStackTrace()
                return@coroutineScope Resource(Status.ERROR, listOf<SupplierEntity>(), "")
            }
        }
    }

    private fun mapList(supplierListBody: SupplierItem?): List<SupplierEntity>? {
        val supplierList = ArrayList<SupplierEntity>()
        supplierListBody?.supplierList?.forEach {
            supplierList.add(supplierEntityMapper.mapFromRemote(it))
        }
        return supplierList
    }

    private suspend fun getMockedSuppliers(): Resource<List<SupplierEntity>> {
        return coroutineScope {
            delay(3000)
            try {
                val gson = Gson()
                val inputStream: InputStream = context.assets.open("mocked_supplier_list")
                val inputStreamReader = InputStreamReader(inputStream, StandardCharsets.UTF_8)
                val supplierList: SupplierListBody =
                    gson.fromJson(inputStreamReader, SupplierListBody::class.java)
                val supplierEntityList = mutableListOf<SupplierEntity>()
                supplierList.supplierListBody?.supplierList?.forEach { it ->
                    supplierEntityList.add(supplierEntityMapper.mapFromRemote(it))
                }
                return@coroutineScope Resource<List<SupplierEntity>>(
                    Status.SUCCESS,
                    supplierEntityList,
                    "Mocked list"
                )
            } catch (e: Throwable) {
                e.printStackTrace()
                return@coroutineScope Resource(Status.ERROR, listOf<SupplierEntity>(), "")
            }

        }
    }
}