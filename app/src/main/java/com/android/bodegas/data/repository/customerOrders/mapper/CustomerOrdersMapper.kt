package com.android.bodegas.data.repository.customerOrders.mapper

import com.android.bodegas.data.repository.customerOrders.model.CustomerOrdersEntity
import com.android.bodegas.data.repository.customerOrders.model.OrderDetailItemEntity
import com.android.bodegas.domain.customerOrders.CustomerOrders
import com.android.bodegas.domain.customerOrders.OrderDetails

class CustomerOrdersMapper (
    private val establishmentOrdersMapper: EstablishmentOrdersMapper,
    private val orderItemMapper: OrderDetailItemMapper
): com.android.bodegas.data.repository.Mapper<CustomerOrdersEntity, CustomerOrders> {
    override fun mapFromEntity(type: CustomerOrdersEntity): CustomerOrders {
        return CustomerOrders(
            type.orderId,
            type.deliveryType,
            type.orderDate,
            type.shippingDateFrom,
            type.shippingDateUntil,
            type.status,
            establishmentOrdersMapper.mapFromEntity(type.establishment),
            type.total,
            getOrdersDetail(type.orderDetails),
            type.updateDate,
            type.deliveryCharge,
            type.customerAmount,
            type.paymentMethodCustomerMessage,
            type.paymentMethodDescription,
            type.paymentMethodEstablishmentMessage,
            type.paymentMethodPaymentMethodId,
            type.paymentMethodRequestAmount
        )
    }

    override fun mapToEntity(type: CustomerOrders): CustomerOrdersEntity {
        return CustomerOrdersEntity(
            type.orderId,
            type.deliveryType,
            type.orderDate,
            type.shippingDateFrom,
            type.shippingDateUntil,
            type.status,
            establishmentOrdersMapper.mapToEntity(type.establishment),
            type.total,
            getOrdersDetailFromUsesCase(type.orderDetails),
            type.updateDate,
            type.deliveryCharge,
            type.customerAmount,
            type.paymentMethodCustomerMessage,
            type.paymentMethodDescription,
            type.paymentMethodEstablishmentMessage,
            type.paymentMethodPaymentMethodId,
            type.paymentMethodRequestAmount
        )
    }

    private fun getOrdersDetail(list: List<OrderDetailItemEntity>): List<OrderDetails>{
        val listSchedule = mutableListOf<OrderDetails>()
        list.forEach {
            listSchedule.add(orderItemMapper.mapFromEntity(it))
        }
        return listSchedule
    }

    private fun getOrdersDetailFromUsesCase(list: List<OrderDetails>): List<OrderDetailItemEntity>{
        val listSchedule = mutableListOf<OrderDetailItemEntity>()
        list.forEach {
            listSchedule.add(orderItemMapper.mapToEntity(it))
        }
        return listSchedule
    }
}
