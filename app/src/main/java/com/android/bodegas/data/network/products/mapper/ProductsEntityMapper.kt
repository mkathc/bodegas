package com.android.bodegas.data.network.products.mapper

import com.android.bodegas.data.network.Mapper
import com.android.bodegas.data.network.products.model.ProductsResponse
import com.android.bodegas.data.network.supplier.model.SupplierResponse
import com.android.bodegas.data.repository.produtcs.model.ProductsEntity
import com.android.bodegas.data.repository.supplier.model.SupplierEntity

class ProductsEntityMapper : Mapper<ProductsResponse, ProductsEntity> {

    override fun mapFromRemote(type: ProductsResponse): ProductsEntity {
        return ProductsEntity(
            type.storeProductId,
            type.price,
            type.status,
            type.productDetails.code,
            type.productDetails.name,
            type.productDetails.description,
            type.productDetails.unitMeasure,
            type.stock,
            type.productDetails.pathImage
        )
    }

}