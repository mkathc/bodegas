package com.android.bodegas.data.network.customerOrders.mapper

import com.android.bodegas.data.network.Mapper
import com.android.bodegas.data.network.customerOrders.model.OrderDetailBodyToSend
import com.android.bodegas.data.network.customerOrders.model.OrderDetailItem
import com.android.bodegas.data.repository.customerOrders.model.OrderDetailBodyEntity
import com.android.bodegas.data.repository.customerOrders.model.OrderDetailItemEntity
import com.android.bodegas.domain.customerOrders.OrderDetailBody

class OrderDetailBodyEntityMapper : Mapper<OrderDetailBodyEntity, OrderDetailBodyToSend> {

    override fun mapFromRemote(type: OrderDetailBodyEntity): OrderDetailBodyToSend {
        return OrderDetailBodyToSend(
            type.orderDetailId,
            type.state
        )
    }


}