package com.android.bodegas.data.repository.clases.store
import com.android.bodegas.data.repository.clases.model.ClassEntity
import com.android.bodegas.domain.util.Resource

interface ClassDataStore {
    suspend fun getClassesBySupplierId(
        establishmentId: Int,
        page: String,
        size: String,
        sortBy: String
    ) : Resource<List<ClassEntity>>
}