package com.android.bodegas.data.repository.duplicateOrder.store.remote

import com.android.bodegas.data.repository.duplicateOrder.model.DuplicateOrderEntity
import com.android.bodegas.data.repository.duplicateOrder.model.DuplicateOrderItemBodyEntity
import com.android.bodegas.data.repository.duplicateOrder.store.DuplicateOrderDataStore
import com.android.bodegas.domain.util.Resource

class DuplicateOrderRemoteDataStore constructor(
    private val duplicateOrderRemote: DuplicateOrderRemote ) : DuplicateOrderDataStore {

    override suspend fun getDuplicateOrder(establishmentId: Int, orderId: Int): Resource<DuplicateOrderEntity> {
        return  duplicateOrderRemote.getDuplicateOrder(establishmentId, orderId)
    }

}