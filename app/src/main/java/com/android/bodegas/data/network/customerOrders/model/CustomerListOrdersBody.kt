package com.android.bodegas.data.network.customerOrders.model

import com.android.bodegas.data.network.ResponseBodySuccess
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


class CustomerListOrdersBody : ResponseBodySuccess() {
    @SerializedName("data")
    @Expose
    var data: DataItem? = null
}

class DataItem (
    @SerializedName("ordersTotal")
    val ordersTotal: Int,
    @SerializedName("orders")
    val orders: List<CustomerListOrdersBodyResponse>
)
