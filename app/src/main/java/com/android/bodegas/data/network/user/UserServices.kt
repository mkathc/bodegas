package com.android.bodegas.data.network.user

import com.android.bodegas.data.network.ResponseBodySuccess
import com.android.bodegas.data.network.user.model.*
import com.android.bodegas.presentation.login.ForgetPassDTO
import com.android.bodegas.presentation.profile.ChangePasswordDtoBody
import com.android.bodegas.presentation.profile.UpdateCustomerBody
import okhttp3.RequestBody
import retrofit2.Response
import retrofit2.http.*

interface UserServices {

    @POST("/security/login")
    suspend fun loginUserToken(@Body userBody: UserBody): Response<UserLoginTokenBody>

    @GET("/customers/{customer-id}")
    suspend fun loginUser(@Path("customer-id") customerId: Int): Response<UserBodyResponse>

    @POST("/security/createCustomer")
    suspend fun registerUser(@Body registerUserBodyRemote: RegisterUserBodyRemote): Response<UserLoginTokenBody>

    @PUT("/common/upload/{id}")
    suspend fun updatePhotoUser(
        @Path("id") id: Int,
        @Body file : RequestBody
    ): Response<FileResponse>

    @PUT("/customers/{customer-id}")
    suspend fun updateUsers(
        @Path("customer-id") id: Int,
        @Body updateEstablishmentDTO : UpdateCustomerBody
    ): Response<UserBodyResponse>

    @PUT("/security/changepassword")
    suspend fun updatePassword(
        @Body changePasswordDto : ChangePasswordDtoBody
    ): Response<ResponseBodySuccess>

    @POST("/common/forgetpassword")
    suspend fun recoveryPassword(
        @Body changePasswordDto : ForgetPassDTO
    ): Response<ResponseBodySuccess>
}