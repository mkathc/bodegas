package com.android.bodegas.data.network.supplier.mapper

import com.android.bodegas.data.network.Mapper
import com.android.bodegas.data.network.supplier.model.PaymentMethodResponse
import com.android.bodegas.data.network.supplier.model.ScheduleResponse
import com.android.bodegas.data.network.supplier.model.SupplierResponse
import com.android.bodegas.data.repository.supplier.model.PaymentMethodEntity
import com.android.bodegas.data.repository.supplier.model.ScheduleEntity
import com.android.bodegas.data.repository.supplier.model.SupplierEntity

class PaymentMethodEntityMapper : Mapper<PaymentMethodResponse, PaymentMethodEntity> {

    override fun mapFromRemote(type: PaymentMethodResponse): PaymentMethodEntity {
        return PaymentMethodEntity(
            type.paymentMethodId,
            type.requestedAmount,
            type.description,
            type.customerMessage
        )
    }

}