package com.android.bodegas.data.network.provinces.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class ProvinceResponse (
    @SerializedName("id")
    @Expose
    val id: String,
    @SerializedName("name")
    @Expose
    val name: String,
    @SerializedName("department_id")
    @Expose
    val department_id: String
)