package com.android.bodegas.data.repository.customerOrders.mapper

import com.android.bodegas.data.repository.Mapper
import com.android.bodegas.data.repository.customerOrders.model.EstablishmentOrdersEntity
import com.android.bodegas.data.repository.customerOrders.model.OrderDetailItemEntity
import com.android.bodegas.data.repository.supplier.mapper.PaymentMethodMapper
import com.android.bodegas.data.repository.supplier.mapper.ScheduleMapper
import com.android.bodegas.data.repository.supplier.model.PaymentMethodEntity
import com.android.bodegas.data.repository.supplier.model.ScheduleEntity
import com.android.bodegas.domain.customerOrders.EstablishmentOrders
import com.android.bodegas.domain.customerOrders.OrderDetails
import com.android.bodegas.domain.supplier.PaymentMethod
import com.android.bodegas.domain.supplier.Schedule

class EstablishmentOrdersMapper(
    private val scheduleMapper: ScheduleMapper,
    private val paymentMethodMapper: PaymentMethodMapper
) : Mapper<EstablishmentOrdersEntity, EstablishmentOrders> {

    override fun mapFromEntity(type: EstablishmentOrdersEntity): EstablishmentOrders {
        return EstablishmentOrders(
        type.establishmentId,
        type.establishmentEmail,
        type.establishmentName,
        type.delivery,
        getListSchedules(type.shippingSchedule),
        getListSchedules(type.operationSchedule),
        getListPaymentMethods(type.paymentMethods),
        type.deliveryCharge,
        type.establishmentAddress
        )
    }

    override fun mapToEntity(type: EstablishmentOrders): EstablishmentOrdersEntity {
        return EstablishmentOrdersEntity(
            type.establishmentId,
            type.establishmentEmail,
            type.establishmentName,
            type.delivery,
            getListSchedulesFromUsesCase(type.shippingSchedule),
            getListSchedulesFromUsesCase(type.operationSchedule),
            getListPaymentMethodsFromUsesCases(type.paymentMethods),
            type.deliveryCharge,
            type.establishmentAddress
        )
    }

    private fun getListSchedules(list: List<ScheduleEntity>): List<Schedule>{
        val listSchedule = mutableListOf<Schedule>()
        list.forEach {
            listSchedule.add(scheduleMapper.mapFromEntity(it))
        }
        return listSchedule
    }

    private fun getListSchedulesFromUsesCase(list: List<Schedule>): List<ScheduleEntity>{
        val listSchedule = mutableListOf<ScheduleEntity>()
        list.forEach {
            listSchedule.add(scheduleMapper.mapToEntity(it))
        }
        return listSchedule
    }

    private fun getListPaymentMethods(list: List<PaymentMethodEntity>): List<PaymentMethod>{
        val listSchedule = mutableListOf<PaymentMethod>()
        list.forEach {
            listSchedule.add(paymentMethodMapper.mapFromEntity(it))
        }
        return listSchedule
    }

    private fun getListPaymentMethodsFromUsesCases(list: List<PaymentMethod>): List<PaymentMethodEntity>{
        val listSchedule = mutableListOf<PaymentMethodEntity>()
        list.forEach {
            listSchedule.add(paymentMethodMapper.mapToEntity(it))
        }
        return listSchedule
    }

}