package com.android.bodegas.data

import android.app.Application
import android.content.Context
import android.content.SharedPreferences
import com.android.bodegas.presentation.login.model.UserLoginTokenView
import com.android.bodegas.presentation.profile.UpdateCustomerBody
import com.android.bodegas.presentation.register_user.model.UserView
import com.android.bodegas.presentation.supplier.model.SupplierView
import com.google.gson.Gson
import org.koin.core.KoinComponent
import org.koin.core.inject

class PreferencesManager : KoinComponent {

    companion object {
        const val MY_USER_LOGIN_TOKEN = "my_user_login_token"
        const val USER_TOKEN = "token"
        const val ESTABLISHMENT_SELECTED = "establishment_selected"
        const val IS_INVITED = "is_invited"
        const val IS_CHARGE_FOR_DELIVERY = "is_charge_for_delivery"
        const val USER = "user"
        const val UPDATE_DATA_USER = "update_data_user"
        const val CURRENT_LATITUDE_OF_USER = "current_latitude_of_User"
        const val CURRENT_LONGITUDE_OF_USER = "current_longitude_of_User"

        private var ourInstance: PreferencesManager? = null

        fun getInstance(): PreferencesManager {
            if (ourInstance == null) {
                ourInstance = PreferencesManager()
            }
            return ourInstance as PreferencesManager
        }
    }

    private val application: Application by inject()

    private var preferences: SharedPreferences
    private val editor: SharedPreferences.Editor

    init {
        preferences = application.getSharedPreferences(
            application.applicationContext.packageName,
            Context.MODE_PRIVATE
        )
        editor = preferences.edit()
    }

    fun isInvited(isInvited: Boolean) {
        editor.putBoolean(IS_INVITED, isInvited).apply()
    }

    fun getIsInvited(): Boolean {
        return preferences.getBoolean(IS_INVITED, true)
    }

    fun setUserToken(token: String) {
        editor.putString(USER_TOKEN, token).apply()
    }

    fun getUserToken(): String {
        return preferences.getString(USER_TOKEN, "")!!
    }

    fun closeSession() {
        editor.clear()
        editor.commit() // commit changes
    }

    fun setEstablishmentSelected(establishment: SupplierView) {
        val gson = Gson()
        val json = gson.toJson(establishment)
        editor.putString(ESTABLISHMENT_SELECTED, json)
        editor.commit()
    }

    fun getEstablishmentSelected(): SupplierView {
        val gson = Gson()
        val json = preferences.getString(ESTABLISHMENT_SELECTED, "");
        var obj = gson.fromJson(json, SupplierView::class.java)
        if(obj == null){
            obj = SupplierView(0,"","","",0.0,0.0,
                mutableListOf(), mutableListOf(), "" ,"", true, mutableListOf(), true)
        }
        return obj
    }

    fun cleanEstablishment(){
        editor.putString(ESTABLISHMENT_SELECTED, "").commit()
    }

    fun setUser(user: UserView) {
        val gson = Gson()
        val json = gson.toJson(user)
        editor.putString(USER, json)
        editor.commit()
    }

    fun getUser(): UserView {
        val gson = Gson()
        val json = preferences.getString(USER, "");
        var obj = gson.fromJson(json, UserView::class.java)
        if(obj == null){
            obj = UserView(0,"","","", "" ,"","","","",
                "",0.0,0.0, "" ,"", "", "","","","")
        }
        return obj
    }


    fun saveData(key: String, value : String){
        editor.putString(key,value).apply()
    }

    fun getData(key:String) : String {
        return preferences.getString(key,"").toString()
    }

    fun saveBodyUpdateUser(data: UpdateCustomerBody) {
        val gson = Gson()
        val json = gson.toJson(data)
        editor.putString(UPDATE_DATA_USER, json)
        editor.commit()
    }

    fun getBodyUpdateUser(): UpdateCustomerBody {
        val gson = Gson()
        val json = preferences.getString(UPDATE_DATA_USER, "");
        var obj = gson.fromJson(json, UpdateCustomerBody::class.java)
        if (obj == null) {
            obj = UpdateCustomerBody(
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                0.0,
                0.0
            )
        }
        return obj
    }

    fun cleanUpdateUser(){
        editor.putString(UPDATE_DATA_USER, "").commit()
    }

    fun setCustomerLoginTokenSelected(establishment: UserLoginTokenView) {
        val gson = Gson()
        val json = gson.toJson(establishment)
        editor.putString(MY_USER_LOGIN_TOKEN, json)
        editor.commit()
    }

    fun getCustomerLoginTokenSelected(): UserLoginTokenView {
        val gson = Gson()
        val json = preferences.getString(MY_USER_LOGIN_TOKEN, "");
        var obj = gson.fromJson(json, UserLoginTokenView::class.java)
        if(obj == null){
            obj = UserLoginTokenView("","")
        }
        return obj
    }

    fun setChargeForDelivery(flag: Boolean) {
        editor.putBoolean(IS_CHARGE_FOR_DELIVERY, flag).apply()
    }

    fun getChargeForDelivery(): Boolean {
        return preferences.getBoolean(IS_CHARGE_FOR_DELIVERY, true)
    }

    fun setCurrentLongitudeOfUser(newCurrentLongitudeOfUser: Float) {
        editor.putFloat(CURRENT_LONGITUDE_OF_USER, newCurrentLongitudeOfUser).apply()
    }

    fun getCurrentLongitudeOfUser(): Float {
        if (preferences.getFloat(CURRENT_LONGITUDE_OF_USER, 0.00f) == 0.00f) {
            return getUser().longitude.toFloat()
        }
        return preferences.getFloat(CURRENT_LONGITUDE_OF_USER, 0.00f)
    }

    fun setCurrentLatitudeOfUser(newCurrentLatitudeOfUser: Float) {
        editor.putFloat(CURRENT_LATITUDE_OF_USER, newCurrentLatitudeOfUser).apply()
    }

    fun getCurrentLatitudeOfUser(): Float {
        if (preferences.getFloat(CURRENT_LATITUDE_OF_USER, 0.00f) == 0.00f) {
            return getUser().latitude.toFloat()
        }
        return preferences.getFloat(CURRENT_LATITUDE_OF_USER, 0.00f)
    }

}
