package com.android.bodegas.data.repository.produtcs.store.remote

import com.android.bodegas.data.repository.produtcs.model.ProductsEntity
import com.android.bodegas.domain.util.Resource


interface ProductsRemote {

    suspend fun getProducts(
        establishmentId: Int,
        supplierDocument:String,
        subCategoryId: Int
    ): Resource<List<ProductsEntity>>

    suspend fun searchProducts(
        searchText:String,
        storeId : Int
    ): Resource<List<ProductsEntity>>

}