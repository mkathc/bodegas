package com.android.bodegas.data.network.customerOrders.mapper

import com.android.bodegas.data.network.Mapper
import com.android.bodegas.data.network.customerOrders.model.EstablishmentItem
import com.android.bodegas.data.network.customerOrders.model.OrderDetailItem
import com.android.bodegas.data.network.supplier.mapper.PaymentMethodEntityMapper
import com.android.bodegas.data.network.supplier.mapper.ScheduleEntityMapper
import com.android.bodegas.data.network.supplier.model.PaymentMethodResponse
import com.android.bodegas.data.network.supplier.model.ScheduleResponse
import com.android.bodegas.data.repository.customerOrders.model.EstablishmentOrdersEntity
import com.android.bodegas.data.repository.customerOrders.model.OrderDetailItemEntity
import com.android.bodegas.data.repository.supplier.model.PaymentMethodEntity
import com.android.bodegas.data.repository.supplier.model.ScheduleEntity

class EstablishmentOrdersEntityMapper(
    private val scheduleEntityMapper: ScheduleEntityMapper,
    private val paymentMethodEntityMapper: PaymentMethodEntityMapper
): Mapper<EstablishmentItem, EstablishmentOrdersEntity> {

    override fun mapFromRemote(type: EstablishmentItem): EstablishmentOrdersEntity {
        return EstablishmentOrdersEntity(
            type.establishmentId,
            type.email,
            type.businessName,
            type.delivery,
            getListSchedules(type.shippingSchedule),
            getListSchedules(type.operationSchedule),
            getListPaymentMethod(type.paymentMethods),
            type.deliveryCharge,
            type.address
        )
    }

    private fun getListSchedules(list: List<ScheduleResponse>): List<ScheduleEntity>{
        val listSchedule = mutableListOf<ScheduleEntity>()
        list.forEach {
            listSchedule.add(scheduleEntityMapper.mapFromRemote(it))
        }
        return listSchedule
    }

    private fun getListPaymentMethod(list: List<PaymentMethodResponse>): List<PaymentMethodEntity>{
        val listPaymentMethod = mutableListOf<PaymentMethodEntity>()
        list.forEach {
            listPaymentMethod.add(paymentMethodEntityMapper.mapFromRemote(it))
        }
        return listPaymentMethod
    }


}