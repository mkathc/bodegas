package com.android.bodegas.data.repository.districts.mapper

import com.android.bodegas.data.repository.Mapper
import com.android.bodegas.data.repository.districts.model.ProvinceEntity
import com.android.bodegas.domain.district.District

class DistrictMapper: Mapper<ProvinceEntity, District> {
    override fun mapFromEntity(type: ProvinceEntity): District {
        return District(type.id, type.name,type.province_id, type.department_id)
    }

    override fun mapToEntity(type: District): ProvinceEntity {
        return ProvinceEntity(type.id, type.name,type.province_id, type.department_id)
    }
}