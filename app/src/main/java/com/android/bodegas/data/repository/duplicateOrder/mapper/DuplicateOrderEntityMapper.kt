package com.android.bodegas.data.repository.duplicateOrder.mapper

import com.android.bodegas.data.repository.Mapper
import com.android.bodegas.data.repository.duplicateOrder.model.DuplicateOrderEntity
import com.android.bodegas.data.repository.duplicateOrder.model.DuplicateOrderItemBodyEntity
import com.android.bodegas.domain.duplicate_order.DuplicateOrder
import com.android.bodegas.domain.duplicate_order.DuplicateOrderItem

class DuplicateOrderEntityMapper (
    private val listDuplicateOrderItemMapper: ListDuplicateOrderItemMapper
) : Mapper<DuplicateOrderEntity, DuplicateOrder> {
    override fun mapFromEntity(type: DuplicateOrderEntity): DuplicateOrder {
        return DuplicateOrder(
            type.establishmentId,
            type.flagAllAvailableProducts,
            getListOrderDetailRequest(type.orderDetailRequest)
        )
    }

    override fun mapToEntity(type: DuplicateOrder): DuplicateOrderEntity {
        return DuplicateOrderEntity(
            type.establishmentId,
            type.flagAllAvailableProducts,
            getListOrderDetailRequestFromUsesCase(type.orderDetailRequest)
        )
    }

    private fun getListOrderDetailRequest(list: List<DuplicateOrderItemBodyEntity>): List<DuplicateOrderItem>{
        val list1 = mutableListOf<DuplicateOrderItem>()
        list.forEach {
            list1.add(listDuplicateOrderItemMapper.mapFromEntity(it))
        }
        return list1
    }

    private fun getListOrderDetailRequestFromUsesCase(list: List<DuplicateOrderItem>): List<DuplicateOrderItemBodyEntity>{
        val list2 = mutableListOf<DuplicateOrderItemBodyEntity>()
        list.forEach {
            list2.add(listDuplicateOrderItemMapper.mapToEntity(it))
        }
        return list2
    }

}