package com.android.bodegas.data.network.duplicateorder.mapper

import com.android.bodegas.data.network.duplicateorder.DuplicateOrderServices
import com.android.bodegas.data.repository.duplicateOrder.model.DuplicateOrderEntity
import com.android.bodegas.data.repository.duplicateOrder.store.remote.DuplicateOrderRemote
import com.android.bodegas.domain.util.Resource
import com.android.bodegas.domain.util.Status
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.coroutineScope
import java.net.UnknownHostException
import kotlin.coroutines.CoroutineContext

class DuplicateOrderRemoteImpl constructor(
    private val duplicateOrderServices: DuplicateOrderServices,
    private val duplicateOrderEntityMapper: DuplicateOrderEntityMapperNetwork
) : DuplicateOrderRemote, CoroutineScope {

    private val job = Job()
    override val coroutineContext: CoroutineContext = Dispatchers.IO + job

    override suspend fun getDuplicateOrder(establishmentId: Int, orderId: Int): Resource<DuplicateOrderEntity>
    {
        return coroutineScope {
            try {
                val response = duplicateOrderServices.getDuplicateOrder(establishmentId, orderId)
                val emptyDuplicateOrderEntity = DuplicateOrderEntity(
                    0,
                    false,
                    mutableListOf()
                )

                if (response.isSuccessful) {
                    if(response.body()!!.success){
                        val duplicateOrderResponse = response.body()!!.data!!.duplicateOrder
                        val duplicateOrderEntity = duplicateOrderResponse.let {
                            duplicateOrderEntityMapper.mapFromRemote(
                                it
                            )
                        }

                        return@coroutineScope Resource(
                            Status.SUCCESS,
                            duplicateOrderEntity,
                            response.message()
                        )
                    }
                    else {
                        val message = response.body()!!.message
                        return@coroutineScope Resource(
                            Status.ERROR,
                            emptyDuplicateOrderEntity,
                            message
                        )
                    }
                } else {
                    return@coroutineScope Resource(
                        Status.ERROR,
                        emptyDuplicateOrderEntity,
                        response.message()
                    )
                }
            } catch (e: UnknownHostException) {
                val emptyDuplicateOrderEntity = DuplicateOrderEntity(
                    0,
                    false,
                    mutableListOf()
                )

                return@coroutineScope Resource(
                    Status.ERROR,
                    emptyDuplicateOrderEntity,
                    "500"
                )
            } catch (e: Throwable) {
                val emptyDuplicateOrderEntity = DuplicateOrderEntity(
                    0,
                    false,
                    mutableListOf()
                )

                return@coroutineScope Resource(
                    Status.ERROR,
                    emptyDuplicateOrderEntity,
                    e.message
                )
            }
        }
    }
}