package com.android.bodegas.data.repository.generate_order

import com.android.bodegas.data.repository.generate_order.mapper.ListOrderMapper
import com.android.bodegas.data.repository.generate_order.model.ListOrderEntity
import com.android.bodegas.data.repository.generate_order.store.remote.GenerateOrderRemoteDataStore
import com.android.bodegas.data.repository.save_order.store.db.SaveOrderDbDataStore
import com.android.bodegas.domain.generate_order.GenerateOrderRepository
import com.android.bodegas.domain.util.Resource
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.coroutineScope
import kotlin.coroutines.CoroutineContext

class GenerateOrderDataRepository constructor(
    private val generateOrderRemoteDataStore: GenerateOrderRemoteDataStore,
    private val saveOrder: SaveOrderDbDataStore,
    private val listOrderMapper: ListOrderMapper
) : GenerateOrderRepository, CoroutineScope {

    private val job = Job()
    override val coroutineContext: CoroutineContext = Dispatchers.IO + job


    override suspend fun generateOrder(
        deliveryType: String,
        total: String,
        startTime: String,
        endTime: String,
        paymentMethodId:Int,
        customerAmount: Double,
        deliveryCharge: Double
    ): Resource<String> {
        return coroutineScope {
            val getOrders = saveOrder.getOrder()
            val listOrder = mutableListOf<ListOrderEntity>()
            getOrders.data.forEach {
                listOrder.add(listOrderMapper.mapFromEntity(it))
            }

            val generateOrderResponse = generateOrderRemoteDataStore.generateOrder(
                deliveryType,
                total, listOrder, startTime, endTime, paymentMethodId, customerAmount, deliveryCharge
            )
            return@coroutineScope Resource(
                generateOrderResponse.status,
                generateOrderResponse.data,
                generateOrderResponse.message
            )
        }
    }
}