package com.android.bodegas.data.db.user.model

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.android.bodegas.data.db.user.dao.UserConstants

@Entity(tableName = UserConstants.TABLE_NAME)
data class UserDb(
    val customerId: Int,
    var creationDate: String,
    var creationUser: String,
    var departament: String,
    var district: String,
    var dni: String,
    var email: String,
    var address: String,
    var lastNameMaternal: String,
    var lastNamePaternal: String,
    var latitude: Double,
    var longitude: Double,
    var name: String,
    var phoneNumber: String,
    var province: String,
    var ruc: String,
    var status: String,
    var urbanization: String,
    var pathImage: String ? = ""
) {
    @PrimaryKey(autoGenerate = true)
    var id: Long = 0
}
