package com.android.bodegas.data.db

import androidx.room.Database
import androidx.room.RoomDatabase
import com.android.bodegas.data.db.save_order.dao.SaveOrderDao
import com.android.bodegas.data.db.save_order.model.SaveOrderDb
import com.android.bodegas.data.db.user.model.UserDb
import com.android.bodegas.data.db.user.dao.UserDao

@Database(
    entities = [UserDb::class, SaveOrderDb::class],
    version = 1,
    exportSchema = false
)
abstract class AppDatabase : RoomDatabase() {

    abstract fun userDbDao(): UserDao
    abstract fun saveOrderDao(): SaveOrderDao

}
