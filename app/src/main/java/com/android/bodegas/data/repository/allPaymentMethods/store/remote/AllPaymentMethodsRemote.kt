package com.android.bodegas.data.repository.allPaymentMethods.store.remote

import com.android.bodegas.domain.util.Resource
import com.android.bodegasadmin.data.repository.allPaymentMethods.model.AllPaymentMethodsEntity

interface AllPaymentMethodsRemote {
    suspend fun getAllPaymentMethods(): Resource<List<AllPaymentMethodsEntity>>
}