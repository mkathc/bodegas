package com.android.bodegas.data.repository.clases.store.remote

import com.android.bodegas.data.repository.clases.model.ClassEntity
import com.android.bodegas.data.repository.clases.store.ClassDataStore
import com.android.bodegas.domain.util.Resource

class ClassRemoteDataStorage constructor(private val classRemote: ClassRemote) :
    ClassDataStore
    {
        override suspend fun getClassesBySupplierId(
            establishmentId: Int,
            page: String,
            size: String,
            sortBy: String
        ): Resource<List<ClassEntity>> {
            return classRemote.getClassesBySupplierId(establishmentId, page, size, sortBy)
        }
    }