package com.android.bodegas.data.repository.sub_sub_category.store

import com.android.bodegas.data.repository.sub_sub_category.mapper.GroupMapper
import com.android.bodegas.data.repository.sub_sub_category.store.remote.SSubCategoryRemoteDataStore
import com.android.bodegas.domain.products.Products
import com.android.bodegas.domain.sub_sub_category.Group
import com.android.bodegas.domain.sub_sub_category.SSubCategoryRepository
import com.android.bodegas.domain.util.Resource
import com.android.bodegas.domain.util.Status
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.coroutineScope
import kotlin.coroutines.CoroutineContext

class SSubCategoryDataRepository(
    private val sSubCategoryRemoteDataStore: SSubCategoryRemoteDataStore,
    private val groupMapper: GroupMapper
) : SSubCategoryRepository, CoroutineScope {

    private val job = Job()
    override val coroutineContext: CoroutineContext = Dispatchers.IO + job

    override suspend fun getSSubCategory(
        establishmentId: Int,
        subCategoryId: Int
    ): Resource<Group> {
        return coroutineScope {
            val ssubCategoryList =
                sSubCategoryRemoteDataStore.getSSubCategory(establishmentId, subCategoryId)

            var resource: Resource<Group> =
                Resource(ssubCategoryList.status, Group(0, mutableListOf(), mutableListOf()), ssubCategoryList.message)

            if (ssubCategoryList.status == Status.SUCCESS) {
                val group = groupMapper.mapFromEntity(ssubCategoryList.data)
                resource = Resource(Status.SUCCESS, group, ssubCategoryList.message)
            } else {
                if (ssubCategoryList.status == Status.ERROR) {
                    resource = Resource(ssubCategoryList.status, Group(0, mutableListOf(), mutableListOf()), ssubCategoryList.message)
                }
            }
            return@coroutineScope resource
        }
    }
}