package com.android.bodegas.data.repository.user.mapper

import com.android.bodegas.data.repository.Mapper
import com.android.bodegas.data.repository.user.model.LoginEntity
import com.android.bodegas.data.repository.user.model.UserEntity
import com.android.bodegas.domain.user.create.model.User
import com.android.bodegas.domain.user.login.model.UserLogin


class UserMapper : Mapper<UserEntity, User> {
    override fun mapFromEntity(type: UserEntity): User {
        return User(
            type.customerId,
            type.creationDate,
            type.creationUser,
            type.departament,
            type.district,
            type.dni,
            type.email,
            type.address,
            type.lastNameMaternal,
            type.lastNamePaternal,
            type.latitude,
            type.longitude,
            type.name,
            type.phoneNumber,
            type.province,
            type.ruc,
            type.status,
            type.urbanization,
            type.pathImage
        )
    }

    override fun mapToEntity(type: User): UserEntity {
        return UserEntity(
            type.customerId,
            type.creationDate,
            type.creationUser,
            type.departament,
            type.district,
            type.dni,
            type.email,
            type.address,
            type.lastNameMaternal,
            type.lastNamePaternal,
            type.latitude,
            type.longitude,
            type.name,
            type.phoneNumber,
            type.province,
            type.ruc,
            type.status,
            type.urbanization,
            type.pathImage
        )
    }

}