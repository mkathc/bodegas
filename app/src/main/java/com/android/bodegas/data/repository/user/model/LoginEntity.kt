package com.android.bodegas.data.repository.user.model

data class LoginEntity(val token: String, val userName: String)


