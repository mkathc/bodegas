package com.android.bodegas.data.network.products.model

import com.android.bodegas.data.network.ResponseBodySuccess
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


class ProductsListBody : ResponseBodySuccess() {
    @SerializedName("data")
    @Expose
    var productsListBody: ProductsItem? = null
}

class ProductsItem(
    @SerializedName("productsTotal")
    val productsTotal: Int,
    @SerializedName("storeProducts")
    val storeProducts: List<ProductsResponse>
)
