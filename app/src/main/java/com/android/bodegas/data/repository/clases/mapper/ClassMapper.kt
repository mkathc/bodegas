package com.android.bodegas.data.repository.clases.mapper

import com.android.bodegas.data.repository.clases.model.ClassEntity
import com.android.bodegas.domain.clases.Class
import com.android.bodegas.data.repository.Mapper

//conectar con el dominio
class ClassMapper : Mapper<ClassEntity, Class> {
    override fun mapFromEntity(type: ClassEntity): Class {
        return Class(
            type.id,
            type.description,
            type.name,
            type.pathImage
        )
    }

    override fun mapToEntity(type: Class): ClassEntity {
        return ClassEntity(
            type.id,
            type.description,
            type.name,
            type.pathImage
        )
    }

}