package com.android.bodegas.data.db.save_order.mapper

import com.android.bodegas.data.db.Mapper
import com.android.bodegas.data.db.save_order.model.SaveOrderDb
import com.android.bodegas.data.repository.save_order.model.SaveOrderEntity
import com.android.bodegas.domain.util.DateTimeHelper
import java.util.*

class SaveOrderDbMapper :
    Mapper<SaveOrderDb, SaveOrderEntity> {

    override fun mapFromDb(type: SaveOrderDb): SaveOrderEntity {
        return SaveOrderEntity(
            type.id,
            type.storeProductId,
            type.price,
            type.quantity,
            type.subtotal,
            type.unitMeasure,
            type.observation,
            type.name,
            type.imageProduct
        )
    }

    override fun mapToDb(type: SaveOrderEntity): SaveOrderDb {
        return SaveOrderDb(
            type.storeProductId.toString() + "-" + DateTimeHelper.parseCalendar(Calendar.getInstance()),
            type.storeProductId,
            type.price,
            type.quantity,
            type.subtotal,
            type.unitMeasure,
            type.observation,
            type.name,
            type.imageProduct
        )
    }

}