package com.android.bodegas.data.network.departments

import com.android.bodegas.data.network.departments.model.DepartmentListBody
import retrofit2.Response
import retrofit2.http.GET

interface DepartmentService {
        @GET("/departments")
        suspend fun getDeparments() : Response<DepartmentListBody>
    }