package com.android.bodegas.di

import androidx.room.Room
import com.android.bodegas.BuildConfig
import com.android.bodegas.data.PreferencesManager
import com.android.bodegas.data.db.AppDatabase
import com.android.bodegas.data.db.save_order.SaverOrderDbImpl
import com.android.bodegas.data.db.save_order.mapper.SaveOrderDbMapper
import com.android.bodegas.data.db.user.UserDbImpl
import com.android.bodegas.data.db.user.mapper.UserDbMapper

import com.android.bodegas.data.network.category.CategoryRemoteImpl
import com.android.bodegas.data.network.category.CategoryServiceFactory
import com.android.bodegas.data.network.category.mapper.CategoryEntityMapper
import com.android.bodegas.data.network.classes.ClassRemoteImpl
import com.android.bodegas.data.network.classes.ClassServiceFactory
import com.android.bodegas.data.network.classes.mapper.ClassEntityMapper
import com.android.bodegas.data.network.customerOrders.CustomerOrdersRemoteImpl
import com.android.bodegas.data.network.customerOrders.CustomerOrdersServiceFactory
import com.android.bodegas.data.network.customerOrders.mapper.CustomerOrdersEntityMapper
import com.android.bodegas.data.network.customerOrders.mapper.EstablishmentOrdersEntityMapper
import com.android.bodegas.data.network.customerOrders.mapper.OrderDetailBodyEntityMapper
import com.android.bodegas.data.network.customerOrders.mapper.OrderDetailItemEntityMapper
import com.android.bodegas.data.network.generate_order.GenerateOrderRemoteImpl
import com.android.bodegas.data.network.generate_order.GenerateOrderServiceFactory
import com.android.bodegas.data.network.generate_order.mapper.GenerateOrderEntityMapper
import com.android.bodegas.data.network.departments.DepartmentRemoteImpl
import com.android.bodegas.data.network.departments.DepartmentServiceFactory
import com.android.bodegas.data.network.departments.mapper.DepartmentEntityMapper
import com.android.bodegas.data.network.district.DistrictRemoteImpl
import com.android.bodegas.data.network.district.DistrictServiceFactory
import com.android.bodegas.data.network.district.mapper.DistrictEntityMapper
import com.android.bodegas.data.network.duplicateorder.DuplicateOrderServiceFactory
import com.android.bodegas.data.network.duplicateorder.mapper.DuplicateOrderEntityMapperNetwork
import com.android.bodegas.data.network.duplicateorder.mapper.DuplicateOrderItemEntityMapper
import com.android.bodegas.data.network.duplicateorder.mapper.DuplicateOrderRemoteImpl
import com.android.bodegas.data.network.subcategory.SubCategoryServiceFactory
import com.android.bodegas.data.network.subcategory.SubcategoryRemoteImpl
import com.android.bodegas.data.network.subcategory.mapper.SubcategoryEntityMapper

import com.android.bodegas.data.network.products.ProductsRemoteImpl
import com.android.bodegas.data.network.products.ProductsServiceFactory
import com.android.bodegas.data.network.products.mapper.ProductsEntityMapper
import com.android.bodegas.data.network.provinces.ProvinceRemoteImpl
import com.android.bodegas.data.network.provinces.ProvinceServiceFactory
import com.android.bodegas.data.network.provinces.mapper.ProvinceEntityMapper
import com.android.bodegas.data.network.sub_sub_categoty.SSubCategoryRemoteImpl
import com.android.bodegas.data.network.sub_sub_categoty.SSubCategoryServiceFactory
import com.android.bodegas.data.network.sub_sub_categoty.mapper.GroupEntityMapper
import com.android.bodegas.data.network.sub_sub_categoty.mapper.SSubcategoryEntityMapper

import com.android.bodegas.data.network.supplier.SupplierRemoteImpl
import com.android.bodegas.data.network.supplier.SupplierServiceFactory
import com.android.bodegas.data.network.supplier.mapper.PaymentMethodEntityMapper
import com.android.bodegas.data.network.supplier.mapper.ScheduleEntityMapper
import com.android.bodegas.data.network.supplier.mapper.SupplierEntityMapper
import com.android.bodegas.data.network.user.UserRemoteImpl
import com.android.bodegas.data.network.user.UserServiceFactory
import com.android.bodegas.data.network.user.mapper.RegisterUserEntityMapper
import com.android.bodegas.data.network.user.mapper.UserEntityMapper
import com.android.bodegas.data.network.user.mapper.UserLoginEntityMapper
import com.android.bodegas.data.network.user.mapper.UserLoginTokenEntityMapper

import com.android.bodegas.data.repository.category.CategoryDataRepository
import com.android.bodegas.data.repository.category.mapper.CategoryMapper
import com.android.bodegas.data.repository.category.store.remote.CategoryRemote
import com.android.bodegas.data.repository.category.store.remote.CategoryRemoteDataStorage
import com.android.bodegas.data.repository.clases.ClassDataRepository
import com.android.bodegas.data.repository.clases.mapper.ClassMapper
import com.android.bodegas.data.repository.clases.store.remote.ClassRemote
import com.android.bodegas.data.repository.clases.store.remote.ClassRemoteDataStorage
import com.android.bodegas.data.repository.customerOrders.CustomerOrdersDataRepository
import com.android.bodegas.data.repository.customerOrders.mapper.CustomerOrdersMapper
import com.android.bodegas.data.repository.customerOrders.mapper.EstablishmentOrdersMapper
import com.android.bodegas.data.repository.customerOrders.mapper.OrderDetailBodyMapper
import com.android.bodegas.data.repository.customerOrders.mapper.OrderDetailItemMapper
import com.android.bodegas.data.repository.customerOrders.store.remote.CustomerOrdersRemote
import com.android.bodegas.data.repository.customerOrders.store.remote.CustomerOrdersRemoteDataStorage
import com.android.bodegas.data.repository.generate_order.GenerateOrderDataRepository
import com.android.bodegas.data.repository.generate_order.mapper.ListOrderMapper
import com.android.bodegas.data.repository.generate_order.store.remote.GenerateOrderRemote
import com.android.bodegas.data.repository.generate_order.store.remote.GenerateOrderRemoteDataStore
import com.android.bodegas.data.repository.departments.DepartmentDataRepository
import com.android.bodegas.data.repository.departments.mapper.DepartmentMapper
import com.android.bodegas.data.repository.departments.store.remote.DepartmentRemote
import com.android.bodegas.data.repository.departments.store.remote.DepartmentRemoteDataStorage
import com.android.bodegas.data.repository.districts.DistrictDataRepository
import com.android.bodegas.data.repository.districts.mapper.DistrictMapper
import com.android.bodegas.data.repository.districts.store.remote.DistrictRemote
import com.android.bodegas.data.repository.districts.store.remote.DistrictRemoteDataStorage
import com.android.bodegas.data.repository.duplicateOrder.DuplicateOrderDataRepository
import com.android.bodegas.data.repository.duplicateOrder.mapper.DuplicateOrderEntityMapper
import com.android.bodegas.data.repository.duplicateOrder.mapper.ListDuplicateOrderItemMapper
import com.android.bodegas.data.repository.duplicateOrder.store.remote.DuplicateOrderRemote
import com.android.bodegas.data.repository.duplicateOrder.store.remote.DuplicateOrderRemoteDataStore
import com.android.bodegas.data.repository.subcategory.SubcategoryDataRepository
import com.android.bodegas.data.repository.subcategory.mapper.SubCategoryMapper
import com.android.bodegas.data.repository.subcategory.store.remote.SubcategoryRemote
import com.android.bodegas.data.repository.subcategory.store.remote.SubcategoryRemoteDataStorage

import com.android.bodegas.data.repository.produtcs.mapper.ProductsMapper
import com.android.bodegas.data.repository.produtcs.store.ProductsDataRepository
import com.android.bodegas.data.repository.produtcs.store.remote.ProductsRemote
import com.android.bodegas.data.repository.produtcs.store.remote.ProductsRemoteDataStore
import com.android.bodegas.data.repository.save_order.SaveOrderDataRepository
import com.android.bodegas.data.repository.save_order.mapper.SaveOrderMapper
import com.android.bodegas.data.repository.save_order.store.db.SaveOrderDb
import com.android.bodegas.data.repository.save_order.store.db.SaveOrderDbDataStore
import com.android.bodegas.data.repository.provinces.ProvinceDataRepository
import com.android.bodegas.data.repository.provinces.mapper.ProvinceMapper
import com.android.bodegas.data.repository.provinces.store.remote.ProvinceRemote
import com.android.bodegas.data.repository.provinces.store.remote.ProvinceRemoteDataStorage
import com.android.bodegas.data.repository.sub_sub_category.mapper.GroupMapper
import com.android.bodegas.data.repository.sub_sub_category.mapper.SSubCategoryMapper
import com.android.bodegas.data.repository.sub_sub_category.store.SSubCategoryDataRepository
import com.android.bodegas.data.repository.sub_sub_category.store.remote.SSubCategoryRemote
import com.android.bodegas.data.repository.sub_sub_category.store.remote.SSubCategoryRemoteDataStore

import com.android.bodegas.data.repository.supplier.SupplierDataRepository
import com.android.bodegas.data.repository.supplier.mapper.PaymentMethodMapper
import com.android.bodegas.data.repository.supplier.mapper.ScheduleMapper
import com.android.bodegas.data.repository.supplier.mapper.SupplierMapper
import com.android.bodegas.data.repository.supplier.store.remote.SupplierRemote
import com.android.bodegas.data.repository.supplier.store.remote.SupplierRemoteDataStore
import com.android.bodegas.data.repository.user.UserDataRepository
import com.android.bodegas.data.repository.user.mapper.RegisterUserMapper
import com.android.bodegas.data.repository.user.mapper.UserLoginMapper
import com.android.bodegas.data.repository.user.mapper.UserLoginTokenMapper
import com.android.bodegas.data.repository.user.mapper.UserMapper
import com.android.bodegas.data.repository.user.store.db.UserDb
import com.android.bodegas.data.repository.user.store.db.UserDbDataStore
import com.android.bodegas.data.repository.user.store.remote.UserRemote
import com.android.bodegas.data.repository.user.store.remote.UserRemoteDataStore

import com.android.bodegas.domain.category.CategoryRepository
import com.android.bodegas.domain.category.GetCategories
import com.android.bodegas.domain.clases.ClassRepository
import com.android.bodegas.domain.clases.GetClassesBySupplierId
import com.android.bodegas.domain.customerOrders.CustomerOrderRepository
import com.android.bodegas.domain.customerOrders.GetCustomerOrdersByCustomerId
import com.android.bodegas.domain.customerOrders.UpdateOrderStatus
import com.android.bodegas.domain.generate_order.GenerateOrder
import com.android.bodegas.domain.generate_order.GenerateOrderRepository
import com.android.bodegas.domain.department.DepartmentRepository
import com.android.bodegas.domain.department.GetDepartments
import com.android.bodegas.domain.district.DistrictRepository
import com.android.bodegas.domain.district.GetDistricts
import com.android.bodegas.domain.duplicate_order.DuplicateOrderRepository
import com.android.bodegas.domain.duplicate_order.GetDuplicateOrder
import com.android.bodegas.domain.subcategory.GetSubcategories
import com.android.bodegas.domain.subcategory.SubcategoryRepository

import com.android.bodegas.domain.products.GetProductsBySubCategory
import com.android.bodegas.domain.products.ProductsRepository
import com.android.bodegas.domain.products.SearchProductsByEstablishment
import com.android.bodegas.domain.save_order.GetOrderByUser
import com.android.bodegas.domain.save_order.SaveOrderForSend
import com.android.bodegas.domain.save_order.SaveOrderRepository
import com.android.bodegas.domain.province.GetProvinces
import com.android.bodegas.domain.province.ProvinceRepository
import com.android.bodegas.domain.sub_sub_category.GetSSubCategory
import com.android.bodegas.domain.sub_sub_category.SSubCategoryRepository

import com.android.bodegas.domain.supplier.GetSupplierByType
import com.android.bodegas.domain.supplier.SupplierRepository
import com.android.bodegas.domain.user.UserRepository
import com.android.bodegas.domain.user.create.RegisterNewUser
import com.android.bodegas.domain.user.login.LoginTokenUser
import com.android.bodegas.domain.user.login.LoginUser
import com.android.bodegas.domain.user.update.RecoveryPassword
import com.android.bodegas.domain.user.update.UpdateDataUser
import com.android.bodegas.domain.user.update.UpdatePhotoUser
import com.android.bodegas.presentation.category.CategoryViewModel
import com.android.bodegas.presentation.category.mapper.CategoryViewMapper
import com.android.bodegas.presentation.clases.ClassViewModel
import com.android.bodegas.presentation.clases.mapper.ClassViewMapper
import com.android.bodegas.presentation.login.LoginViewModel


//import com.android.bodegas.presentation.stores.StoresViewModel
import com.android.bodegas.presentation.subcategory.SubCategoryViewModel
import com.android.bodegas.presentation.subcategory.mapper.SubCategoryViewMapper


import com.android.bodegas.presentation.products.ProductsViewModel
import com.android.bodegas.presentation.products.SearchProductsViewModel
import com.android.bodegas.presentation.products.mapper.ProductsViewMapper

import com.android.bodegas.presentation.save_order.SaveOrderViewModel
import com.android.bodegas.presentation.save_order.mapper.SaveOrderViewMapper

import com.android.bodegas.presentation.register_user.RegistroViewModel
import com.android.bodegas.presentation.register_user.mapper.*
import com.android.bodegas.presentation.detail_orders.DetailOrderViewModel
import com.android.bodegas.presentation.orders.CustomerOrdersViewModel
import com.android.bodegas.presentation.orders.mapper.CustomerOrderViewMapper
import com.android.bodegas.presentation.orders.mapper.EstablishmentOrdersViewMapper
import com.android.bodegas.presentation.orders.mapper.OrderDetailsViewMapper
import com.android.bodegas.presentation.profile.ProfileViewModel
import com.android.bodegas.presentation.subsubcategory.mapper.GroupViewMapper
import com.android.bodegas.presentation.subsubcategory.mapper.SSubcategoryViewMapper
import com.android.bodegas.presentation.supplier.SupplierViewModel
import com.android.bodegas.presentation.supplier.mapper.ScheduleViewMapper
import com.android.bodegas.presentation.supplier.mapper.SupplierViewMapper
import com.android.bodegas.domain.user.update.UpdatePassword
import com.android.bodegas.presentation.detail_orders.DuplicateOrderViewModel
import com.android.bodegas.presentation.login.mapper.UserLoginTokenViewMapper
import com.android.bodegas.presentation.supplier.mapper.PaymentMethodViewMapper
import com.android.bodegas.data.network.allPaymentMethods.AllPaymentMethodsRemoteImpl
import com.android.bodegasadmin.data.network.allPaymentMethods.AllPaymentMethodsServiceFactory
import com.android.bodegasadmin.data.network.allPaymentMethods.mapper.AllPaymentMethodsEntityMapper
import com.android.bodegas.data.repository.allPaymentMethods.AllPaymentMethodsDataRepository
import com.android.bodegasadmin.data.repository.allPaymentMethods.mapper.AllPaymentMethodsMapper
import com.android.bodegas.data.repository.allPaymentMethods.store.remote.AllPaymentMethodsRemote
import com.android.bodegas.data.repository.allPaymentMethods.store.remote.AllPaymentMethodsRemoteDataStore
import com.android.bodegas.domain.allpaymentmethods.AllPaymentMethodRepository
import com.android.bodegas.domain.allpaymentmethods.GetAllPaymentMethods
import com.android.bodegas.presentation.allPaymentMethods.GetAllPaymentMethodsViewModel
import com.android.bodegasadmin.presentation.allPaymentMethods.mapper.AllPaymentMethodsViewMapper
import org.koin.android.ext.koin.androidContext
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val appModule = module(override = false) {

    single { PreferencesManager() }
    single {
        Room.databaseBuilder(
            androidContext(),
            AppDatabase::class.java, "bodegas.db"
        ).fallbackToDestructiveMigration()
            .build()
    }
}

val userModule = module(override = false) {
    single { UserServiceFactory.makeService(BuildConfig.DEBUG, androidContext()) }

    factory { UserDbMapper() }
    factory<UserDb> { UserDbImpl(get(), get()) }
    factory { UserDbDataStore(get()) }

    factory { UserLoginEntityMapper() }
    factory { UserEntityMapper() }
    factory { RegisterUserEntityMapper() }
    factory<UserRemote> { UserRemoteImpl(get(), get(),get(), get()) }
    factory { UserRemoteDataStore(get()) }

    factory { UserMapper() }
    factory { UserLoginMapper() }
    factory { RegisterUserMapper() }
    factory<UserRepository> { UserDataRepository(get(), get(), get(), get(), get()) }
    single { LoginUser(get()) }
    single { RegisterNewUser(get()) }
    single { UpdatePhotoUser(get()) }
    single { UpdatePassword(get()) }
    single { UpdateDataUser(get()) }
    single { RecoveryPassword(get()) }
    factory { UserViewMapper() }
   // factory { UpdateDataUserActivity() }
    factory { UserLoginTokenEntityMapper() }
    factory { UserLoginTokenMapper() }
    factory { LoginTokenUser(get()) }
    factory { UserLoginTokenViewMapper() }
    factory { RegisterUserBodyViewMapper() }
    viewModel { LoginViewModel(get(), get(), get(), get(), get()) }
    viewModel { RegistroViewModel( get(), get(), get(), get(),get(), get(),get(), get(), get(), get(), get()) }
    viewModel { ProfileViewModel(get(), get(), get(), get(), get()) }
}

val supplierModule = module(override = false) {
    single { SupplierServiceFactory.makeService(BuildConfig.DEBUG, androidContext()) }
    factory { ScheduleEntityMapper() }
    factory { SupplierEntityMapper(get(), get()) }
    factory<SupplierRemote> { SupplierRemoteImpl(get(), get(), androidContext()) }
    factory { SupplierRemoteDataStore(get()) }
    factory { ScheduleMapper() }
    factory { SupplierMapper(get(), get()) }
    factory<SupplierRepository> { SupplierDataRepository(get(), get()) }
    single { GetSupplierByType(get()) }
    factory { ScheduleViewMapper() }
    factory { SupplierViewMapper(get(), get()) }
    viewModel { SupplierViewModel(get(), get()) }
}

val allPaymentMethodsModule = module(override = false){
    single { AllPaymentMethodsServiceFactory.makeService(BuildConfig.DEBUG, androidContext()) }
    factory { AllPaymentMethodsEntityMapper() }
    factory<AllPaymentMethodsRemote> { AllPaymentMethodsRemoteImpl(get(), get()) }
    factory { AllPaymentMethodsRemoteDataStore(get()) }
    factory { AllPaymentMethodsMapper() }
    factory<AllPaymentMethodRepository> { AllPaymentMethodsDataRepository(get(), get()) }
    single { GetAllPaymentMethods(get()) }
    factory { AllPaymentMethodsViewMapper() }
    factory { PaymentMethodEntityMapper() }
    factory { PaymentMethodViewMapper() }
    factory { PaymentMethodMapper() }
    viewModel { GetAllPaymentMethodsViewModel(get(), get()) }
}

val classModule = module(override = false) {
    single { ClassServiceFactory.makeService(BuildConfig.DEBUG, androidContext()) }
    factory { ClassEntityMapper() }
    factory<ClassRemote> { ClassRemoteImpl(get(), get(), androidContext()) }
    factory { ClassRemoteDataStorage(get()) }
    factory { ClassMapper() }
    factory<ClassRepository> { ClassDataRepository(get(), get()) }
    single { GetClassesBySupplierId(get()) }
    factory { ClassViewMapper() }
    viewModel { ClassViewModel(get(), get()) }
}

val categoryModule = module(override = false) {
    single { CategoryServiceFactory.makeService(BuildConfig.DEBUG, androidContext()) }
    factory { CategoryEntityMapper() }
    factory<CategoryRemote> { CategoryRemoteImpl(get(), get(), androidContext()) }
    factory { CategoryRemoteDataStorage(get()) }
    factory { CategoryMapper() }
    factory<CategoryRepository> { CategoryDataRepository(get(), get()) }
    single { GetCategories(get()) }
    factory { CategoryViewMapper() }
    viewModel { CategoryViewModel(get(), get()) }
}

val subCategoryModule = module(override = false) {
    single { SubCategoryServiceFactory.makeService(BuildConfig.DEBUG, androidContext()) }
    factory { SubcategoryEntityMapper() }
    factory<SubcategoryRemote> { SubcategoryRemoteImpl(get(), get(), androidContext()) }
    factory { SubcategoryRemoteDataStorage(get()) }
    factory { SubCategoryMapper() }
    factory<SubcategoryRepository> { SubcategoryDataRepository(get(), get()) }
    single { GetSubcategories(get()) }
    factory { SubCategoryViewMapper() }
    viewModel { SubCategoryViewModel(get(), get(), get(), get()) }
}


val productsModule = module(override = false) {
    single { ProductsServiceFactory.makeService(BuildConfig.DEBUG, androidContext()) }
    factory { ProductsEntityMapper() }
    factory<ProductsRemote> { ProductsRemoteImpl(get(), get(), androidContext()) }
    factory { ProductsRemoteDataStore(get()) }
    factory { ProductsMapper() }
    factory<ProductsRepository> { ProductsDataRepository(get(), get()) }
    single { GetProductsBySubCategory(get()) }
    single { SearchProductsByEstablishment(get()) }
    factory { ProductsViewMapper() }
    viewModel { ProductsViewModel(get(), get()) }
    viewModel { SearchProductsViewModel(get(), get()) }
}

val subsubcategoryModule = module(override = false) {
    single { SSubCategoryServiceFactory.makeService(BuildConfig.DEBUG, androidContext()) }
    factory { SSubcategoryEntityMapper() }
    factory { GroupEntityMapper(get(), get()) }
    factory<SSubCategoryRemote> { SSubCategoryRemoteImpl(get(), get(), androidContext()) }
    factory { SSubCategoryRemoteDataStore(get()) }
    factory { SSubCategoryMapper() }
    factory { GroupMapper(get(), get()) }
    factory<SSubCategoryRepository> { SSubCategoryDataRepository(get(), get()) }
    single { GetSSubCategory(get()) }
    factory { SSubcategoryViewMapper() }
    factory { GroupViewMapper(get(), get()) }
}

val saveOrderModule = module(override = false) {

    factory { SaveOrderDbMapper() }
    single<SaveOrderDb> { SaverOrderDbImpl(get(), get()) }
    factory { SaveOrderDbDataStore(get()) }
    factory { SaveOrderMapper() }
    factory<SaveOrderRepository> { SaveOrderDataRepository(get(), get()) }
    factory { SaveOrderViewMapper() }
    single { GetOrderByUser(get()) }
    single { SaveOrderForSend(get()) }
}

val generateOrderModule = module(override = false) {
    single { GenerateOrderServiceFactory.makeService(BuildConfig.DEBUG, androidContext()) }
    factory { GenerateOrderEntityMapper() }
    single<GenerateOrderRemote> { GenerateOrderRemoteImpl(get(), get()) }
    factory { GenerateOrderRemoteDataStore(get()) }
    factory { ListOrderMapper() }
    factory<GenerateOrderRepository> { GenerateOrderDataRepository(get(), get(), get()) }
    single { GenerateOrder(get()) }
    viewModel { SaveOrderViewModel(get(), get(), get(), get()) }
}

val departmentModule= module(override = false) {
    single { DepartmentServiceFactory.makeService(BuildConfig.DEBUG, androidContext()) }
    factory { DepartmentEntityMapper() }
    factory<DepartmentRemote> { DepartmentRemoteImpl(get(), get(), androidContext()) }
    factory { DepartmentRemoteDataStorage(get()) }
    factory { DepartmentMapper() }
    factory<DepartmentRepository> { DepartmentDataRepository(get(), get()) }
    single { GetDepartments(get()) }
    factory { DepartmentViewMapper() }
}

val provinceModule= module(override = false) {
    single { ProvinceServiceFactory.makeService(BuildConfig.DEBUG, androidContext()) }
    factory { ProvinceEntityMapper() }
    factory<ProvinceRemote> { ProvinceRemoteImpl(get(), get(), androidContext()) }
    factory { ProvinceRemoteDataStorage(get()) }
    factory { ProvinceMapper() }
    factory<ProvinceRepository> { ProvinceDataRepository(get(), get()) }
    single { GetProvinces(get()) }
    factory { ProvinceViewMapper() }
}

val districtModule= module(override = false) {
    single { DistrictServiceFactory.makeService(BuildConfig.DEBUG, androidContext()) }
    factory { DistrictEntityMapper() }
    factory<DistrictRemote> { DistrictRemoteImpl(get(), get(), androidContext()) }
    factory { DistrictRemoteDataStorage(get()) }
    factory { DistrictMapper() }
    factory<DistrictRepository> { DistrictDataRepository(get(), get()) }
    single { GetDistricts(get()) }
    factory { DistrictViewMapper() }
}

val customerOrders= module(override = false) {
    single { CustomerOrdersServiceFactory.makeService(BuildConfig.DEBUG, androidContext()) }
    factory { EstablishmentOrdersEntityMapper(get(), get()) }
    factory { CustomerOrdersEntityMapper(get(), get())}
    factory { OrderDetailItemEntityMapper() }
    factory { OrderDetailBodyEntityMapper() }
    factory<CustomerOrdersRemote> { CustomerOrdersRemoteImpl(get(), get(), get()) }
    factory { CustomerOrdersRemoteDataStorage(get()) }
    factory { EstablishmentOrdersMapper(get(), get()) }
    factory { CustomerOrdersMapper(get(), get()) }
    factory { OrderDetailItemMapper() }
    factory { OrderDetailBodyMapper() }
    factory<CustomerOrderRepository> { CustomerOrdersDataRepository(get(), get(), get()) }
    single { GetCustomerOrdersByCustomerId(get()) }
    factory { CustomerOrdersViewModel(get(),get()) }
    factory { EstablishmentOrdersViewMapper(get(), get()) }
    factory { CustomerOrderViewMapper(get(), get()) }
    factory { OrderDetailsViewMapper() }
    factory { UpdateOrderStatus(get()) }
    viewModel { DetailOrderViewModel(get(), get(), get(), get(), get()) }
}

val duplicateOrderModule = module(override = false){
    single { DuplicateOrderServiceFactory.makeService(BuildConfig.DEBUG, androidContext()) }
    factory { DuplicateOrderEntityMapperNetwork( get()) }
    factory { DuplicateOrderItemEntityMapper() }

    factory { DuplicateOrderEntityMapper( get()) }
    factory { ListDuplicateOrderItemMapper() }

    factory<DuplicateOrderRemote> { DuplicateOrderRemoteImpl(get(), get()) }
    factory { DuplicateOrderRemoteDataStore(get()) }

    factory<DuplicateOrderRepository> { DuplicateOrderDataRepository(get(), get()) }
  //  factory { DuplicateOrderDataStore( get()) }
    single { GetDuplicateOrder(get()) }
    viewModel { DuplicateOrderViewModel(get()) }

}