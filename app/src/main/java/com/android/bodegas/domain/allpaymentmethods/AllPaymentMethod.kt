package com.android.bodegasadmin.domain.allpaymentmethods

data class AllPaymentMethod(
    val paymentMethodId: Int,
    val requestedAmount: Boolean,
    val description: String,
    val customerMessage: String
)