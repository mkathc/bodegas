package com.android.bodegas.domain.sub_sub_category

import com.android.bodegas.domain.util.Resource


interface SSubCategoryRepository {
    suspend fun getSSubCategory(
        establishmentId: Int,
        subCategoryId: Int
    ): Resource<Group>
}
