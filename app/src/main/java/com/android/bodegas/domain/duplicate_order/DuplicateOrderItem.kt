package com.android.bodegas.domain.duplicate_order

data class DuplicateOrderItem (
    val description: String,
    val price: Double,
    val storeProductId: Int,
    val subTotal: Double
)