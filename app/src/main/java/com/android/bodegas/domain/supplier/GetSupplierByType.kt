package com.android.bodegas.domain.supplier

import com.android.bodegas.domain.util.Resource
import java.util.*


class GetSupplierByType(private val supplierRepository: SupplierRepository) {

    suspend fun getSupplierByType(
        type: Int,
        latitude: String,
        longitude: String,
        radius: Int
    ): Resource<List<Supplier>> {
        return supplierRepository.getSupplierByType(type,latitude, longitude, radius)
    }
}
