package com.android.bodegas.domain.save_order

import com.android.bodegas.domain.util.Resource


interface SaveOrderRepository {

    suspend fun getOrder(): Resource<List<SaveOrder>>

    suspend fun getOrderById(orderId:String): Resource<SaveOrder>

    suspend fun saveOrder(saveOrder: SaveOrder)

    suspend fun updateOrder(saveOrder: SaveOrder)

    suspend fun deleteOrder()

    suspend fun deleteOrderById(orderId: String)

}