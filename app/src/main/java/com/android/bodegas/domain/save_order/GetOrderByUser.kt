package com.android.bodegas.domain.save_order

import com.android.bodegas.domain.util.Resource
import java.util.*


class GetOrderByUser(private val saveOrderRepository: SaveOrderRepository) {

    suspend fun getOrder(): Resource<List<SaveOrder>>  {
        return saveOrderRepository.getOrder()
    }

    suspend fun getOrderById(orderId:String): Resource<SaveOrder>  {
        return saveOrderRepository.getOrderById(orderId)
    }

    suspend fun deleteOrder()  {
        return saveOrderRepository.deleteOrder()
    }

    suspend fun deleteOrderById(orderId: String)  {
        return saveOrderRepository.deleteOrderById(orderId)
    }
}
