package com.android.bodegas.domain.products

import com.android.bodegas.domain.util.Resource
import java.util.*


class SearchProductsByEstablishment(private val productsRepository: ProductsRepository) {

    suspend fun searchProductsByEstablishment(
        searchText: String,
        storeId:Int
    ): Resource<List<Products>> {
        return productsRepository.searchProducts(searchText, storeId)
    }
}
