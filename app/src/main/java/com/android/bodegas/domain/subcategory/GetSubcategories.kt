package com.android.bodegas.domain.subcategory

import com.android.bodegas.domain.util.Resource

class GetSubcategories (private val subclassRepository: SubcategoryRepository) {
    suspend fun getSubCategories(establishmentId:Int,
                                 category_id:Int,
                                 page: String,
                                 size: String,
                                 sortBy: String) : Resource<List<Subcategory>> {
        return subclassRepository.getSubcategories(establishmentId, category_id, page, size,  sortBy)
    }
}
