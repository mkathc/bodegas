package com.android.bodegas.domain.user.create
import com.android.bodegas.domain.user.create.model.User
import com.android.bodegas.domain.user.UserRepository
import com.android.bodegas.domain.user.create.model.RegisterUserBody
import com.android.bodegas.domain.user.login.model.UserLoginToken
import com.android.bodegas.domain.util.Resource

class RegisterNewUser constructor(private val userRepository: UserRepository){

     suspend fun registerUser( registerUserBody: RegisterUserBody): Resource<UserLoginToken> {
        return userRepository.registerUser(registerUserBody)
    }
}
