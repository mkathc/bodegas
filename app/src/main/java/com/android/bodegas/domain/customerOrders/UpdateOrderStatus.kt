package com.android.bodegas.domain.customerOrders

import com.android.bodegas.domain.util.Resource

class UpdateOrderStatus(
    private val customerOrderRepository: CustomerOrderRepository
) {
    suspend fun updateStaterOrder(orderId:Int, state:String, list: List<OrderDetailBody>): Resource<Boolean> {
        return customerOrderRepository.updateStateOrder(orderId, state, list)
    }

}