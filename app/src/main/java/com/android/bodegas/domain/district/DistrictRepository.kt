package com.android.bodegas.domain.district

import com.android.bodegas.domain.department.Department
import com.android.bodegas.domain.util.Resource

interface DistrictRepository {
    suspend fun getDistricts(
        id: String,
        name: String,
        province_id: String,
        department_id: String
    ): Resource<List<District>>
}