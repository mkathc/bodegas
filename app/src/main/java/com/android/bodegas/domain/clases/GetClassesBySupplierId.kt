package com.android.bodegas.domain.clases

import com.android.bodegas.domain.util.Resource

class GetClassesBySupplierId(private val classRepository: ClassRepository) {
    suspend fun getClassesBySupplierId(establishmentId:Int,
                                       page: String,
                                       size: String,
                                       sortBy: String) : Resource<List<Class>> {
        return classRepository.getClassesBySupplierId(establishmentId, page,size,sortBy)
    }
}