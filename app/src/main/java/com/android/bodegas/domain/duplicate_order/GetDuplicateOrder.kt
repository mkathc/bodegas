package com.android.bodegas.domain.duplicate_order

import com.android.bodegas.domain.util.Resource

class GetDuplicateOrder constructor(private val distanceRepository: DuplicateOrderRepository) {

    suspend fun getDuplicateOrder(establishmentId: Int, orderId: Int): Resource<DuplicateOrder> {
        return distanceRepository.getDuplicateOrder(establishmentId, orderId)
    }
}