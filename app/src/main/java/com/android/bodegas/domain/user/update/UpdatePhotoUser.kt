package com.android.bodegas.domain.user.update

import com.android.bodegas.domain.user.UserRepository
import com.android.bodegas.domain.user.create.model.User
import com.android.bodegas.domain.user.login.model.UserLogin
import com.android.bodegas.domain.util.Resource


class UpdatePhotoUser constructor(private val userRepository: UserRepository) {

    suspend fun updatePhoto(customerId: Int, filePath: String): Resource<Boolean> {
        return userRepository.updatePhotoUser(customerId, filePath)
    }
}