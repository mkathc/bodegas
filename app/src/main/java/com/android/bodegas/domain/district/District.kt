package com.android.bodegas.domain.district

data class District (
    val id: String,
    val name: String,
    val province_id: String,
    val department_id: String
)