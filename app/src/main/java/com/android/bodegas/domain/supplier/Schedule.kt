package com.android.bodegas.domain.supplier

data class Schedule(
    val range: String,
    val startTime: String ? ="",
    val endTime: String ? = ""
)