package com.android.bodegas.domain.category

import com.android.bodegas.domain.util.Resource

interface CategoryRepository {
    suspend fun getCategories(
        establishmentId: Int,
        class_id: Int,
        page: String,
        size: String,
        sortBy: String
    ) : Resource<List<Category>>
}
