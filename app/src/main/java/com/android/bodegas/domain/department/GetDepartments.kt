package com.android.bodegas.domain.department

import com.android.bodegas.domain.util.Resource

class GetDepartments (private val departmentRepository: DepartmentRepository){
    suspend fun getDepartments(id:String, name: String) : Resource<List<Department>> {
        return departmentRepository.getDepartments(id, name)
    }
}