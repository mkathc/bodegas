package com.android.bodegas.domain.customerOrders

import com.android.bodegas.domain.util.Resource

class GetCustomerOrdersByCustomerId(
    private val customerOrderRepository: CustomerOrderRepository
) {
    suspend fun getCustomerOrdersByCustomerId(customerId:Int) : Resource<List<CustomerOrders>> {
        return customerOrderRepository.getCustomerOrdersByCustomerId(customerId)
    }

    suspend fun getFileByOrder(orderId:Int) : Resource<String> {
        return customerOrderRepository.getFileByOrder(orderId)
    }
}