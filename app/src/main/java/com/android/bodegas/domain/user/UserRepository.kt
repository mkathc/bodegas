package com.android.bodegas.domain.user

import com.android.bodegas.data.repository.user.model.UserEntity
import com.android.bodegas.domain.user.create.RegisterNewUser
import com.android.bodegas.domain.user.create.model.RegisterUserBody
import com.android.bodegas.domain.user.create.model.User
import com.android.bodegas.domain.user.login.model.UserLogin
import com.android.bodegas.domain.user.login.model.UserLoginToken
import com.android.bodegas.domain.util.Resource
import com.android.bodegas.presentation.login.ForgetPassDTO
import com.android.bodegas.presentation.profile.ChangePasswordDtoBody
import com.android.bodegas.presentation.profile.UpdateCustomerBody


interface UserRepository {

    suspend fun loginUserToken(email: String, password: String): Resource<UserLoginToken>

    suspend fun loginUser(establishmentId: Int): Resource<User>

    suspend fun logoutUser()

    suspend fun registerUser(registerUser: RegisterUserBody): Resource<UserLoginToken>

    suspend fun updatePhotoUser(customerId:Int, filePath:String): Resource<Boolean>

    suspend fun updatePassword(body: ChangePasswordDtoBody): Resource<Boolean>

    suspend fun recoveryPassword(body: ForgetPassDTO): Resource<Boolean>

    suspend fun updateUser(customerId: Int, body: UpdateCustomerBody): Resource<User>
}