package com.android.bodegas.domain.category

import com.android.bodegas.domain.util.Resource

class GetCategories (private val categoryRepository: CategoryRepository) {
    suspend fun getCategories(
        establishmentId:Int,
        class_id:Int,
        page: String,
        size: String,
        sortBy: String) : Resource<List<Category>> {

        return categoryRepository.getCategories(establishmentId,class_id , page,size,sortBy)
    }
}