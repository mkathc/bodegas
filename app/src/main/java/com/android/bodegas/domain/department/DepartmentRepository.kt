package com.android.bodegas.domain.department

import com.android.bodegas.domain.util.Resource

interface DepartmentRepository {
    suspend fun getDepartments(
        id: String,
        name: String
    ): Resource<List<Department>>
}