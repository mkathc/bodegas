package com.android.bodegas.domain.user.update

import com.android.bodegas.domain.user.UserRepository
import com.android.bodegas.domain.util.Resource
import com.android.bodegas.presentation.profile.ChangePasswordDtoBody

class UpdatePassword constructor(private val userRepository: UserRepository) {

    suspend fun updatePassword(body: ChangePasswordDtoBody): Resource<Boolean> {
        return userRepository.updatePassword(body)
    }
}