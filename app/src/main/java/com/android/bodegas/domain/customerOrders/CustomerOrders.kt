package com.android.bodegas.domain.customerOrders

class CustomerOrders (
    val orderId: String,
    val deliveryType: String,
    val orderDate:String,
    val shippingDateFrom: String,
    val shippingDateUntil: String,
    val status: String,
    val establishment: EstablishmentOrders,
    val total:String,
    val orderDetails: List<OrderDetails>,
    val updateDate:String,
    val deliveryCharge: String,
    val customerAmount: String,
    val paymentMethodCustomerMessage: String,
    val paymentMethodDescription: String,
    val paymentMethodEstablishmentMessage: String,
    val paymentMethodPaymentMethodId: Int,
    val paymentMethodRequestAmount: Boolean
)