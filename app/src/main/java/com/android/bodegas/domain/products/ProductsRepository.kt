package com.android.bodegas.domain.products

import com.android.bodegas.domain.util.Resource


interface ProductsRepository {
    suspend fun getProducts(
        establishmentId: Int,
        supplierDocument: String,
        subCategoryId: Int
    ): Resource<List<Products>>


    suspend fun searchProducts(
        searchText: String,
        storeId:Int
    ): Resource<List<Products>>
}
