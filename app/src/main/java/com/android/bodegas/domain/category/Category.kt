package com.android.bodegas.domain.category

class Category (
    val id: Int,
    val description: String,
    val name: String,
    val pathImage: String ? = ""
)