package com.android.bodegas.domain.province

data class Province (
    val id: String,
    val name: String,
    val department_id: String
)