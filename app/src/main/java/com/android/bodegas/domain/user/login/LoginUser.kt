package com.android.bodegas.domain.user.login

import com.android.bodegas.domain.user.UserRepository
import com.android.bodegas.domain.user.create.model.User
import com.android.bodegas.domain.user.login.model.UserLogin
import com.android.bodegas.domain.util.Resource


class LoginUser constructor(private val userRepository: UserRepository) {

    suspend fun loginUser(establishmentId: Int): Resource<User> {
        return userRepository.loginUser(establishmentId)
    }
}