package com.android.bodegas.domain.clases

data class Class (
    val id: Int,
    val description: String,
    val name: String,
    val pathImage:String?=""
)