package com.android.bodegas.domain.products

data class Products(
    val storeProductId: Int,
    val price: Double,
    val status: String,
    val code: String,
    val name: String,
    val description: String,
    val unitMeasure: String,
    val stock:String,
    val imageProduct: String?=""
)