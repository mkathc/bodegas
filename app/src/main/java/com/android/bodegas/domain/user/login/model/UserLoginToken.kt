package com.android.bodegas.domain.user.login.model

class UserLoginToken (
    val idEntity: String,
    val token: String
)