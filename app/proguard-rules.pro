# Add project specific ProGuard rules here.
# You can control the set of applied configuration files using the
# proguardFiles setting in build.gradle.
#
# For more details, see
#   http://developer.android.com/guide/developing/tools/proguard.html

# If your project uses WebView with JS, uncomment the following
# and specify the fully qualified class name to the JavaScript interface
# class:
#-keepclassmembers class fqcn.of.javascript.interface.for.webview {
#   public *;
#}

# Uncomment this to preserve the line number information for
# debugging stack traces.
#-keepattributes SourceFile,LineNumberTable

# If you keep the line number information, uncomment this to
# hide the original source file name.
#-renamesourcefileattribute SourceFile
-keepclassmembers class com.android.bodegas.data.network.allPaymentMethods.model.** { *; }
-keepclassmembers class com.android.bodegas.data.network.category.model.** { *; }
-keepclassmembers class com.android.bodegas.data.network.classes.model.** { *; }
-keepclassmembers class com.android.bodegas.data.network.customerOrders.model.** { *; }
-keepclassmembers class com.android.bodegas.data.network.departments.model.** { *; }
-keepclassmembers class com.android.bodegas.data.network.district.model.** { *; }
-keepclassmembers class com.android.bodegas.data.network.duplicateorder.model.** { *; }
-keepclassmembers class com.android.bodegas.data.network.generate_order.model.** { *; }
-keepclassmembers class com.android.bodegas.data.network.products.model.** { *; }
-keepclassmembers class com.android.bodegas.data.network.provinces.model.** { *; }
-keepclassmembers class com.android.bodegas.data.network.sub_sub_categoty.model.** { *; }
-keepclassmembers class com.android.bodegas.data.network.subcategory.model.** { *; }
-keepclassmembers class com.android.bodegas.data.network.supplier.model.** { *; }
-keepclassmembers class com.android.bodegas.data.network.user.model.** { *; }
